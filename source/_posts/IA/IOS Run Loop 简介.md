-   **什么是Run Loop?**

-   **Run Loop的作用**

-   **使用Run Loop的场景**

-   **Run Loop启动方式**

-   **Run Loop退出方式**

-   **Run Loop与线程关系**

-   **RunLoop 结构图**

-   **RunLoop内部逻辑**

-   **RunLoop应用举例**

-   **FAQ**

**1.什么是Run Loop?**

![](media/53410a07f58939b54cb0b4ca23ab5200.png)

RunLoop
是与线程相关的基础架构的一部分。运行循环是一个事件处理循环，用于调度工作并协调传入事件的接收。运行循环的目的是在有工作时保持线程忙，并在没有线程时让线程进入休眠状态。本质上就是一个的对象，这个对象管理了其需要处理的事件和消息，并提供了一个入口函数来执行Event
Loop 的逻辑。线程执行了这个函数后，就会一直处于这个函数内部
“接受消息-\>等待休眠（节省CPU）-\>处理” 的循环中，直到这个循环结束（比如传入
quit 的消息），函数返回。抽象出的代码结构如下：

>   main() {

>       initialize();

>       do {

>           message = get_next_message();

>           process_message(message);

>       } while (message != quit);

>   }

OSX/iOS 系统中，提供了两个这样的对象：NSRunLoop 和 CFRunLoopRef。

    **CFRunLoopRef** 是在 CoreFoundation 框架内的，它提供了纯 C 函数的
API，所有这些 API 都是线程安全的。苹果已经开源了CoreFoundation源代码。

**NSRunLoop** 是Foundation框架，基于 CFRunLoopRef 的封装，提供了面向对象的
API，但是这些 API 不是线程安全的。

**2.Run Loop的作用**

1.  保持线程持续存在  
      
    线程中包含一个以上的RunLoopMode
    Item对象时，该线程则会持续存在，直到线程被销毁。

2.  处理App中的各种事件  
      
    比如：触摸事件，定时器事件，Selector事件等。

3.  节省CPU资源，提高程序性能  
      
    程序运行起来时，当什么操作都没有做的时候，RunLoop就告诉CPU，现在没有事情做，我要去休息，这时CPU就会将其资源释放出来去做其他的事情，当有事情做的时候RunLoop就会立马起来去做事情。

**3.使用Run Loop的场景**

1.  使用端口或自定义输入源来和其他线程通信

2.  使用线程的定时器

3.  Cocoa中使用任何performSelector...的方法

4.  使线程周期性工作

**4.Run Loop启动方式**

1.  **无条件启动：**  
      
    - (void)run;  
      
    无条件的进入run
    loop是最简单的方法，但也最不推荐使用的，除非你不想退出这个run
    loop。因为这样会使你的线程处在一个永久的循环中，这会让你对run
    loop本身的控制很少。你可以添加或删除输入源和定时器，但是退出run
    loop的唯一方法是杀死它。没有任何办法可以让这run
    loop运行在自定义模式下，默认运行在NSDefaultRunLoopMode模式下。

2.  **设置超时时间：**  
      
    - (void)runUntilDate:(NSDate \*)limitDate;  
      
      
      
    用预设超时时间来运行run loop，run
    loop运作直到某一事件到达或者规定的时间已经到期。如果是事件到达，消息会被传递给相应的处理程序来处理，然后run
    loop退出。你可以重新启动run
    loop来等待下一事件。如果是规定时间到期了，你只需简单的重启run
    loop或使用此段时间来做任何的其他工作。

3.  **特定的模式：**  
      
    - (BOOL)runMode:(NSRunLoopMode)mode beforeDate:(NSDate \*)limitDate;  
      
      
      
    使用特定的模式来运行你的run loop。模式和超时不是互斥的，他们可以在启动run
    loop的时候同时使用。模式限制了可以传递事件给run loop的输入源的类型，这在”Run
    Loop模式”部分介绍。

**5.Run Loop退出方式**

1.  **给run loop设置超时时间**  
      
    指定一个超时时间可以使run loop退出前完成所有正常操作，包括发送消息给run
    loop观察者。（推荐使用）

2.  **通知run loop停止**  
      
    CFRunLoopStop([NSRunLoopcurrentRunLoop].getCFRunLoop);  
      
      
      
    使用CFRunLoopStop来显式的停止run loop和使用超时时间产生的结果相似。Run
    loop把所有剩余的通知发送出去再退出。与设置超时的不同的是你可以在无条件启动的run
    loop里面使用该技术。

移除run loop的输入源和定时器也可能导致run loop退出，但这并不是可靠的退出run
loop的方法。一些系统例程会添加输入源到run
loop里面来处理所需事件。因为你的代码未必会考虑到这些输入源，这样可能导致你无法没从系统例程中移除它们，从而导致退出run
loop。

**6.Run Loop与线程关系**

iOS 开发中能遇到两个线程对象: pthread_t 和 NSThread。可以肯定的是 pthread_t 和
NSThread 是一一对应的。比如，你可以通过 pthread_main_thread_np() 或 [NSThread
mainThread] 来获取主线程；也可以通过 pthread_self() 或 [NSThread currentThread]
来获取当前线程。**CFRunLoop 是基于 pthread 来管理的**。

线程和 RunLoop 之间是一一对应的，其关系是保存在一个全局的 Dictionary
里。线程刚创建时并没有 RunLoop，如果你不主动获取，那它一直都不会有。RunLoop
的创建是发生在第一次获取时，RunLoop
的销毁是发生在线程结束时。你只能在一个线程的内部获取其 RunLoop（主线程除外）。

    获取线程RunLoop的方式，如下代码所示：

| /// 全局的Dictionary，key 是 pthread_t， value 是 CFRunLoopRef staticCFMutableDictionaryRef loopsDic; /// 访问 loopsDic 时的锁 static*C*FSpinLock_t loopsLock; /// 获取一个 pthread 对应的 RunLoop。 CFRunLoopRef \_CFRunLoopGet(pthread_t thread) {     *O*SSpinLockLock(\&loopsLock);     if (!loopsDic) {         // 第一次进入时，初始化全局Dic，并先为主线程创建一个 RunLoop。         loopsDic = *CFDictionaryCreateMutable*(*)*;         CFRunLoopRef*m*ainLoop = *\_CFRunLoopCreate()*;         CFDictionarySetValue(loopsDic, *pthread_main_thread_np()*, mainLoop);     }     /// 直接从 Dictionary 里获取。     CFRunLoopRef*l*oop = *CFDictionaryGetValue(loopsDic, thread));*     if (!loop) {         /// 取不到时，创建一个         loop *=_CFRunLoopCreate()*;         CFDictionarySetValue(loopsDic, thread, loop);         /// 注册一个回调，当线程销毁时，顺便也销毁其对应的 RunLoop。 TSD线程私有数据         *\_*CFSetTSD(*.*.., thread, loop, \__CFFinalizeRunLoop);     }     OSSpinLockUnLock(\&loopsLock);     return loop; } CFRunLoopRef CFRunLoopGetMain() {     return_CFRunLoopGet(pthread_main_thread_np()); } CFRunLoopRef CFRunLoopGetCurrent() {     return \_CFRunLoopGet(pthread_self()); } |
|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|


**7.RunLoop 结构图**

Run loop接收输入事件来自两种不同的来源：输入源（input source）和定时源（timer
source）。输入源传递异步事件，通常消息来自于其他线程或程序。定时源则传递同步事件，发生在特定时间或者重复的时间间隔。两种源都使用程序的某一特定的处理例程来处理到达的事件。

官方结构图如下：

![](media/b9606c711f4481639ca8dc2d89858167.png)

代码结构图如下：

![](media/a3fb634fa07c2af725e248873181b174.png)

  一个 RunLoop 包含若干个 Mode，每个 Mode 又包含若干个
Source/Timer/Observer。每次调用RunLoop 的主函数时，只能指定其中一个
Mode，这个Mode被称作 CurrentMode。如果需要切换 Mode，可以退出当前
RunLoop，再重新指定一个 Mode
进入，或者在当前RunLoop中嵌套切换Mode。这样做主要是为了分隔开不同组的Source/Timer/Observer，让其互不影响。

      有关RunLoop的5个结构体：

-   CFRunLoopRef

-   CFRunLoopModeRef

-   CFRunLoopSourceRef

-   CFRunLoopObserverRef

-   CFRunLoopTimerRef

**CFRunLoopRef**

>   typedefstruct__CFRunLoop \* CFRunLoopRef;

>   struct \__CFRunLoop {

>       CFRuntimeBase \_base;

>       pthread_mutex_t \_lock; /\* locked for accessing mode list \*/

>       \__CFPort \_wakeUpPort; // used for CFRunLoopWakeUp

>       Boolean \_unused;

>       volatile_per_run_data \*_perRunData; // reset for runs of the run loop

>       pthread_t \_pthread;

>       uint32_t \_winthread;

>       CFMutableSetRef \_commonModes; // 字符串，记录所有标记为common的mode

>       CFMutableSetRef \_commonModeItems; //
>   所有commonMode的item(source、timer、observer)

>       CFRunLoopModeRef \_currentMode;

>       CFMutableSetRef \_modes; // CFRunLoopModeRef set

>       struct \_block_item \*_blocks_head;

>       struct_block_item \*_blocks_tail;

>       CFTypeRef \_counterpart;

>   };

**CFRunLoopModeRef**

从mode的组成可以看出来：mode管理了所有的事件（sources/timers/observers），而RunLoop是管理mode的，CFRunLoopModeRef没有对外暴露，通过CFRunLoopRef的接口进行了封装。CFRunLoopModeRef是所有要监视的输入源和定时源以及要通知的run
loop注册观察者的集合。每次运行你的run
loop，你都要指定（无论显示还是隐式）其运行个模式。在run
loop运行过程中，只有和模式相关的源才会被监视并允许他们传递事件消息。（类似的，只有和模式相关的观察者会通知run
loop的进程）。和其他模式关联的源只有在run
loop运行在其模式下才会运行，否则处于暂停状态。

>   struct \__CFRunLoopMode {

>       CFStringRef \_name; // mode的名字，唯一标识

>       Boolean \_stopped; // mode的状态，是否停止

>       CFMutableSetRef \_sources0; // sources0 的集合

>       CFMutableSetRef \_sources1; // sources1 的集合

>       CFMutableArrayRef \_observers; // 存储所有观察者（observers）的数组

>       CFMutableArrayRef \_timers; // 存储所有定时器（timers）的数组

>       CFMutableDictionaryRef \_portToV1SourceMap; // 字典
>   key是__CFPort，value是CFRunLoopSourceRef

>       \__CFPortSet \_portSet; // 端口的集合

>   }

**系统提供的Mode**

-   kCFRunLoopDefaultMode: App的默认Mode，通常主线程是在这个 Mode 下运行的。

-   UITrackingRunLoopMode: 界面跟踪 Mode，用于 ScrollView
    追踪触摸滑动，保证界面滑动时不受其他 Mode 影响。

-   UIInitializationRunLoopMode: 在刚启动 App 时进入的第一个
    Mode，启动完成后就不再使用。

-   GSEventReceiveRunLoopMode：接受系统内部事件，通常用不到。

-   kCFRunLoopCommonModes：伪模式，不是一种真正的运行模式。  
      
         
    我们没有办法直接创建一个CFRunLoopMode对象，但是我们可以调用CFRunLoopAddCommonMode
    传入一个字符串向 RunLoop 中添加 Mode，传入的字符串即为 Mode
    的名字，Mode对象应该是此时在RunLoop内部创建的。  
      
      
      
    void CFRunLoopAddCommonMode(CFRunLoopRef rl, CFStringRef modeName) {  
      
        CHECK_FOR_FORK();  
      
        if (__CFRunLoopIsDeallocating(rl)) return;  
      
        \__CFRunLoopLock(rl);  
      
        //看rl中是否已经有这个mode，如果有就什么都不做  
      
        if (!CFSetContainsValue(rl-\>_commonModes, modeName)) {  
      
            CFSetRef set = rl-\>_commonModeItems ?
    CFSetCreateCopy(kCFAllocatorSystemDefault, rl-\>_commonModeItems) : NULL;  
      
            //把modeName添加到RunLoop的_commonModes中  
      
            CFSetAddValue(rl-\>_commonModes, modeName);  
      
            if (NULL != set) {  
      
                CFTypeRef context[2] = {rl, modeName};  
      
                /\* add all common-modes items to new mode \*/  
      
               
    //这里调用CFRunLoopAddSource/CFRunLoopAddObserver/CFRunLoopAddTimer的时候会调用  
      
                //__CFRunLoopFindMode(rl, modeName,
    true)，CFRunLoopMode对象在这个时候被创建  
      
                CFSetApplyFunction(set, (__CFRunLoopAddItemsToCommonMode), (void
    \*)context);  
      
                CFRelease(set);  
      
            }  
      
        } else {  
      
        }  
      
        \__CFRunLoopUnlock(rl);  
      
    }

    -   modeName不能重复，modeName是mode的唯一标识符

    -   RunLoop的_commonModes数组存放所有被标记为common的mode的名称

    -   添加commonMode会把commonModeItems数组中的所有source同步到新添加的mode中

    -   CFRunLoopMode对象在CFRunLoopAddItemsToCommonMode函数中调用CFRunLoopFindMode时被创建

**自定义Mode**

       对于一个 RunLoop 来说，其内部的 mode
只能增加不能删除。自定义Mode使用如下所示：

>   \- (void)modeTestTimer {

>       NSLog(\@"mode:%\@",[[NSRunLoopcurrentRunLoop] currentMode]);

>   }

>   /// 这里使用非主线程，主要考虑如果一直处于customMode模式，则主线瘫痪

>   \- (void)runLoopModeTest {

>       dispatch_async(dispatch_get_global_queue(0, 0), \^{

>           NSTimer \*tickTimer = [[NSTimeralloc] initWithFireDate:[NSDatedate]
>   interval:2target:selfselector:\@selector(modeTestTimer)
>   userInfo:nilrepeats:YES];

>           [[NSRunLoopcurrentRunLoop] addTimer:tickTimer
>   forMode:\@"customMode"];

>           [[NSRunLoopcurrentRunLoop]
>   runMode:\@"customMode"beforeDate:[NSDatedistantFuture]];

>       });

>   }

**runloop mode的切换**

-   对于非主线程，我们可以退出当前模式，然后再进入另一个模式，也可以直接进入另一个模式，即嵌套

-   对于主线程，我们当然也可以像上面一样操作，但是主线程有其特殊性，有很多系统的事件。系统会做一些切换，系统切换模式时，并没有使用嵌套。

**一个RunLoop Mode切换与嵌套的实例：**

>   dispatch_async(dispatch_get_global_queue(0, 0), \^{

>           NSTimer \*timer = [[NSTimeralloc] initWithFireDate:[NSDatedate]
>   interval:2repeats:YESblock:\^(NSTimer \* \_Nonnull timer) {

>               NSLog(\@"Hello, World RunLoop On DefaultMode Started");

>               NSLog(\@"DefaultMode-%\@",[[NSRunLoopcurrentRunLoop]
>   currentMode]);

>               staticdispatch_once_t onceToken;

>               dispatch_once(&onceToken, \^{

>                   NSLog(\@"Hello, World RunLoop On CommonMode Started");

>                   NSTimer \*timer1 = [[NSTimeralloc]
>   initWithFireDate:[NSDatedate] interval:2repeats:YESblock:\^(NSTimer \*
>   \_Nonnull timer) {

>                       NSLog(\@"Hello, World RunLoop On CommonMode");

>                       NSLog(\@"CommonMode-%\@",[[NSRunLoopcurrentRunLoop]
>   currentMode]);

>                   }];

>                   [[NSRunLoopcurrentRunLoop] addTimer:timer1
>   forMode:\@"customMode"];

>                   [[NSRunLoopcurrentRunLoop] runMode:\@"customMode" 
>   beforeDate:[NSDatedateWithTimeIntervalSinceNow:4]];

>                   NSLog(\@"DefaultMode-%\@",[[NSRunLoopcurrentRunLoop]
>   currentMode]);

>                   NSLog(\@"Hello, World RunLoop On CommonMode Finished");

>               });

>               NSLog(\@"Hello, World RunLoop On DefaultMode Finished");

>           }];

>           [[NSRunLoopcurrentRunLoop] addTimer:timer
>   forMode:NSDefaultRunLoopMode];

>           [[NSRunLoopcurrentRunLoop] runMode:NSDefaultRunLoopMode 
>   beforeDate:[NSDatedistantFuture]];

>       });

 

输出结果为：

>   **2018-10-23 15:12:38.280298+0800 MAX Phone Manager**[**8969:138208**]
>   **Hello**, **World RunLoop On DefaultMode Started**

>   **2018-10-23 15:12:38.280400+0800 MAX Phone Manager**[**8969:138208**]
>   **DefaultMode-kCFRunLoopDefaultMode**

>   **2018-10-23 15:12:38.280466+0800 MAX Phone Manager**[**8969:138208**]
>   **Hello**, **World RunLoop On CommonMode Started**

>   **2018-10-23 15:12:38.280607+0800 MAX Phone Manager**[**8969:138208**]
>   **Hello**, **World RunLoop On CommonMode**

>   **2018-10-23 15:12:38.280677+0800 MAX Phone Manager**[**8969:138208**]
>   **CommonMode-customMode**

>   **2018-10-23 15:12:40.284447+0800 MAX Phone Manager**[**8969:138208**]
>   **Hello**, **World RunLoop On CommonMode**

>   **2018-10-23 15:12:40.284655+0800 MAX Phone Manager**[**8969:138208**]
>   **CommonMode-customMode**

>   **2018-10-23 15:12:42.280798+0800 MAX Phone Manager**[**8969:138208**]
>   **DefaultMode-kCFRunLoopDefaultMode**

>   **2018-10-23 15:12:42.280967+0800 MAX Phone Manager**[**8969:138208**]
>   **Hello**, **World RunLoop On CommonMode Finished**

>   **2018-10-23 15:12:42.281119+0800 MAX Phone Manager**[**8969:138208**]
>   **Hello**, **World RunLoop On DefaultMode Finished**

>   **2018-10-23 15:12:44.284521+0800 MAX Phone Manager**[**8969:138208**]
>   **Hello**, **World RunLoop On DefaultMode Started**

>   **2018-10-23 15:12:44.284783+0800 MAX Phone Manager**[**8969:138208**]
>   **DefaultMode-kCFRunLoopDefaultMode**

>   **2018-10-23 15:12:44.284982+0800 MAX Phone Manager**[**8969:138208**]
>   **Hello**, **World RunLoop On DefaultMode Finished**

**CFRunLoopSourceRef**

Source有两个类型：Source0与Source1.

-   **Source0**只包含了了一个回调（函数指针），它并不能主动触发事件。使用时，你需要先调用CFRunLoopSourceSignal（source），将这个Source标记为待处理，然后手动调用CFRunLoopWakeUp(runloop)来唤醒RunLoop，让其处理这个事件。

-   **Source1**包含了一个mach_port和一个回调（函数指针），被用于通过内核和其他线程相互发送消息。这种Source能主动唤醒Runloop的线程。

source0和source1的区别，source1比source0多一个接收消息的端口mach_port_t

>   struct \__CFRunLoopSource {

>       CFMutableBagRef \_runLoops; // 一个Source 对应多个RunLoop

>       union {

>           CFRunLoopSourceContext version0; // source0

>           CFRunLoopSourceContext1 version1; //source1

>       } \_context;

>   }

>   // source0

>   typedefstruct {

>       CFIndex version; // 版本号，用来区分是source1还是source0

>       void \* info;

>       // schedule cancel 是对应的，

>       void (\*schedule)(void \*info, CFRunLoopRef rl, CFStringRef mode);

>       void (\*cancel)(void \*info, CFRunLoopRef rl, CFStringRef mode);

>       void (\*perform)(void \*info); // 用来回调的指针

>   } CFRunLoopSourceContext;

>   // source1

>   typedefstruct {

>       CFIndex version; // 版本号

>       void \* info;

>   \#if (TARGET_OS_MAC && !(TARGET_OS_EMBEDDED \|\| TARGET_OS_IPHONE)) \|\|
>   (TARGET_OS_EMBEDDED \|\| TARGET_OS_IPHONE)

>       mach_port_t (\*getPort)(void \*info); // 端口

>       void \* (\*perform)(void \*msg, CFIndex size, CFAllocatorRef allocator,
>   void \*info);

>   \#else

>       void \* (\*getPort)(void \*info);

>       void (\*perform)(void \*info); // 用来回调的指针

>   \#endif

>   } CFRunLoopSourceContext1;

Source操作及接口

>   void CFRunLoopAddSource(CFRunLoopRef rl, CFRunLoopSourceRef source,
>   CFStringRef mode);

>   void CFRunLoopRemoveSource(CFRunLoopRef rl, CFRunLoopSourceRef source,
>   CFStringRef mode);

**CFRunLoopTimerRe**f是基于时间的触发器，包含一个时间长度和一个回调（函数指针）。当其加入到RunLoop时，RunLoop会注册对应的时间点，当时间点到时，RunLoop会被唤醒以执行那个回调。

>   struct \__CFRunLoopTimer {

>       CFRunLoopRef \_runLoop; // timer 对应的runLoop

>       CFMutableSetRef \_rlModes; // 集合，存放对应的modes，猜测一个timer
>   可以有多个modes，即可以被加入到多个modes中

>       CFRunLoopTimerCallBack \_callout;

>   };

Timer操作及接口

>   void CFRunLoopAddTimer(CFRunLoopRef rl, CFRunLoopTimerRef timer, CFStringRef
>   mode);

>   void CFRunLoopRemoveTimer(CFRunLoopRef rl, CFRunLoopTimerRef timer,
>   CFStringRef mode*;*

**CFRunLoopObserverRef**是观察者，每个Observer都包含了一个回调（函数指针），当RunLoop的状态发生变化时，观察者就能通过回调接受到这个变化，用于观察RunLoop的自身状态，可以观测的时间点如下：

>   struct \__CFRunLoopObserver {

>       CFRunLoopRef \_runLoop; // observer对应的runLoop, 一一对应

>       CFIndex \_rlCount; //
>   observer当前监测的runLoop数量，主要在安排/移除runLoop的时候用到

>       **CFOptionFlags** \_activities; // observer观测runLoop的状态，枚举类型，

>       CFIndex \_order; // mode使用数组存储observers，根据_order添加observer

>       CFRunLoopObserverCallBack \_callout;

>   };

>   typedefCF_OPTIONS(**CFOptionFlags**, CFRunLoopActivity) {

>       kCFRunLoopEntry         = (1UL \<\< 0), // 即将进入Loop

>       kCFRunLoopBeforeTimers  = (1UL \<\< 1), // 即将处理 Timer

>       kCFRunLoopBeforeSources = (1UL \<\< 2), // 即将处理 Source

>       kCFRunLoopBeforeWaiting = (1UL \<\< 5), // 即将进入休眠

>       kCFRunLoopAfterWaiting  = (1UL \<\< 6), // 刚从休眠中唤醒

>       kCFRunLoopExit          = (1UL \<\< 7), // 即将退出Loop

>   };

Observer操作及接口

>   void CFRunLoopAddObserver(CFRunLoopRef rl, CFRunLoopObserverRef observer,
>   CFStringRef mode);

>   void CFRunLoopRemoveObserver(CFRunLoopRef rl, CFRunLoopObserverRef observer,
>   CFStringRef mode);

**8.RunLoop内部逻辑**

![](media/da089069efeba95c850b160317dfa2dd.png)

>   /// 用DefaultMode启动

>   void CFRunLoopRun(void) {

>       CFRunLoopRunSpecific(CFRunLoopGetCurrent(), kCFRunLoopDefaultMode,
>   1.0e10, false);

>   }

>   /// 用指定的Mode启动，允许设置RunLoop超时时间

>   int CFRunLoopRunInMode(CFStringRef modeName, CFTimeInterval seconds, Boolean
>   stopAfterHandle) {

>       return CFRunLoopRunSpecific(CFRunLoopGetCurrent(), modeName, seconds,
>   returnAfterSourceHandled);

>   }

>   /// RunLoop的实现

>   int CFRunLoopRunSpecific(runloop, modeName, seconds, stopAfterHandle) {

>       /// 首先根据modeName找到对应mode

>       CFRunLoopModeRefcurrentMode = \__CFRunLoopFindMode(runloop, modeName,
>   false);

>       /// 如果mode里没有source/timer/observer, 直接返回。

>       if (__CFRunLoopModeIsEmpty(currentMode)) return;

>       /// 1. 通知 Observers: RunLoop 即将进入 loop。

>       \__CFRunLoopDoObservers(runloop, currentMode, kCFRunLoopEntry);

>       /// 内部函数，进入loop

>       \__CFRunLoopRun(runloop, currentMode, seconds, returnAfterSourceHandled)
>   {

>           Boolean sourceHandledThisLoop = NO;

>           int retVal = 0;

>           do {

>               /// 2. 通知 Observers: RunLoop 即将触发 Timer 回调。

>               \__CFRunLoopDoObservers(runloop, currentMode,
>   kCFRunLoopBeforeTimers);

>               /// 3. 通知 Observers: RunLoop 即将触发 Source0 (非port) 回调。

>               \__CFRunLoopDoObservers(runloop, currentMode,
>   kCFRunLoopBeforeSources);

>               /// 执行被加入的block，处理费延迟的主线程调用

>               \__CFRunLoopDoBlocks(runloop, currentMode);

>               /// 4. RunLoop 触发 Source0 (非port) 回调。

>               sourceHandledThisLoop = \__CFRunLoopDoSources0(runloop,
>   currentMode, stopAfterHandle);

>               /// 执行被加入的block

>               \__CFRunLoopDoBlocks(runloop, currentMode);

>               /// 5. 如果有 Source1 (基于port) 处于 ready 状态，直接处理这个
>   Source1 然后跳转去处理消息。

>               if (__Source0DidDispatchPortLastTime) {

>                   Boolean hasMsg = \__CFRunLoopServiceMachPort(dispatchPort,
>   &msg)

>                   if (hasMsg) goto handle_msg;

>               }

>               /// 通知 Observers: RunLoop 的线程即将进入休眠(sleep)。

>               if (!sourceHandledThisLoop) {

>                   \__CFRunLoopDoObservers(runloop, currentMode,
>   kCFRunLoopBeforeWaiting);

>               }

>   //GCD dispatch main queue

>       CheckIfExistMessageInMainDispatchQueue();

>               /// 7. 调用 mach_msg 等待接受 mach_port 的消息。线程将进入休眠,
>   直到被下面某一个事件唤醒。

>               /// • 一个基于 port 的Source 的事件。

>               /// • 一个 Timer 到时间了

>               /// • RunLoop 自身的超时时间到了

>               /// • 被其他什么调用者手动唤醒

>               \__CFRunLoopServiceMachPort(waitSet, &msg, sizeof(msg_buffer),
>   &livePort) {

>                   mach_msg(msg, MACH_RCV_MSG, port); // thread wait for
>   receive msg

>               }

>               /// 8. 通知 Observers: RunLoop 的线程刚刚被唤醒了。

>               \__CFRunLoopDoObservers(runloop, currentMode,
>   kCFRunLoopAfterWaiting);

>               /// 收到消息，处理消息。

>           handle_msg:

>               /// 9.1 如果一个 Timer 到时间了，触发这个Timer的回调。

>               if (msg_is_timer) {

>                   \__CFRunLoopDoTimers(runloop, currentMode,
>   mach_absolute_time())

>               }

>               /// 9.2 如果有dispatch到main_queue的block，执行block。

>               elseif (msg_is_dispatch) {

>                   \__CFRUNLOOP_IS_SERVICING_THE_MAIN_DISPATCH_QUEUE__(msg);

>               }

>               /// 9.3 如果一个 Source1 (基于port) 发出事件了，处理这个事件

>               else {

>                   CFRunLoopSourceRef source1 =
>   \__CFRunLoopModeFindSourceForMachPort(runloop, currentMode, livePort);

>                   sourceHandledThisLoop = \__CFRunLoopDoSource1(runloop,
>   currentMode, source1, msg);

>                   if (sourceHandledThisLoop) {

>                       mach_msg(reply, MACH_SEND_MSG, reply);

>                   }

>               }

>               /// 执行加入到Loop的block

>               \__CFRunLoopDoBlocks(runloop, currentMode);

>               if (sourceHandledThisLoop && stopAfterHandle) {

>                   /// 进入loop时参数说处理完事件就返回。

>                   retVal = kCFRunLoopRunHandledSource;

>               } elseif (timeout) {

>                   /// 超出传入参数标记的超时时间了

>                   retVal = kCFRunLoopRunTimedOut;

>               } elseif (__CFRunLoopIsStopped(runloop)) {

>                   /// 被外部调用者强制停止了

>                   retVal = kCFRunLoopRunStopped;

>               } elseif (__CFRunLoopModeIsEmpty(runloop, currentMode)) {

>                   /// source/timer/observer一个都没有了

>                   retVal = kCFRunLoopRunFinished;

>               }

>               /// 如果没超时，mode里没空，loop也没被停止，那继续loop。

>           } while (retVal == 0);

>       }

>       /// 10. 通知 Observers: RunLoop 即将退出。

>       \__CFRunLoopDoObservers(rl, currentMode, kCFRunLoopExit);

>   }

RunLoop代码结构图
-----------------

![](media/6a407ed570f59cdd3ba8db4a20e4a2a1.png)

Runloop事件类型
---------------

RunLoop主要处理以下6类事件回调：

>   static void
>   \__CFRUNLOOP_IS_CALLING_OUT_TO_AN_OBSERVER_CALLBACK_FUNCTION__();

>   static void \__CFRUNLOOP_IS_CALLING_OUT_TO_A_BLOCK__();

>   static void \__CFRUNLOOP_IS_SERVICING_THE_MAIN_DISPATCH_QUEUE__();

>   static void \__CFRUNLOOP_IS_CALLING_OUT_TO_A_TIMER_CALLBACK_FUNCTION__();

>   static void \__CFRUNLOOP_IS_CALLING_OUT_TO_A_SOURCE0_PERFORM_FUNCTION__();

>   static void \__CFRUNLOOP_IS_CALLING_OUT_TO_A_SOURCE1_PERFORM_FUNCTION__();

1.  Observer事件：runloop中状态变化时进行通知。比如，CAAnimation是由RunloopObserver触发回调来布局和重绘。

2.  Block事件：非延迟的[NSObject performSelector:]，CFRunLoopPerformBlock()。

3.  Main_Dispatch_Queue事件：GCD中dispatch到main queue的block会被dispatch到main
    loop执行。比如dispatch_async,dispatch_after。

4.  Timer事件：延迟的[NSObject performSelector:]，timer事件。

5.  Source0事件：处理如UIEvent，CFSocket这类事件。触摸事件其实是Source1接收系统事件后在回调
    \__IOHIDEventSystemClientQueueCallback()内触发的 Source0，Source0
    再触发的_UIApplicationHandleEventQueue()。source0一定是要唤醒runloop及时响应并执行的，如果runloop此时在休眠等待系统的
    mach_msg事件，那么就会通过source1来唤醒runloop执行。自定义的Source0事件，需要手动触发：

>   CFRunLoopSourceSignal(source); //触发事件

>           CFRunLoopWakeUp(runloop);//唤醒runloop处理事件

1.  Source1事件：系统内核的mach_msg事件。比如，CADisplayLink。

所有的事件可以分为两类：

1.  非基于port的事件：Observer事件、Block事件、Source0事件

2.  基于port的事件: Timer事件、Dispatch事件、Source1事件

**9.RunLoop应用举例**

**9.1 输入源**

9.1.1 performSelector方式：

performSelector同样是触发Source0事件。selector也是特殊的基于自定义的源.理论上来说,允许在当前线程向任何线程上执行发送消息,和基于端口的源一样,执行selector请求会在目标线程上序列化,减缓许多在线程上允许多个方法容易引起的同步问题.不像基于端口的源,一个selector执行完后会自动从run
loop里面移除.

主线程执行

>   dispatch_async(dispatch_get_global_queue(0, 0), \^{

>       [self performSelectorOnMainThread:\@selector(test) withObject:nil
>   waitUntilDone:YES];

>   })

当前线程延时执行

>   //
>   内部会创建一个Timer到当前线程的runloop中（如果当前线程没runloop则方法无效；performSelector:onThread:
>   方法放到指定线程runloop中）

>   \- (void)performSelector:(SEL)aSelector withObject:(id)anArgument
>   afterDelay:(NSTimeInterval)delay;

当调用上述API，实际上其内部会创建一个 Timer 并添加到当前线程的 RunLoop
中。所以如果当前线程没有 RunLoop，则这个方法会失效。

指定线程执行

>   \- (void)performSelector:(SEL)aSelector onThread:(NSThread \*)thr
>   withObject:(id)arg waitUntilDone:(BOOL)wait;

当调用 performSelector:onThread:
时，实际上其会创建一个Timer加到对应的线程去，同样的，如果对应线程没有 RunLoop
该方法也会失效.

当前线程指定mode name并延时执行

>   // 只在NSDefaultRunLoopMode下执行(刷新图片)

>   [self.myImageView performSelector:\@selector(setImage:) withObject:[UIImage
>   imageNamed:\@""] afterDelay:ti inModes:\@[NSDefaultRunLoopMode]]

**9.1.2 自定义输入源**

自定义源：自定义输入源则监听自定义的事件源。使用CFRunLoopSourceRef
类型相关的函数 (线程) 来创建自定义输入源。

调用VC

>   \-(void)test {

>       NSThread\* aThread = [[NSThread alloc] initWithTarget:self
>   selector:\@selector(testForCustomSource) object:nil];

>       self.aThread = aThread;

>       [aThread start];

>   }

>   \-(void)testForCustomSource{

>       NSLog(\@"starting thread.......");

>       NSRunLoop \*myRunLoop = [NSRunLoop currentRunLoop];

>       // 设置Run Loop observer的运行环境

>       CFRunLoopObserverContext context = {0, (__bridgevoid \*)(self), NULL,
>   NULL, NULL};

>       // 创建Run loop observer对象

>       CFRunLoopObserverRef observer =
>   CFRunLoopObserverCreate(kCFAllocatorDefault,kCFRunLoopAllActivities, YES, 0,
>   &myRunLoopObserver, \&context);

>       if (observer){

>           CFRunLoopRef cfLoop = [myRunLoop getCFRunLoop];

>           CFRunLoopAddObserver(cfLoop, observer, kCFRunLoopDefaultMode);

>       }

>       \_source = [[ZXRunLoopSource alloc] init];

>       [_source addToCurrentRunLoop];

>       while (!self.aThread.isCancelled)

>       {

>           NSLog(\@"We can do other work");

>           [myRunLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate
>   dateWithTimeIntervalSinceNow:5.0f]];

>       }

>       [_source invalidate];

>       NSLog(\@"finishing thread.........");

自定义输入源

>   \- (id)init

>   {

>       CFRunLoopSourceContext  context = {0, (__bridgevoid \*)(self), NULL,
>   NULL, NULL, NULL, NULL,

>           &RunLoopSourceScheduleRoutine,

>           RunLoopSourceCancelRoutine,

>           RunLoopSourcePerformRoutine};

>       \_runLoopSource = CFRunLoopSourceCreate(NULL, 0, \&context);

>       \_commands = [[NSMutableArray alloc] init];

>       returnself;

>   }

>   \- (void)addToCurrentRunLoop

>   {

>       //获取当前线程的runLoop(辅助线程)

>       CFRunLoopRef runLoop = CFRunLoopGetCurrent();

>       CFRunLoopAddSource(runLoop, \_runLoopSource, kCFRunLoopDefaultMode);

>   }

>   /\*\*

>    \*  调度例程

>    \*  当将输入源安装到run
>   loop后，调用这个协调调度例程，将源注册到客户端（可以理解为其他线程）

>    \*

>    \*/

>   void RunLoopSourceScheduleRoutine (void \*info, CFRunLoopRef rl, CFStringRef
>   mode)

>   {

>       ZXRunLoopSource \*obj = (__bridgeZXRunLoopSource\*)info;

>       //    AppDelegate\*   delegate = [[AppDelegate sharedAppDelegate];

>       AppDelegate \*delegate = [[UIApplication sharedApplication] delegate];

>       RunLoopContext \*theContext = [[RunLoopContext alloc] initWithSource:obj
>   andLoop:rl];

>       //发送注册请求

>       [delegate performSelectorOnMainThread:\@selector(registerSource:)
>   withObject:theContext waitUntilDone:YES];

>   }

>   /\*\*

>    \*  处理例程

>    \*  在输入源被告知（signal
>   source）时，调用这个处理例程，这儿只是简单的调用了 [obj sourceFired]方法

>    \*

>    \*/

>   void RunLoopSourcePerformRoutine (void \*info)

>   {

>       ZXRunLoopSource\*  obj = (__bridge ZXRunLoopSource\*)info;

>       [obj sourceFired];

>       //    [NSTimer scheduledTimerWithTimeInterval:1.0 target:obj
>   selector:\@selector(timerAction:) userInfo:nil repeats:YES];

>   }

>   /\*\*

>    \*  取消例程

>    \* 
>   如果使用CFRunLoopSourceInvalidate/CFRunLoopRemoveSource函数把输入源从run
>   loop里面移除的话，系统会调用这个取消例程，并且把输入源从注册的客户端（可以理解为其他线程）里面移除

>    \*

>    \*/

>   void RunLoopSourceCancelRoutine (void \*info, CFRunLoopRef rl, CFStringRef
>   mode)

>   {

>       ZXRunLoopSource\* obj = (__bridge ZXRunLoopSource\*)info;

>       AppDelegate\* delegate = [AppDelegate sharedAppDelegate];

>       RunLoopContext\* theContext = [[RunLoopContext alloc] initWithSource:obj
>   andLoop:rl];

>       [delegate performSelectorOnMainThread:\@selector(removeSource:)
>   withObject:theContext waitUntilDone:NO];

>   }

>   \- (void)sourceFired

>   {

>       NSLog(\@"Source fired: do some work, dude!");

>       NSThread \*thread = [NSThread currentThread];

>       [thread cancel];

>      
>   //既然线程没了，就把AppDelegate缓存的runloop也给删了，以免下次调用CFRunLoopWakeUp(runloop);会崩溃，因为只有runloop没了线程

>       [[AppDelegate sharedAppDelegate].sources removeObjectAtIndex:0];

**9.1.3 端口输入源**

端口输入源：基于端口的输入源监听程序相应的端口，需要配置 NSMachPort 对象。

为了和 NSMachPort
对象建立稳定的本地连接，你需要创建端口对象并将之加入相应的线程的 run
loop。当运行辅助线程的时候，你传递端口对象到线程的主体入口点。辅助线程可以使用相同的端口对象将消息返回给原线程。

VC调用

>   \- (void)launchThreadForPort

>   {

>       NSPort\* myPort = [NSMachPort port];

>       if (myPort)

>       {

>           //这个类持有即将到来的端口消息

>           [myPort setDelegate:self];

>           //将端口作为输入源安装到当前的 runLoop

>           [[NSThread currentThread] setName:\@"launchThreadForPort---Thread"];

>           [[NSRunLoop currentRunLoop] addPort:myPort
>   forMode:NSDefaultRunLoopMode];

>           //当前线程去调起工作线程

>           MyWorkerClass \*work = [[MyWorkerClass alloc] init];

>           [NSThread detachNewThreadSelector:\@selector(launchThreadWithPort:)
>   toTarget:work withObject:myPort];

>       }

为了在线程间建立双向的通信，你需要让工作线程在签到的消息中发送自己的本地端口到主线程。主线程接收到签到消息后就可以知道辅助线程运行正常，并且供了发送消息给辅助线程的方法。

以下代码显示了主线程的 handlePortMessage:
方法。当由数据到达线程的本地端口时，该方法被调用。当签到消息到达时，此方法可以直接从辅助线程里面检索端口并保存下来以备后续使用。

VC实现代理

>   //NSPortDelegate

>   \#define kCheckinMessage 100

>   //处理从工作线程返回的响应

>   \- (void) handlePortMessage: (id)portMessage {

>       //消息的 id

>       unsignedint messageId = (int)[[portMessage valueForKeyPath:\@"msgid"]
>   unsignedIntegerValue];

>       if (messageId == kCheckinMessage) {

>           //1. 当前主线程的port

>           NSPort \*localPort = [portMessage valueForKeyPath:\@"localPort"];

>           //2. 接收到消息的port（来自其他线程）

>           NSPort \*remotePort = [portMessage valueForKeyPath:\@"remotePort"];

>           //3. 获取工作线程关联的端口，并设置给远程端口，结果同2

>           NSPort \*distantPort = [portMessage valueForKeyPath:\@"sendPort"];

>           NSMutableArray \*arr = [[portMessage valueForKeyPath:\@"components"]
>   mutableCopy];

>           if ([arr objectAtIndex:0]) {

>               NSData \*data = [arr objectAtIndex:0];

>               NSString \* str  =[[NSString alloc] initWithData:data
>   encoding:NSUTF8StringEncoding];

>               NSLog(\@"");

>           }

>           NSLog(\@"");

>           //为了以后的使用保存工作端口

>           //        [self storeDistantPort: distantPort];

>       } else {

>           //处理其他的消息

>       }

>   }

对于辅助工作线程，你必须配置线程使用特定的端口以发送消息返回给主要线程。

以下显示了如何设置工作线程的代码。创建了线程的自动释放池后，紧接着创建工作对象驱动线程运行。工作对象的
sendCheckinMessage: 方法创建了工作线程的本地端口并发送签到消息回主线程。

MyWorkerClass.m

>   \- (void)launchThreadWithPort:(NSPort \*)port {

>       \@autoreleasepool {

>           //1. 保存主线程传入的port

>           remotePort = port;

>           //2. 设置子线程名字

>           [[NSThread currentThread] setName:\@"MyWorkerClassThread"];

>           //3. 开启runloop

>           [[NSRunLoop currentRunLoop] run];

>           //4. 创建自己port

>           myPort = [NSPort port];

>           //5.

>           myPort.delegate = self;

>           //6. 将自己的port添加到runloop

>           //作用1、防止runloop执行完毕之后推出

>           //作用2、接收主线程发送过来的port消息

>           [[NSRunLoop currentRunLoop] addPort:myPort
>   forMode:NSDefaultRunLoopMode];

>           //7. 完成向主线程port发送消息

>           [self sendPortMessage];

>       }

>   }

当使用 NSMachPort
的时候，本地和远程线程可以使用相同的端口对象在线程间进行单边通信。换句话说，一个线程创建的本地端口对象成为另一个线程的远程端口对象。

以下代码辅助线程的签到例程，该方法为之后的通信设置自己的本地端口，然后发送签到消息给主线程。它使用
LaunchThreadWithPort: 方法中收到的端口对象做为目标消息。

MyWorkerClass.m

>   \- (void)sendPortMessage {

>       NSString \*str1 = \@"aaa111";

>       NSString \*str2 = \@"bbb222";

>       arr = [[NSMutableArray alloc] initWithArray:\@[[str1
>   dataUsingEncoding:NSUTF8StringEncoding],[str2
>   dataUsingEncoding:NSUTF8StringEncoding]]];

>       //发送消息到主线程，操作1

>       [remotePort sendBeforeDate:[NSDate date]

>                            msgid:kMsg1

>                       components:arr

>                             from:myPort

>                         reserved:0];

>       //发送消息到主线程，操作2

>       //    [remotePort sendBeforeDate:[NSDate date]

>       //                         msgid:kMsg2

>       //                    components:nil

>       //                          from:myPort

>       //                      reserved:0];

**注意：**上述的一个API中 components
不能直接装NSString等数据，必须是NSData或者NSPort及其子类的实例对象。

**9.2 Timer**

Timer的创建有两种写法：

>   // 第一种写法

>   NSTimer \*timer = [NSTimer timerWithTimeInterval:1.0 target:self
>   selector:\@selector(timerUpdate) userInfo:nil repeats:YES];

>   [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];

>   [timer fire];

>   // 第二种写法

>   [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
>   selector:\@selector(timerUpdate) userInfo:nil repeats:YES];

上面的两种写法其实是等价的。第二种写法，默认也是将timer添加到
NSDefaultRunLoopMode 下的，并且会自动fire。

**9.3 observer**监听所有状态，在非主线程（可以看到一个完整的周期）：

>   \+ (void)observerTest {

>       dispatch_async(dispatch_get_global_queue(0, 0), \^{

>           /\*\*

>            param1: 给observer分配存储空间

>            param2: 需要监听的状态类型:kCFRunLoopAllActivities监听所有状态

>            param3:
>   是否每次都需要监听，如果NO则一次之后就被销毁，不再监听，类似定时器的是否重复

>            param4: 监听的优先级，一般传0 param5: 监听到的状态改变之后的回调

>            return: 观察对象

>            \*/

>           CFRunLoopObserverRef observer =
>   CFRunLoopObserverCreateWithHandler(CFAllocatorGetDefault(),
>   kCFRunLoopAllActivities, YES, 0,         \^(CFRunLoopObserverRef observer,
>   CFRunLoopActivity activity) {

>               switch (activity) {

>                   case kCFRunLoopEntry:

>                       NSLog(\@"即将进入runloop");

>                       break;

>                   case kCFRunLoopBeforeTimers:

>                       NSLog(\@"即将处理timer");

>                       break;

>                   case kCFRunLoopBeforeSources:

>                       NSLog(\@"即将处理input Sources");

>                       break;

>                   case kCFRunLoopBeforeWaiting:

>                       NSLog(\@"即将睡眠");

>                       break;

>                   case kCFRunLoopAfterWaiting:

>                       NSLog(\@"从睡眠中唤醒，处理完唤醒源之前");

>                       break;

>                   case kCFRunLoopExit:

>                       NSLog(\@"退出");

>                       break;

>                   default:

>                       break;

>               }

>           });

>           // 没有任何事件源则不会进入runloop

>           [NSTimer scheduledTimerWithTimeInterval:3 target:self
>   selector:\@selector(doFireTimer) userInfo:nil repeats:NO];

>           CFRunLoopAddObserver([[NSRunLoop currentRunLoop] getCFRunLoop],
>   observer, kCFRunLoopDefaultMode);

>           [[NSRunLoop currentRunLoop] run];

>       });

>   }

>   \+ (void)doFireTimer {

>       NSLog(\@"---fire---");

>   }

打印结果：一个完整的周期

![](media/d82248838870c600a93caed954e27a70.png)

**9.4 资源释放**

即将进入睡眠的时候,先释放上一次创建的自动释放池,然后再创建一个新的释放。

**10.FAQ**

1.Run Loop&GCD关系?

在RunLoop 中大量使用到了GCD。具体关联例如：

-   RunLoop的超时机制就是使用GCD的dispatch_source_t来实现的。

-   GCD 中dispatch到main queue的block会被dispatch到main Runloop中执行。

2.CommonModes下面如果和别的重复定义Item，执行一次还是两次?

   
NSRunLoopCommonModes是个伪mode，它标记mode为common的mode后，runloop执行在common标记的mode下时，会把_commonModeItems下的所有item添加当前执行的mode下，同一个
item 可以被同时加入多个 mode，但一个 item 被重复加入同一个 mode
时是不会有效果的，所以CommonModes下面如果和别的重复定义Item，只执行一次。

3.延迟执行performSelecter相关方法是怎样被执行的？在子线程中也是一样的吗？

当调用 NSObject 的 performSelecter:afterDelay: 后，实际上其内部会创建一个 Timer
并添加到当前线程的 RunLoop 中。所以如果当前线程没有 RunLoop，则这个方法会失效。
当调用 performSelector:onThread: 时，实际上其会创建一个 Timer
加到对应的线程去，同样的，如果对应线程没有 RunLoop 该方法也会失效。

4.如何解决Mode切换时item不执行的问题？

有两种解决方案：

-   将item添加到每一个mode中。

-   使用**NSRunLoopCommonModes，**一个 Mode
    可以将自己标记为"Common"属性（通过将其 ModeName 添加到 RunLoop 的
    "commonModes"
    中）。每当 **RunLoop** 的内容发生变化时，**RunLoop** 都会自动将 **\_commonModeItems** 里的
    Source/Observer/Timer 同步到具有 **Common** 标记的所有Mode里。  
      
    CFRunLoopAddCommonMode(CFRunLoopRef runloop, CFStringRef modeName);

5.手动切换主线程runloop到自定义mode下运行，会发生什么？

   
自定义mode不退出的情况下，UI会卡住不动且不响应任何自定义mode之外的事件源，例如滑动、点击等。

6.Run Loop执行的block有什么？

Block事件：非延迟的 [NSObject
performSelector:]（因为延迟的performSelector是封装成一个Timer在runloop中执行的），CFRunLoopPerformBlock()（通过这个方法加入的block直接设置为runloop的blocks链表的head，插队执行）

<https://opensource.apple.com/source/>** 苹果开源库代码下载网址**
