展示悬浮窗是我们常做的行为，这是一个很简单的操作，只需要将一段通用标准的代码写上去即可，忙着赶进度的我们几乎没有时间停下来总结展示悬浮窗的背后所有配置的作用。此次分享就是针对这个情况进行一次弥补。

展示悬浮窗我们首先要创建WindowManager，这个Context可以是Activity本身也可以是Application的Context。

>   WindowManager.windowManager = (WindowManager)
>   context.getSystemService(Context.WINDOW_SERVICE);

接下来就是创建一个展示悬浮窗的LayoutParams

>   WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

再填充一个View对象出来

>   View view = View.inflate(this, R.layout.layout_float_window, null);

然后展示接口

>   windowManager.addView(view, layoutParams);

仅需要这短短的4步，悬浮窗就能展示出来了。

但是这样所展示的悬浮窗肯定无法满足我们的需求。我们从最简单的出发，探究一下悬浮窗的各种设置的作用。

针对悬浮窗的所有配置都是通过WindowManager.LayoutParams进行的，这里面被定义好了许多的变量，

>   layoutParams.width = WindowManager.LayoutParams.*MATCH_PARENT*;

>   layoutParams.height = WindowManager.LayoutParams.*MATCH_PARENT*;

>   layoutParams.gravity = Gravity.*TOP*;

>   layoutParams.type = WindowManager.LayoutParams.*TYPE_PHONE*;

>   layoutParams.format = PixelFormat.*RGBA_8888*;

>   ...

如下所示：

一共有40个变量。其中27个我们可直接修改，13个被系统隐藏不能直接修改。

可被直接调用的27个变量如下

1.width

2.height

3.x

4.y

5.gravity

6.type

7.flags

8.format

9.screenOrientation

10.packageName

11.alpha

12.dimAmount

13.horizontalWeight

14.verticalWeight

15.horizontalMargin

16.verticalMargin

17.softInputMode

18.windowAnimations

19.screenBrightness

20.buttonBrightness

21.rotationAnimation

22.token

23.preferredDisplayModeId

24.systemUiVisibilie

25.layoutAnimationParameters

被弃用的2个

1.preferredRefreshRate

2.memoryType

被隐藏的13个变量

1.privateFlags

2.needsMenuKey

3.surfaceInsets

4.hasManualSurfaceInsets

5.preservePreviousSurfaceInsets

6.subtreeSystemUiVisibility

7.hasSystemUiListeners

8.inputFeatures

9.userActivityTimeout

10.accessibilityIdOfAnchor

11.accessibilityTitle

12.hideTimeoutMilliseconds

13.mColorMode

**1.width  2.height**

显示悬浮窗的宽和高的信息，可以是ViewGroup.LayoutParams.MATCH_PARENT或者WRAP_CONTENT也可以是精确的大小。精确大小的单位是像素。

系统给的默认值是ViewGroup.LayoutParams.MATCH_PARENT。

根据自己的需要选择合适的值。

**3.x  4.y**

悬浮窗显示的坐标值。

注意事项：

1.如果width和height都为MATCH_PARENT的话，悬浮窗会填满可显示区域，设置x和y是不生效的。

2.Gravity的默认值是居中的，如果希望x和y的坐标原点是可显示区域的左上角的话，需要给Gravity赋值：

>   layoutParams.gravity = Gravity.TOP \| Gravity.LEFT;

**5.gravity**

字面意思：重力、地心引力。系统会根据这个值在屏幕中放置悬浮窗。

我们通过android.view.Gravity类来给此变量赋值。Gravity类是用于将对象放置在更大容器内的标准常量和工具类。

默认值是NO_GRAVITY。常用的值是：TOP \| LEFT \| RIGHT \| BOTTOM \| CENTER \|
CENTER_VERTICAL \| CENTER_HORIZONTAL

**6.type**

悬浮窗的类型，一共有三种：

1.应用程序窗口

取值范围从FIRST_APPLICATION_WINDOW到LAST_APPLICATION_WINDOW，是正常的的应用程序的悬浮窗，这些类型的悬浮窗，token变量必须设置为它们所属的Activity。

2.子窗口

取值范围从FIRST_SUB_WINDOW到LAST_SUB_WINDOW，与另一个悬浮窗相关联，这些类型的悬浮窗token必须是它所附加的窗口的token。

3.系统窗口

取值范围从FIRST_SYSTEM_WINDOW到LAST_SYSTEM_WINDOW，是特殊类型的悬浮窗，供系统使用，它们通常不应被应用程序使用，并且需要特殊权限才能使用它们。

**TYPE_BASE_APPLICATION**

Constant Value: 1 (0x00000001)

一个所有程序的基础window，所有其他程序都显示在其上面

**TYPE_APPLICATION**

Constant Value: 2 (0x00000002)

一个普通的应用window，它的token必须是Activity的token，用来表示window的归属

**TYPE_APPLICATION_STARTING**      

Constant Value: 3 (0x00000003)

特殊的程序window，用于在程序启动的时候显示,不是给程序使用的

当程序可以显示自己的window之前系统会使用这个window来显示Something

**TYPE_DRAWN_APPLICATION**

Constant Value: 4 (0x00000004)

一个TYPE_APPLICATION 的变形,

当应用显示之前,用来保证windowmanager会等待这个window绘制完毕

**TYPE_APPLICATION_PANEL**

Constant Value: 1000 (0x000003e8)

这种window相当于一个至于程序window顶部的panel,显示在依附的window上面

**TYPE_APPLICATION_MEDIA**

Constant Value: 1001 (0x000003e9)

这种window用来显示media(比如视频),显示在依附的window下面

**TYPE_APPLICATION_SUB_PANEL**

Constant Value: 1002 (0x000003ea)

这是相当于一个子panel,显示在依附的window上面,并且也显示在任何其他TYPE_APPLICATION_PANEL类型的window上面

**TYPE_APPLICATION_ABOVE_SUB_PANEL**

constant value: 1005

貌似官方网站网站上没有注解,但我在AS中看到了注释

显示在依附的window上面,且顾名思义显示在所有TYPE_APPLICATION_SUB_PANEL的上面

**TYPE_APPLICATION_ATTACHED_DIALOG**

Constant Value: 1003 (0x000003eb)

类似于 TYPE_APPLICATION_PANEL ,不过是作为顶层window,而不是作为一个子window

**TYPE_STATUS_BAR**

Constant Value: 2000 (0x000007d0)

这个window是用来显示状态栏的,只可能有一个状态栏window,它被放置在屏幕的最上方,所有的其他window都在它的下方

**TYPE_SEARCH_BAR**

Constant Value: 2001 (0x000007d1)

searchbar的window,只可能有一个searchbar的window

**TYPE_PHONE**

Constant Value: 2002 (0x000007d2)

这不是一个程序的窗口,它用来提供与用户交互的界面(特别是接电话的界面),这个window通常会置于所有程序window之上,但是会在状态栏之下

**TYPE_SYSTEM_ALERT**       

Constant Value: 2003 (0x000007d3)

系统window,比如低电量警告之类的,这个window通常在所有应用window之上

**TYPE_TOAST**

Constant Value: 2005 (0x000007d5)

这个window用来显示短暂的通知,比如toast之类的

**TYPE_SYSTEM_OVERLAY**

Constant Value: 2006 (0x000007d6)

这个window会显示在所有东西之上,系统用来覆盖屏幕用的,这个window最好不要获取焦点,不然会影响keyguard的正常使用

**TYPE_PRIORITY_PHONE**

Constant Value: 2007 (0x000007d7)

高优先级的UI,即使keyguard处于激活状态也要显示它,最好不要获取焦点

**TYPE_STATUS_BAR_PANEL**

Constant Value: 2014 (0x000007de)

状态栏的下拉界面

**TYPE_SYSTEM_DIALOG**

Constant Value: 2008 (0x000007d8)

状态栏的下拉界面显示的dialog

**TYPE_KEYGUARD_DIALOG**

Constant Value: 2009 (0x000007d9)

锁屏界面显示的对话框

**TYPE_SYSTEM_ERROR**

Constant Value: 2010 (0x000007da)

系统内部错误,显示在所有东西上面

**TYPE_INPUT_METHOD**

Constant Value: 2011 (0x000007db)

内部输入法window,显示在普通的UI之上,

当这个window显示的时候,为了保证这个window获取到焦点,Application的window会被重新测绘

**TYPE_INPUT_METHOD_DIALOG**

Constant Value: 2012 (0x000007dc)

输入法的对话框,显示在输入法的window之上

**7.flags**

各种行为选项/标志。默认为none。我们可以设置的Flag一共有31个，如下所示：

**FLAG_ALLOW_LOCK_WHILE_SCREEN_ON**

设置后，只要该悬浮窗对用户可见，允许在屏幕打开时激活锁定屏幕。
这可以单独使用，也可以与FLAG_KEEP_SCREEN_ON 和/或 FLAG_SHOW_WHEN_LOCKED 结合使用

**FLAG_DIM_BEHIND**

设置后，此窗口后面的所有内容都将变暗。使用dimAmount来控制暗淡的程度

**FLAG_BLUR_BEHIND**

设置后，模糊此窗口后面的所有内容。

此Flag已经被弃用，系统已经不再支持模糊了

**FLAG_NOT_FOCUSABLE**

设置后，悬浮窗不可聚焦，此悬浮窗不会获得键输入焦点，因此用户无法向其发送键或其他按钮事件。
相反，它们会转到它背后的任何可聚焦窗口。 

无论是否明确设置，此标志都将启用FLAG_NOT_TOUCH_MODAL。

设置此标志还意味着悬浮窗不需要与输入法交互，因此它将是Z顺序和独立于任何活动输入方法定位（通常这意味着它在Z之上排序）
输入法，因此它可以使用全屏显示其内容并在需要时覆盖输入法。可以使用FLAG_ALT_FOCUSABLE_IM来修改此行为。

**FLAG_NOT_TOUCHABLE**

设置后，此悬浮窗永远不会接收触摸事件

**FLAG_NOT_TOUCH_MODAL**

设置后，即使此窗口是可聚焦的（未设置FLAG_NOT_FOCUSABLE），也允许悬浮窗外的任何
pointer events 发送到它后面的窗口。 若不设置，将消耗所有 pointer events
本身，无论它们是否在窗口内。

**FLAG_TOUCHABLE_WHEN_WAKING**

设置后，如果按下触摸屏时设备处于睡眠状态，您将收到第一个触摸事件。
通常，系统会消耗第一个触摸事件，在用户无法看到他们正在按下的内容之前。

此Flag已经弃用，无效了。

**FLAG_KEEP_SCREEN_ON**

只要该窗口对用户可见，保持设备屏幕常亮。

**FLAG_LAYOUT_IN_SCREEN**

将悬浮窗放在整个屏幕内，忽略边框周围的装饰（例如状态栏）。
悬浮窗必须正确定位其内容以考虑屏幕装饰。 此标志通常由Window为您设置，如{\@link
Window＃setFlags}中所述。

**FLAG_LAYOUT_NO_LIMITS**

允许窗口延伸到屏幕之外。

**FLAG_FULLSCREEN**

显示此窗口时隐藏所有屏幕装饰（例如状态栏）。 这允许窗口自己使用整个显示空间 -
当设置了此标志的应用程序窗口位于顶层时，状态栏将被隐藏。 对于窗口的{\@link
\#softInputMode}字段，全屏窗口将忽略{\@link \#SOFT_INPUT_ADJUST_RESIZE}的值;
窗口将保持全屏，不会调整大小。

可以通过{android.R.attr.windowFullscreen}属性在您的主题中控制此标志;
此属性将在标准全屏主题中自动为您设置

例如：

android.R.style.Theme_NoTitleBar_Fullscreen

android.R.style.Theme_Black_NoTitleBar_Fullscreen

android.R.style.Theme_Light_NoTitleBar_Fullscreen

android.R.style.Theme_Holo_NoActionBar_Fullscreen

android.R.style.Theme_Holo_Light_NoActionBar_Fullscreen

**FLAG_FORCE_NOT_FULLSCREEN**

设置后，覆盖FLAG_FULLSCREEN，并强制显示屏幕装饰（例如状态栏）

**FLAG_DITHER**

设置后，此窗口显示到屏幕时开始震动。

此Flag已废弃，不再生效了。

**FLAG_SECURE**

对窗口内容增加安全防范，防止其出现在屏幕截图中或在非安全显示器上查看

有关安全表面和安全显示的更多详细信息，请参阅android.view.Display.FLAG_SECURE

**FLAG_SCALED**

一种特殊模式，其中布局参数用于在合成到屏幕时执行表面缩放。

**FLAG_IGNORE_CHEEK_PRESSES**

它将用于经常使用的窗口，设置后，当用户将屏幕靠在脸上时，将积极地过滤事件流，以防止用户意外按下可能不需要的特定窗口，当检测到这样的事件流时
，应用程序将收到一个CANCEL动作事件，应用程序可以相应地处理此事件，直到手指被释放为止，不对事件采取任何操作。

**FLAG_LAYOUT_INSET_DECOR**

一个特殊选项，仅与FLAG_LAYOUT_IN_SCREEN结合使用。
在屏幕中请求布局时，您的窗口可能会显示在屏幕装饰的顶部或后面，例如状态栏。
通过包含此标志，窗口管理器将报告所需的插入矩形，以确保屏幕装饰不覆盖您的内容。
这个标志通常由Window为你设置，可参阅Window对象的setFlags方法

**FLAG_ALT_FOCUSABLE_IM**

设置后，将反转FLAG_NOT_FOCUSABLE的状态。

如果设置了FLAG_NOT_FOCUSABLE并设置了这个标志，那么窗口的行为就像它需要与输入方法交互一样，因此被置于其后面/远离它;
如果未设置FLAG_NOT_FOCUSABLE并且设置了此标志，那么窗口将表现得好像它不需要与输入方法交互，并且可以放置以使用更多空间并覆盖输入方法

**FLAG_WATCH_OUTSIDE_TOUCH**

如设置了FLAG_NOT_TOUCH_MODAL，则可以将此标志设置为接收带有该操作的单个特殊MotionEvent

用于在窗口外发生的触摸。
请注意，您不会收到完整的向下/移动/向上手势，只会将第一个向下的位置作为ACTION_OUTSIDE。

**FLAG_SHOW_WHEN_LOCKED**

屏幕锁定时显示窗口的特殊标志。
这将使应用程序窗口优先于密钥保护或任何其他锁定屏幕。
可以与FLAG_KEEP_SCREEN_ON一起使用，在显示密钥保护窗口之前直接打开屏幕并显示窗口。
可与FLAG_DISMISS_KEYGUARD一起使用，以自动完全取消非安全密钥保护。
此标志仅适用于最顶层的全屏窗口。

**FLAG_SHOW_WALLPAPER**

要求系统壁纸显示在窗口后面。 窗户表面必须是半透明的，才能真正看到它背后的墙纸;
如果此窗口实际上具有半透明区域，则此标志仅确保壁纸表面将在那里。

可以通过 android.R.attr.windowShowWallpaper 属性在您的主题中控制此标志;
此属性将在标准壁纸主题中自动为您设置

例如：

android.R.style.Theme_Wallpaper_NoTitleBar

android.R.style.Theme_Wallpaper_NoTitleBar_Fullscreen

android.R.style.Theme_Holo_Wallpaper

android.R.style.Theme_Holo_Wallpaper_NoTitleBar

**FLAG_TURN_SCREEN_ON**

设置后，一旦显示窗口，系统将激活电源管理器的用户Activity（就像用户已唤醒设备一样）去打开屏幕。

**FLAG_DISMISS_KEYGUARD**

设置窗口时，只有当它不是安全锁定键盘锁时才会解除键盘锁定。
因为安全性不需要这样的键盘锁，所以如果用户导航到另一个窗口，它将永远不会重新出现（与{\@link
\#FLAG_SHOW_WHEN_LOCKED}形成对比，后者只会暂时隐藏安全和非安全密钥保护，但确保它们重新出现
当用户移动到另一个不隐藏它们的UI时）。
如果键盘锁当前处于活动状态并且安全（需要解锁凭证），则用户在看到此窗口之前仍需要确认它，除非还设置了{\@link
\#FLAG_SHOW_WHEN_LOCKED}

此已经弃用了，请改用{\@link \#FLAG_SHOW_WHEN_LOCKED}或{\@link KeyguardManager
\#dismissKeyguard}。

**FLAG_SPLIT_TOUCH**

设置后，窗口将接受其边界之外的触摸事件，以发送到也支持分割触摸的其他窗口。
如果未设置此标志，则向下的第一个 pointer 将确定所有后续触摸所在的窗口，直到所有
pointer 都上升为止。 当设置该标志时，每个向下的 pointer（不一定是第一个）确定该
pointer 的所有后续触摸将进入的窗口，直到该 pointer 上升，从而使得多个 pointer
的触摸能够在多个窗口之间被分割。

**FLAG_HARDWARE_ACCELERATED**

指示此窗口应使用硬件加速。请求硬件加速但并不能保证它会发生。

**FLAG_LAYOUT_IN_OVERSCAN**

允许窗口内容扩展到屏幕的过扫描区域（如果有）。窗口仍应正确定位其内容以考虑过扫描区域。

可以通过android.R.attr.windowOverscan属性在您的主题中控制此标志;

此属性在标准过扫描主题中自动设置，例如：

android.R.style＃Theme_Holo_NoActionBar_Overscan

android.R.style＃Theme_Holo_Light_NoActionBar_Overscan

android.R.style＃Theme_DeviceDefault_NoActionBar_Overscan

android.R.style＃Theme_DeviceDefault_Light_NoActionBar_Overscan}

如果该标志针对窗口启用，则其正常内容可能在某种程度上被显示器的过扫描区域遮挡。要确保用户可以看到该内容的关键部分，可以使用View＃setFitsSystemWindows（boolean）View.setFitsSystemWindows（boolean）来设置视图层次结构中应该应用适当偏移的点

**FLAG_TRANSLUCENT_STATUS**

请求半透明状态栏，系统提供最少的背景保护。

可以通过android.R.attr \#windowTranslucentStatus属性在您的主题中控制此标志; 

此属性将在标准半透明装饰主题中自动为您设置，例如：

android.R.style＃Theme_Holo_NoActionBar_TranslucentDecor

android.R.style＃Theme_Holo_Light_NoActionBar_TranslucentDecor

android.R.style＃Theme_DeviceDefault_NoActionBar_TranslucentDecor

android.R.style＃Theme_DeviceDefault_Light_NoActionBar_TranslucentDecor

为窗口启用此标志后，它会自动设置系统UI可见性标志
View＃SYSTEM_UI_FLAG_LAYOUT_STABLE 和 View＃SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

**FLAG_TRANSLUCENT_NAVIGATION**

请求半透明导航栏，系统提供最少的背景保护。

可以通过android.R.attr \#windowTranslucentNavigation属性在您的主题中控制此标志; 

此属性将在标准半透明装饰主题中自动为您设置，例如：

android.R.style＃Theme_Holo_NoActionBar_TranslucentDecor

android.R.style＃Theme_Holo_Light_NoActionBar_TranslucentDecor

android.R.style＃Theme_DeviceDefault_NoActionBar_TranslucentDecor

android.R.style＃Theme_DeviceDefault_Light_NoActionBar_TranslucentDecor

为窗口启用此标志后，它会自动设置系统UI可见性标志View＃SYSTEM_UI_FLAG_LAYOUT_STABLE
和 View＃SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION

**FLAG_LOCAL_FOCUS_MODE**

设置后，在本地对焦模式下标记窗口。 局部焦点模式下的窗口可以使用Window
\#setLocalFocus（boolean，boolean）独立于窗口管理器控制焦点。 

通常，此模式下的窗口不会从窗口管理器获取触摸/键事件，但只能通过使用Window＃injectInputEvent（InputEvent）的本地注入来获取事件

**FLAG_LAYOUT_ATTACHED_IN_DECOR**

当请求具有附加窗口的布局时，附加窗口可能与父窗口（例如导航栏）的屏幕装饰重叠。
通过包含此标志，窗口管理器将在父窗口的装饰框架内布置附加窗口，使其不与屏幕装饰重叠

**FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS**

指示此窗口负责绘制系统栏背景的标志。
如果设置，系统栏将以透明背景绘制，此窗口中的相应区域将填充Window＃getStatusBarColor（）和
Window＃getNavigationBarColor（）中指定的颜色

**8.format**

所需的位图格式。 可能是 android.graphics.PixelFormat
中的常量之一。setColorMode(int)可以覆盖格式的选择。 默认是不透明。

如果没有特殊的目的，使用 PixelFormat.RGBA_8888 即可

**9.screenOrientation**

窗口的特定方向值。 可以是 android.content.pm.ActivityInfo＃screenOrientation
允许的任何相同值。 

如果未设置，默认值是
android.content.pm.ActivityInfo＃SCREEN_ORIENTATION_UNSPECIFIED

**SCREEN_ORIENTATION_UNSET**

\@hide 内部常量，用于指示应用程序未设置特定方向值

**SCREEN_ORIENTATION_UNSPECIFIED**

未指明的，未详细说明的，是默认值

**SCREEN_ORIENTATION_LANDSCAPE**

设置为保持横屏显示

**SCREEN_ORIENTATION_PORTRAIT**

设置为保持竖屏显示，但遇到只能横屏的应用，会出现故障

**SCREEN_ORIENTATION_USER**

设置为用户当前的首选方向

**SCREEN_ORIENTATION_BEHIND**

继承Activity堆栈中当前Activity下面的那个Activity的方向

**SCREEN_ORIENTATION_SENSOR**

由物理感应器决定显示方向

**SCREEN_ORIENTATION_NOSENSOR**

忽略物理感应器，显示方向与物理感应器无关

**SCREEN_ORIENTATION_SENSOR_LANDSCAPE**

保持横屏，由重力传感器决定是哪个方向的横屏

**SCREEN_ORIENTATION_SENSOR_PORTRAIT**

保持竖屏，由重力传感器决定是哪个方向的竖屏

**SCREEN_ORIENTATION_REVERSE_LANDSCAPE**

反向横屏

**SCREEN_ORIENTATION_REVERSE_PORTRAIT**

反向竖屏（倒拿手机的效果）

**SCREEN_ORIENTATION_FULL_SENSOR**

由重力传感器决定0/90/180/270°

**SCREEN_ORIENTATION_USER_LANDSCAPE**

用户和重力传感器共同决定是哪个方向的横屏

**SCREEN_ORIENTATION_USER_PORTRAIT**

用户和重力传感器共同决定是哪个方向的竖屏

**SCREEN_ORIENTATION_FULL_USER**

显示方向完全由用户来定

**SCREEN_ORIENTATION_LOCKED**

锁定屏幕当前方向

**10.packageName**

此窗口的名称，默认值是null

**11.alpha**

整个窗口的alpha值，0 到 1.0

**12.dimAmount**

设置暗淡的数量，0 到 1.0，1.0暗淡到完全不透明，0.0表示没有暗淡效果

**13.horizontalWeight**

**14.verticalWeight**

表示分配给悬浮窗的空间有多大，在横纵方向上为相关的View按权重预留扩展像素，如果不应拉伸视图，请指定0；否则，额外像素将在权重大于0的所有视图中按比例分配

**15.horizontalMargin**

**16.verticalMargin**

分辨表示横竖两个方向的边距，容器与widget之间的距离，占容器宽度的百分率。

**17.softInputMode**

输入法区域的操作模式，可以是一项模式的组合

**SOFT_INPUT_MASK_STATE**

用于描述软键盘显示规则的bite的mask

**SOFT_INPUT_STATE_UNSPECIFIED**

输入法的可见状态为: 未指定状态，没有约定规则

**SOFT_INPUT_STATE_UNCHANGED**

输入法的可见状态为:不改变输入法当前状态

**SOFT_INPUT_STATE_HIDDEN**

输入法的可见状态为:用户进入窗口时，隐藏所有输入法

**SOFT_INPUT_STATE_ALWAYS_HIDDEN**

输入法的可见状态为:窗口获取焦点是，隐藏所有输入法

**SOFT_INPUT_STATE_VISIBLE**

输入法的可见状态为:用户进入窗口后，显示输入法

**SOFT_INPUT_STATE_ALWAYS_VISIBLE**

输入法的可见状态为:当窗口获取输入焦点时，显示输入法

**SOFT_INPUT_MASK_ADJUST**

定窗口是否应该根据输入法进行调整的代码

**SOFT_INPUT_ADJUST_UNSPECIFIED**

窗口调整设置为: 未指定, 系统会尝试进行选择

**SOFT_INPUT_ADJUST_RESIZE**

窗口调整设置为: 当显示输入法时, 允许窗口被缩放, 使得窗口的内容不会被输入法覆盖

**SOFT_INPUT_ADJUST_PAN**

窗口调整设置为: 当显示输入法时, 移动窗口使得输入焦点可见, 而不会缩放窗口

**SOFT_INPUT_ADJUST_NOTHING**

窗口调整设置为: 当显示输入法时, 既不缩放, 也不移动，不调整窗口的布局

**SOFT_INPUT_IS_FORWARD_NAVIGATION**

用户导航到此窗口时的配置代码. 通常有系统配置, 除非你需要自定义. 当窗口显示后,
该配置会清除

**18.windowAnimations**

效果类似这种style，\<item
name="android:windowAnimationStyle"\>\@android:style/Animation.Dialog\</item\>

用于此Window的动画的样式资源。
这是对整个View的一个动画，而不是View中某个控件的动画。而且，使用的时候需要在View状态改变的时候才会出现动画效果。比如消失/出现的时候才会有动画效果。

**19.screenBrightness**

这可以用于覆盖用户首选的屏幕亮度。 默认值小于0表示使用首选屏幕亮度。
0到1可将亮度从暗调整为全亮。

**BRIGHTNESS_OVERRIDE_NONE**

默认值，表示此窗口未覆盖亮度值，应使用正常亮度策略。

**BRIGHTNESS_OVERRIDE_OFF**

表示当此窗口在前面时，屏幕或按钮背光亮度应设置为最低值。

**BRIGHTNESS_OVERRIDE_FULL**

表示当此窗口在前面时，屏幕或按钮背光亮度应设置为最高值。

**20.buttonBrightness**

这可用于覆盖按钮和键盘背光的标准行为。 默认值小于0表示使用标准背光行为。
0到1可将亮度从暗调整为全亮。取值也是上面三条。

**21.rotationAnimation**

用于确定屏幕旋转时的动画. 是否有效取决于手机

默认为 ROTATION_ANIMATION_ROTATE

**ROTATION_ANIMATION_ROTATE**

旋转动画

**ROTATION_ANIMATION_CROSSFADE**

有淡入淡出效果

**ROTATION_ANIMATION_JUMPCUT**

立刻切换, 没有动画

**ROTATION_ANIMATION_SEAMLESS**

用于指定无缝旋转模式，结余淡入淡出和立即切换之间。
这类似于JUMPCUT，但如果在不暂停屏幕的情况下无法应用旋转，它将回退到CROSSFADE。
例如，对于不希望取景器内容旋转或淡化（而非无缝）的相机应用程序而言，这是理想的选择，但在无法应用无缝旋转的应用程序转换场景中也不希望ROTATION_ANIMATION_JUMPCUT

**22.token**

此窗口的标识符。
通常由应用程序来传入。token是用来表示窗口的一个令牌，只有符合条件的token才能被WMS通过添加到应用上。

之前讲了三种类型的悬浮窗：应用程序窗口类型、子窗口类型、系统窗口类型

在应用程序窗口中，token是用来标识Activity的，一个Activity就对应一个token令牌

而在子窗口中，某个子窗口想要依附在对应的宿主窗口上设置要将token设置为对应宿主窗口的token

系统窗口类型，可以不加。

**23.preferredDisplayModeId**

窗口的首选显示模式的ID。这必须是窗口所在的显示器所支持的模式之一。 0表示没有偏好

**24.systemUiVisibilie**

控制状态栏的可见性。

**View.STATUS_BAR_VISIBLE**

**View.STATUS_BAR_HIDDEN**

上面这两条已经被废弃了，取代的是：

**SYSTEM_UI_FLAG_VISIBLE：**显示状态栏，Activity不全屏显示(恢复到有状态的正常情况)。

**SYSTEM_UI_FLAG_LOW_PROFILE：**状态栏显示处于低能显示状态(low
profile模式)，状态栏上一些图标显示会被隐藏。

或者是其他的值

**View.INVISIBLE**：隐藏状态栏，同时Activity会伸展全屏显示。

**View.SYSTEM_UI_FLAG_FULLSCREEN**：Activity全屏显示，且状态栏被隐藏覆盖掉。

**View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN**：Activity全屏显示，但状态栏不会被隐藏覆盖，状态栏依然可见，Activity顶端布局部分会被状态遮住。

**View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION**：效果同View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

**View.SYSTEM_UI_LAYOUT_FLAGS**：效果同View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

**View.SYSTEM_UI_FLAG_HIDE_NAVIGATION**：隐藏虚拟按键(导航栏)。有些手机会用虚拟按键来代替物理按键。

**25.layoutAnimationParameters**

该属性类是LayoutAnimationController的内部类，封装了子View的索引值和子View的个数，在计算每个子View动画的延迟时间时会用到。

用于为布局设置动画。
