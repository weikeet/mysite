[How does Firebase initialize on Android?](http://firebase.googleblog.com/2016/12/how-does-firebase-initialize-on-android.html)
-------------------------------------------------------------------------------------------------------------------------------

If you've been working with Firebase on Android, you may have noticed that you
don't normally have to write any lines of code to initialize a feature. You just
grab the singleton object for that feature, and start using it right away. And,
in the case of Firebase Crash Reporting, you don't even have to write any code
at all for it to start capturing crashes! This question [pops
up](http://stackoverflow.com/questions/37738445/how-does-firebase-crash-reporting-initialize) from
time to time, and I [talked about it a
bit](https://youtu.be/AJqakuas_6g?t=20m38s) at Google I/O 2016, but I'd also
like to break it down in detail here.

如果你在Android上使用Firebase，可能已经注意到，你通常不需要写任何代码来初始化。你只需获取该功能的单例对象，然后立即开始使用它。而且，在Firebase
Crash
Reporting，你甚至不需要写任何的代码它就已经开始捕捉Crash！这个问题不时地出现，我在Google
I/O 2016中提到过，在此我想详细的解析一下。

**The problem**

Many SDKs need an
Android [Context](https://developer.android.com/reference/android/content/Context.html) to
be able to do their work. This Context is the hook into the Android runtime that
lets the SDK access app resources and assets, use system services, and register
BroadcastReceivers. Many SDKs ask you to pass a Context into a static init
method once, so they can hold and use that reference as long as the app process
is alive. In order to get that Context at the time the app starts up, it's
common for the developers of the SDK to ask you to pass that in a
custom [Application](https://developer.android.com/reference/android/app/Application.html) subclass
like this:

许多Android
SDK需要Context完成他们的工作。以便Android运行时让SDK访问应用程序的资源，使用系统服务，并注册broadcastreceivers。许多SDK通过静态方法传入Context，这样他们就可以在整个Application生命周期内持有这些引用。为了在应用程序启动时获得Context，SDK的开发人员要求你在自定义应用程序子类中传递它。

>   publicclassMyApplicationextendsApplication {

>       \@Override

>       publicvoid onCreate() {

>           super.onCreate();

>           SomeSdk.init(this);  // init some SDK, MyApplication is the Context

>       }

>   }

And if you hadn't already registered a custom subclass in your app, you'd also
have to add that to your manifest in
the [application](https://developer.android.com/guide/topics/manifest/application-element.html) tag's
android:name attribute:

>   \<application

>       android:icon="\@mipmap/ic_launcher"

>       android:label="\@string/app_name"

>       android:name="package.of.MyApplication"

>       ... \>

All this is fine, but Firebase SDKs make this a lot easier for its users!

这些都很好，但是Firebase SDKs为用户提供了一个更简单的方式！

**The solution**

There is a little trick that the Firebase SDKs for Android use to install a hook
early in the process of an application launch cycle. It introduces
a [ContentProvider](https://developer.android.com/reference/android/content/ContentProvider.html) to
implement both the timing and Context needed to initialize an SDK, but without
requiring the app developer to write any code. A ContentProvider is a convenient
choice for two reasons:

1.  They are created and initialized (on the main thread) before *all* other
    components, such as Activities, Services, and BroadcastReceivers, after the
    app process is started.

2.  They participate in manifest merging at build time, if they are declared in
    the manifest of an Android library project. As a result, they are
    automatically added to the app's manifest.

Let's investigate those two properties.

Firebase SDKs巧妙的利用了Android进程启动生命周期，那就是ContentProvider.

1
ContentProvider创建和初始化在进程启动后，其他组件之前，如Activities、Services和BroadcastReceiver。

2
如果ContentProvider在库Manifest中声明，它们将在构建时参与显式合并。因此，它们会自动添加到App的Manifest中。

**ContentProvider initializes early**

When an Android app process is first started, there is well-defined order of
operations:

1.  Each ContentProvider declared in the manifest is created, in [priority
    order](https://developer.android.com/guide/topics/manifest/provider-element.html#init).

2.  The Application class (or custom subclass) is created.

3.  If another component that was invoked via some Intent, that is created.

When a ContentProvider is created, Android will call its onCreate method. This
is where the Firebase SDK can get a hold of a Context, which it does by calling
the [getContext](https://developer.android.com/reference/android/content/ContentProvider.html#getContext()) method.
This Context is safe to hold on to indefinitely.

当ContentProvider创建时，Android将调用它的onCreate方法。Firebase
SDK通过调用getContext方法获取Context。调用getContext()方法是安全的。获取的Context可以保存下来并一直使用。

1 Manifest声明的ContentProvider按照优先级顺序依次创建（android:initOrder）。

2 Application（或者自定义Application子类）创建。

3 其他触发的创建。

android:initOrder

The order in which the content provider should be instantiated, relative to
other content providers hosted by the same process. When there are dependencies
among content providers, setting this attribute for each of them ensures that
they are created in the order required by those dependencies. *The value is a
simple integer, with higher numbers being initialized first.*

This is also a place that can be used to set up things that need to be active
throughout the app's lifetime, such
as [ActivityLifecycleCallbacks](https://developer.android.com/reference/android/app/Application.ActivityLifecycleCallbacks.html)(which
are used by [Firebase Analytics](https://firebase.google.com/docs/analytics/)),
or
a [UncaughtExceptionHandler](https://developer.android.com/reference/java/lang/Thread.UncaughtExceptionHandler.html)(which
is used by [Firebase Crash Reporting](https://firebase.google.com/docs/crash/)).
You might also initialize a dependency injection framework here.

**ContentProviders participate in manifest merger（关于ContentProvider Merge）**

[Manifest
merge](https://developer.android.com/studio/build/manifest-merge.html) is a
process that happens at build time when the Android build tools need to figure
out the contents of the final manifest that defines your app. In your app's
AndroidManifest.xml file, you declare all your application components,
permissions, hardware requirements, and so on. But the final manifest that gets
built into the APK contains all of those elements from *all* of the Android
library projects that your app depends on.

It turns out that ContentProviders are merged into the final manifest as well.
As a result, any Android library project can simply declare a ContentProvider in
its own manifest, and that entry will end up in the app's final manifest. So,
when you declare a dependency on Firebase Crash Reporting, the ContentProvider
from its manifest is merged in your own app's manifest. This ensures that its
onCreate is executed, without you having to write any
code.（库中Manifest文件可以声明ContentProvider，build过程中tool会将所有库及App声明merge到一个Manifest文件中）

**FirebaseInitProvider (surprise!) initializes your app**

All apps using Firebase in some way will have a dependency on the
firebase-common library. This library exposes FirebaseInitProvider, whose
responsibility is to
call [FirebaseApp.initializeApp](https://firebase.google.com/docs/reference/android/com/google/firebase/FirebaseApp.html#initializeApp(android.content.Context)) in
order to initialize the default FirebaseApp instance using the configurations
from the project's google-services.json file. (Those configurations are injected
into the build as Android resources by the [Google Services
plugin](https://developers.google.com/android/guides/google-services-plugin).)
However, If you're referencing multiple Firebase projects in one app, you'll
have to write code to initialize other FirebaseApp instances, as discussed in
an [earlier blog
post](https://firebase.googleblog.com/2016/12/working-with-multiple-firebase-projects-in-an-android-app.html).

Firebase-common
库中有FirebaseInitProvider，它触发FirebaseApp.initializeApp读取google-services.json初始化了FirebaseApp
实例。在build过程中Google Services plugin会加工google-services.json。

**Some drawbacks with ContentProvider init**

If you choose to use a ContentProvider to initialize your app or library,
there's a couple things you need to keep in mind.

First, there can be only one ContentProvider on an Android device with a given
"[authority](https://developer.android.com/guide/topics/manifest/provider-element.html#auth)"
string. So, if your library is used in more than one app on a device, you have
to make sure that they get added with two different authority strings, or the
second app will be rejected for installation. That string is defined for the
ContentProvider in the manifest XML, which means it's effectively hard-coded.
But there is a trick you can use with the Android build tools to make sure that
each app build declares a different authority.

There is a feature of Android Gradle builds call [manifest
placeholders](https://developer.android.com/studio/build/manifest-build-variables.html)that
lets you declare and insert a placeholder value that get inserted into manifest
strings. The app's unique application ID is automatically available as a
placeholder, so you can declare your ContentProvider like this:

ContentProvider
的authority必须不同，如果两个App的authority相同，则后安装的App安装不上，这里可以利用Build
tool处理达到不同。

>   \<provider

>       android:authorities="\${applicationId}.yourcontentprovider"

>       android:name=".YourContentProvider"

>       android:exported="false"/\>

The other thing to know about about ContentProviders is that they are only run
in the main process of an app. For a vast majority of apps, this isn't a
problem, as there is only one process by default. But the moment you declare
that one of the Android components in your app must run in another process, that
process won't create any ContentProviders, which means your ContentProvider
onCreate will never get invoked. In this case, the app will have to either avoid
calling anything that requires the initialization, or safely initialize another
way. Note that this behavior is different than a custom Application subclass,
which *does* get invoked in every process for that app.

**But why misuse ContentProvider like this?**

Yes, it's true, this particular application of ContentProvider seems really
weird, since it's not actually *providing* any *content*. And you have to
provide implementations of all the other ContentProvider required methods by
returning null. But, it turns out that this is the most reliable way to
automatically initialize without requiring extra code. I think the convenience
for developers using Firebase more than makes up for this strangeness of this
use of a ContentProvider. Firebase is all about being easy to use, and there's
nothing easier than no code at all!

**Labels: **[Android
Studio ](http://firebase.googleblog.com/search/label/Android%20Studio), [Crash
Reporting ](http://firebase.googleblog.com/search/label/Crash%20Reporting), [engagement ](http://firebase.googleblog.com/search/label/engagement), [featured ](http://firebase.googleblog.com/search/label/featured), [firebase](http://firebase.googleblog.com/search/label/firebase)

<https://firebase.googleblog.com/2016/12/how-does-firebase-initialize-on-android.html>
