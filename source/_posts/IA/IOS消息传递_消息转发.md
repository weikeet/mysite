1.动态绑定&静态绑定

静态绑定 static binding

\#import \<stdio.h\>

void printHello( ) {

    printf(“Hello, world!\\n”);

}

void printGoodbye( ) {

    printf(“Goodbye, world!\\n”);

}

void doTheThing(int type) {

    if (type == 0) {

    printHello( );

}

else {

    printGoodbye( );

}

    return 0;

}

在不考虑内联函数的情况，编译器在编译代码时就已经知道程序中有printHello和printGoodbye这两个函数了，于是直接生成调用这些函数的指令，这里函数地址其实是硬编码在指令之中的。

动态绑定 dynamic binding

\#import \<stdio.h\>

void printHello( ) {

    printf(“Hello, world!\\n”);

}

void printGoodbye( ) {

    printf(“Goodbye, world!\\n”);

}

void doTheThing(int type) {

    void (\*fnc) ( ); //函数指针

    if (type == 0) {

        fnc = printHello( );

}

else {

        fnc = printGoodbye( );

}

fnc( );

    return 0;

}

函数指针所要调用的函数直到运行期才能确定

在第一个例子中，if与else语句里都有函数调用指令，第二个例子中只有一个函数调用指令，待调用的函数地址无法硬编码在指令之中，只能在运行期读取出来。

OC作为动态语言，就是采用了动态绑定的方法向对象传递消息。

2.objc_msgsend

OC中向对象发送消息：

Id returnValue = [someObjmessageName:parameter];

someObj：接收者receiver

messageName：选择子selector

parameter:  参数

messageName：parameter  二者结合是消息

编译器将消息转换为一条标准的C语言函数调用—objc_msgsend(消息传递函数)：

void objc_msgsend（id self, SEL cmd, ….）

这是参数个数可变的函数，能接受两个或者两个以上的参数

Id self ：接收者

SEL cmd：选择子selector，其实就是方法的名字

…. : 参数, 它们顺序不变

Xcode处理后：

id returnValue = objc_msgSend (  someObj,

  \@selector (messageName:),

      parameter );

objc_msgSend为了完成此操作，它会在接收者所属类中搜索其“方法列表”，如果能找到与选择子名字相符的方法，就跳转至其实现代码；如果找不到就沿着继承体系继续向上查找，等找到合适的方法之后再跳转；如果还是找不到，就执行“消息转发”机制。

为了快速，objc_msgSend会将匹配结果缓存在“快速映射表”里面，若是之后还向该类发送相同的消息执行起来就很快了，每个类都有这样一个缓存。

当然，这种快速执行路径肯定不如“静态绑定的函数调用操作”迅速。

**消息转发**

 对象收到无法解读的消息时的行为准则

消息转发分两大阶段：

![](media/872e4825e356e9f1c9f39d8b4f989d68.png)

1.动态方法解析

询问接受者所属的类是否能动态添加方法，以处理当前这个“未知的选择子”

2.完整的消息转发机制

（1）请接受者看看有没有其他对象（备援接收者）能够处理这条消息

（2）启动完整的消息转发机制，运行期系统会把与消息有关的全部细节都封装到NSInvocation对象中，再给接受者最后一次机会。

动态方法解析

对象将调用其所属类的下面方法：

\+ （BOOL）resolveInstanceMethod:(SEL)selector

这里的参数就是那个未知的选择子，这个方法表示这个类能否新增一个实例方法用于处理此选择子

如果尚未实现的方法是类方法，则调用“resolveClassMethod:”

使用这类方法的前提是：

相关方法的实现代码已经写好，只等着运行的时候插在类里面就可以了，此类方案常用来实现\@dynamic属性

例：实现\@dynamic属性

id autoDictionaryGetter (id self, SEL_cmd);

void autoDictionarySetter (id self, SEL_cmd, id value );

\+ (BOOL) resolveInstanceMethod:(SEL)selector {

    NSString \* selectorString = NSStringFromSelector (selector);

    if ( /\* selector is from a \@dynamic property \*/) {

    if ([selectorString hasPrefix:\@“set”] ) {

    //OC中动态添加方法

    class_addMethod ( self,

   selector,

   (IMP)autoDictionarySetter,

   “v\@:\@“ );    

}

    else {

    class_addMethod ( self,

    selector,

    (IMP)autoDictionaryGetter,

    “\@ \@ :”);

}

    return YES;

}

    return [super resolveInstanceMethod:selector];

}

首先将选择子转化为字符串，然后检测其是否表示设置方法。若前置为set，则表示设置方法，否则就是获取方法。不管哪种情况，都会把处理该选择子的方法加到类里面。

备援接收者

当前接收者的第二次机会去处理未知的选择子

在这里，运行期系统问它能不能把这条消息转发给其他接收者来处理：

\- (id) forwardingTargetForSelector: (SEL)selector

若当前接收者能找到备援对象，就返回该备援对象；反之返回nil

这样，我们可以通过“组合”来模拟出多重继承的某些特性。因为在一个对象内部，可能还有一系列其他对象，该可经由此方法将能够处理选择子的相关内部对象返回，这样在外界看来，好像是该对象亲自处理了这些消息一样。

这一步我们无法操作，若想修改内容，只能在完整的消息转发机制中做

完整的消息转发

首先创建NSInvocation对象，把无法处理消息的全部细节都封于其中，包括选择子，目标和参数。

之后“消息派发系统”亲自出马，把消息派发给目标对象。

此步骤调用以下方法来转发消息：

\- (void) forwardInvocation:(NSInvocation \*)invocation

在触发此消息前可以改变消息内容，如追加另外一个参数或者改换选择子等

继承体系中的每个类都有机会处理此调用请求（只有在这个步骤会调用父类），直至NSObject。如果最后调用了NSObject类的方法，那么该方法还会继而调用“doseNotRecognizeSelector:”抛出异常，声明选择子最终未能得到处理

消息转发全流程
--------------

下图是消息转发全流程图，描述了**消息转发机制**的各个步骤。

![](media/872e4825e356e9f1c9f39d8b4f989d68.png)

接收者在每一步中均有机会处理消息。步骤越往后，处理消息的代价就越大；最好能在第一步就处理完，这样的话，runtime系统就可以将此方法缓存起来，进而提高效率。若想在第三步里把消息转发给备援的接收者，那还不如把转发操作提前到第二步。因为第三步只是修改了调用目标，这项改动放在第二步会更为简单，不然的话，还得创建并处理完整的NSInvocation。

应用实例：

在CoreAnimation框架中，CALayer就用了与动态方法解析相似的实现方式，使得CALayer除了使用存取方法和点语法之外还可以使用字符串做键，通过“valueForKey”与“setValue:forKey:”这种形式来访问属性。

\- (void)setAngle:(CGFloat)angle forHand:(UIView \*)handView
animated:(BOOL)animated

{

    //generate transform

    CATransform3D transform = CATransform3DMakeRotation(angle, 0, 0, 1);

    if (animated) {

        //create transform animation

        CABasicAnimation \*animation = [CABasicAnimation animation];

        [self updateHandsAnimated:NO];

        animation.keyPath = \@"transform";

        animation.toValue = [NSValue valueWithCATransform3D:transform];

        animation.duration = 0.5;

        animation.delegate = self;

        [animation setValue:handView forKey:\@"handView"];

        [handView.layer addAnimation:animation forKey:nil];

    } else {

        //set transform directly

        handView.layer.transform = transform;

    }

}

\- (void)animationDidStop:(CABasicAnimation \*)anim finished:(BOOL)flag

{

    //set final position for hand view

    UIView \*handView = [anim valueForKey:\@"handView"];

    handView.layer.transform = [anim.toValue CATransform3DValue];

}

这意味着你可以对动画用任意类型打标签，更有利于识别是哪个动画，然后在委托方法中用这个信息正确地更新动画
