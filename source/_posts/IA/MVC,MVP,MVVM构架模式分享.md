**一.软件构架的定义**

软件架构是一个系统的草图，是软件的体系结构。软件架构描述的对象是直接构成系统的抽象组件。各个组件之间的连接则明确和相对细致地描述组件之间的通讯。在面向对象领域中，组件之间的连接通常用接口来实现。

    大型WEB项目构架图如下：

![](media/4ff37cb03b82ecad1e689e769dc89657.png)

android系统体系结构图如下：

![](media/8a153354594a133be00ac7a85ee8e086.jpg)

**二.软件构架的目的**

-   可靠性（Reliable）

-   安全性（Secure）

-   可伸缩性（SCAlable）

-   可定制化（CuSTomizable）

-   可扩展性（Extensible）

-   可维护性（MAIntainable）

-   客户体验（Customer Experience）

-   市场时机（Time to Market）

**三.构架模式**

**分类表：**

| 名称                 | 优点                                                                                                                                                                      | 缺点                                                                                                                  |
|----------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------|
| 分层模式             | 一个较低的层可以被不同的层所使用。层使标准化更容易，因为我们可以清楚地定义级别。可以在层内进行更改，而不会影响其他层。                                                    | 不是普遍适用的。在某些情况下，某些层可能会被跳过。                                                                    |
| 客户端-服务器模式    | 很好地建立一组服务，用户可以请求他们的服务。                                                                                                                              | 请求通常在服务器上的单独线程中处理。由于不同的客户端具有不同的表示，进程间通信会导致额外开销。                        |
| 主从设备模式         | 准确性——将服务的执行委托给不同的从设备，具有不同的实现。                                                                                                                  | 从设备是孤立的：没有共享的状态。主-从通信中的延迟可能是一个问题，例如在实时系统中。这种模式只能应用于可以分解的问题。 |
| 管道-过滤器模式      | 展示并发处理。当输入和输出由流组成时，过滤器在接收数据时开始计算。轻松添加过滤器，系统可以轻松扩展。过滤器可重复使用。 可以通过重新组合一组给定的过滤器来构建不同的管道。 | 效率受到最慢的过滤过程的限制。从一个过滤器移动到另一个过滤器时的数据转换开销。                                        |
| 代理模式             | 允许动态更改、添加、删除和重新定位对象，这使开发人员的发布变得透明。                                                                                                      | 要求对服务描述进行标准化。                                                                                            |
| 点对点模式           | 支持分散式计算。对任何给定节点的故障处理具有强大的健壮性。在资源和计算能力方面具有很高的可扩展性。                                                                        | 服务质量没有保证，因为节点是自愿合作的。安全是很难得到保证的。性能取决于节点的数量。                                  |
| 事件总线模式         | 新的发布者、订阅者和连接可以很容易地添加。对高度分布式的应用程序有效。                                                                                                    | 可伸缩性可能是一个问题，因为所有消息都是通过同一事件总线进行的。                                                      |
| 模型-视图-控制器模式 | 可以轻松地拥有同一个模型的多个视图，这些视图可以在运行时连接和断开。                                                                                                      | 增加复杂性。可能导致许多不必要的用户操作更新。                                                                        |
| 黑板模式             | 很容易添加新的应用程序。扩展数据空间的结构很简单。                                                                                                                        | 修改数据空间的结构非常困难，因为所有应用程序都受到了影响。可能需要同步和访问控制。                                    |
| 解释器模式           | 高度动态的行为是可行的。对终端用户编程性提供好处。提高灵活性，因为替换一个解释程序很容易。                                                                                | 由于解释语言通常比编译后的语言慢，因此性能可能是一个问题。                                                            |

**构架模式与设计模式的区别：**

使用权区分：构架模式拥有设计的选择权，而设计模式是在特定场景下选择的最适合实现方式。

    重用对象不同：构架通常是代码的重用，而设计模式是设计重用。

   
策略层次区分：构架模式是对软件系统的系统组织，是对构成系统模块、结构、行为模式、协作关系等体系问题的决策总和，涉及到系统的使用、功能、性能、适应性、重用性、可理解性。而设计模式是一套被反复使用、多数人知晓、代码设计经验的总结，它强调的是一个设计问题的解决方法，是针对解决具体的问题。

**四.软件构架分层模式-MVC,MVP,MVVM构架模式**

MVC，MVP和MVVM是软件构架分层模式的思想，这三种架构模式的目的都是分离关注，避免将过多的逻辑全部堆积在一个或某几个类中。以Android开发为例，在activity中既有UI的相关处理逻辑，又有数据获取逻辑，从而导致activity逻辑复杂不单一难以维护。为了一个应用可以更好的维护和扩展，我们需要很好的区分相关层级，要不然以后将数据获取方式从数据库变为网络获取时，我们需要去修改整个activity。架构使得view和数据相互独立，我们把应用分成三个不同层级，这样我们就能够单独测试相关层级，使用架构能够把大多数逻辑从activity中移除，方便进行单元测试。

**五.软件构架模式之MVC**

    Model View
Controller模式是一种设计典范、设计思想，它用一种业务逻辑、数据、界面显示分离的方法组织代码，将业务逻辑聚集到一个部件里面，在改进和个性化定制界面及用户交互的同时，不需要重新编写业务逻辑。MVC将应用分成三个主要层级：Model，View和Controller，它强制将逻辑进行分离，Model和Controller逻辑与UI是解耦的，所以测试相关模块变的更简单。 

![](media/8d9ef225b2f1ad0c299dafb774eb5a3a.png)

 

模型层（Model）：

   
是应用程序中用于处理应用程序数据逻辑的部分。通常模型对象负责在数据库中存取数据。

   
android中一般是对数据库的操作、对网络等的操作都应该在Model里面处理，当然对业务计算，变更等操作也是必须放在的该层的。

视图层（View）：

是应用程序中处理数据显示的部分。通常View是依据Model数据创建的。

android中一般采用XML文件进行界面的描述，使用的时候可以非常方便的引入，当然，也可以使用JavaScript+HTML等的方式作为View层，他的职责就是负责显示从Controller上获取到的数据（但是xml布局作为View来说功能很无力，所以通常Activity也会承担一部分View的工作）。

控制层（Controller）：

   
是应用程序中处理用户交互的部分。通常控制器负责从View读取数据，控制用户输入，并向Model发送数据。

Android的控制层的重任通常落在了众多的Activity的肩上，他们从模型层获取数据，将获取到的数据绑定到view上，并且还需要监听用户的输入等操作。

MVC模式具体表现在android上的效果如下图所示： 

![](media/3e101d2fa4f27221fd93d68c8ae621b4.png)

 

android的activity就是一个小型MVC的应用。

android 中的adapter也是使用的MVC模式，自定义的adapter相当于Controller。 

例子
----

以一个获取天气的例子来说，xml布局可视为View层；Activity为Controller层，控制用户输入，将Model层获取到的数据展示到View层；Model层的实体类当然就是用来获取网络数据了。 

**Model层** 

WeatherModel.class接口

public interface WeatherModel { 

    void getWeather(OnLoadWeatherCallback callback); 

}

WeatherModelImpl.class类

public class WeatherModelImpl implements WeatherModel{ 

    private Context mContext; 

    public WeatherModelImpl(Context context){ 

        mContext = context;

    }

 

    \@Override 

    public void getWeather(final OnLoadWeatherCallback callback) { 

       
NetApi.getInstance().jsonObjectRequest(mContext, "<http://www.weather.com.cn/data/sk/101010100.html>", 

                new HashMap\<String, String\>(), new
BaseNetApi.OnNetCallback\<JSONObject\>() { 

 

            \@Override 

            public void onSuccess(JSONObject jsonObject) { 

                try { 

                    jsonObject = new
JSONObject(jsonObject.getString("weatherinfo")); 

                    WeatherInfo info = new WeatherInfo(); 

                    info.city = jsonObject.getString("city");

                    info.temp =
Double.parseDouble(jsonObject.getString("temp"));

                    info.WD = jsonObject.getString("WD");

                   [info.WS](http://info.ws/)= jsonObject.getString("WS"); 

                    info.time = jsonObject.getString("time");

                    callback.onLoadSuccess(info);

                } catch (JSONException e) { 

                    L.e(e);

                }

            }

 

            \@Override 

            public void onFail(NetError netError) { 

                callback.onError(netError);

            }

        });

    }

}

**Controller层** 

WeatherActivity.class类

public class WeatherActivity extends BaseActivity implements OnLoadWeatherCallback{ 

    private TextView tv_name; 

    private TextView tv_temperature; 

    private TextView tv_wind_d; 

    private TextView tv_wind_s; 

    private TextView tv_time; 

    private LoadingDialog ld; 

    private WeatherModel weatherModel; 

 

    \@Override 

    protected void onCreate(Bundle savedInstanceState) { 

        super.onCreate(savedInstanceState); 

        setContentView(R.layout.activity_weather);

        tv_name = (TextView) findViewById(R.id.tv_name);

        tv_temperature = (TextView) findViewById(R.id.tv_temperature);

        tv_wind_d = (TextView) findViewById(R.id.tv_wind_d);

        tv_wind_s = (TextView) findViewById(R.id.tv_wind_s);

        tv_time = (TextView) findViewById(R.id.tv_time);

 

        weatherModel = new WeatherModelImpl(this); 

        ld = new LoadingDialog(this); 

        ld.setLoadingText("正在获取天气...");

        ld.show();

        weatherModel.getWeather(this);

    }

 

    private void onShowWeather(WeatherInfo weatherInfo){ 

        tv_name.setText(weatherInfo.city);

        tv_temperature.setText(weatherInfo.temp+"");

        tv_wind_d.setText(weatherInfo.WD);

        tv_wind_s.setText(weatherInfo.WS);

        tv_time.setText(weatherInfo.time);

    }

 

    \@Override 

    public void onLoadSuccess(WeatherInfo info) { 

        ld.dismiss();

        onShowWeather(info);

    }

 

    \@Override 

    public void onError(NetError error) { 

        ld.dismiss();

        T.getInstance().showShort(error.errorCode +" "+ error.errorMessage);

    }

}

这个例子逻辑很简单，Controller层的Activity持有Model层WeatherModel的对象，然后通过该对象去获取数据，获取到数据之后，通过View层去显示。但是上面有提到过xml布局文件作为View层，其实能做的事情特别少，实际上关于该布局文件中的数据绑定的操作，事件处理的代码都在Activity中，造成了Activity既像View又像Controller，所以有这么一句话：

Most of the modern Android applications just use View-Model
architecture，everything is connected with Activity.

所以这时候可以继续把Activity拆分，Activity只控制view和接受用户的输入，另外新建一个Controller类，这个类不能继承任何Android自带类，用来将逻辑拆分出来，避免Activity的难以维护。

**MVC优点：**

-   耦合性低。

-   重用性高。

-   生命周期成本低，MVC使开发和维护用户接口的技术含量降低。

-   可维护性高，分离试图层和业务逻辑层也使得WEB应用更易于维护和修改。

**MVC缺点：**

-   不适合小型，中等规模的应用程序。花费大量时间将MVC应用到规模并不是很大的应用程序通常会得不偿失。

-   增加系统结构和实现的复杂性。对于简单的界面，严格遵循MVC，使模型、视图与控制器分离，会增加结构的复杂性，并可能产生过多的更新操作，降低运行效率。

-   视图与控制器间过于紧密的连接。视图与控制器是相互分离，但却是联系紧密的部件，视图没有控制器的存在，其应用是很有限的，反之亦然，这样就妨碍了他们的独立重用。

-   视图对模型数据的低效率访问。依据模型操作接口的不同，视图可能需要多次调用才能获得足够的显示数据。对未变化数据的不必要的频繁访问，也将损害操作性能。

**六.软件构架模式之MVP**

Model, View and
Presenter模式，MVP模式和MVC模式类似，是由MVC演变而来，MVP将Controller变成Presenter，并且改变了通信方向，这个模式将应用分为三个主要层级：Model,
View and Presenter。 

![](media/bb122c9f93ca7de217a981cbe76ea18c.png)

 

可以看到Presenter与Model，Presenter与View的通信都是双向的，View不与Model发生通信，都是通过Presenter来传递，所以Presenter的业务量会显的非常大，三层之间的交互关系为：

View接受用户的交互请求

View将请求转交给Presenter

Presenter操作Model进行数据库更新

数据更新之后，Model通知Presenter数据发生变化

Presenter更新View层的显示

Model和View层之间是没有交互的，这是和MVC不同的一点：

Model层

该层通常是用来处理业务逻辑和实体模型。

View层

通常是一个Activity或者Fragment或者View，这取决于应用的结构，它会持有一个Presenter层的引用，所以View唯一做的事情就是在有用户交互等操作时调用Presenter层的方法。

Presenter层

该层用来作为一个中间层的角色，它接受Model层的数据，并且处理之后传递给View层，还需要处理View层的用户交互等操作。

View和Presenter的一对一关系意味着一个View只能映射到一个Presenter上，并且View只有Presenter的引用，没有Model的引用，所以Presenter和View是一个双向的交互。Presenter不管View层的UI布局，View的UI布局变更，Presenter层不需要做任何修改。

 

例子
----

MVP 的写法就有很多了，不同人对于 MVP
的写法各有不同，在遵循面向对象的设计原则基础上都是可以的，用哪种形式不要纠结，最重要的是自己用起来顺手。

### google 官方写法

源码地址：<https://github.com/googlesamples/android-architecture/tree/todo-mvp/todoapp>，分析一下： 

![](media/b54e36637497ad9b1e588a68dd3b2252.png)

 

在该 demo 中，有一个 BaseView 和 BasePresenter
，并且使用泛型来定义，作用是定义该模块不同 View 和 Presenter 的基础行为： 

**BaseView.class**

public interface BaseView\<T\> { 

    void setPresenter(T presenter); 

}

**BasePresenter.class**

public interface BasePresenter { 

    void start(); 

} 

之后的每一个页面的 View 和 Presenter 都要继承自 BaseView 和 BasePresenter ，在
demo 中，View 和 Presenter 的接口类都定义在一个 TasksContract 类中： 

**TasksContract.class**

public interface TasksContract { 

 

    interface View extends BaseView\<Presenter\> { 

 

        void setLoadingIndicator(boolean active); 

 

        void showTasks(List\<Task\> tasks); 

 

        void showAddTask(); 

 

        void showTaskDetailsUi(String taskId); 

 

        void showTaskMarkedComplete(); 

 

        void showTaskMarkedActive(); 

 

        void showCompletedTasksCleared(); 

 

        void showLoadingTasksError(); 

 

        void showNoTasks(); 

 

        void showActiveFilterLabel(); 

 

        void showCompletedFilterLabel(); 

 

        void showAllFilterLabel(); 

 

        void showNoActiveTasks(); 

 

        void showNoCompletedTasks(); 

 

        void showSuccessfullySavedMessage(); 

 

        boolean isActive(); 

 

        void showFilteringPopUpMenu(); 

    }

 

    interface Presenter extends BasePresenter { 

 

        void result(int requestCode, int resultCode); 

 

        void loadTasks(boolean forceUpdate); 

 

        void addNewTask(); 

 

        void openTaskDetails(\@NonNull Task requestedTask); 

 

        void completeTask(\@NonNull Task completedTask); 

 

        void activateTask(\@NonNull Task activeTask); 

 

        void clearCompletedTasks(); 

 

        void setFiltering(TasksFilterType requestType); 

 

        TasksFilterType getFiltering();

    }

}

之后就是实现这两个基础接口了，demo 中使用的是 Fragment 作为 View
的角色，把逻辑从 Activity 中抽离出来，这个看自己的编程习惯，只使用 Activity
还是使用 Fragment。先看看 Activity 的代码： 

**TasksActivity.class**

public class TasksActivity extends AppCompatActivity { 

 

    private static final String CURRENT_FILTERING_KEY
= "CURRENT_FILTERING_KEY"; 

 

    private DrawerLayout mDrawerLayout; 

 

    private TasksPresenter mTasksPresenter; 

 

    \@Override 

    protected void onCreate(Bundle savedInstanceState) { 

        super.onCreate(savedInstanceState); 

        setContentView(R.layout.tasks_act);

 

        // Set up the toolbar. 

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        ActionBar ab = getSupportActionBar();

        ab.setHomeAsUpIndicator(R.drawable.ic_menu);

        ab.setDisplayHomeAsUpEnabled(true);

 

        // Set up the navigation drawer. 

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mDrawerLayout.setStatusBarBackground(R.color.colorPrimaryDark);

        NavigationView navigationView = (NavigationView)
findViewById(R.id.nav_view);

        if (navigationView != null) { 

            setupDrawerContent(navigationView);

        }

 

        TasksFragment tasksFragment =

                (TasksFragment)
getSupportFragmentManager().findFragmentById(R.id.contentFrame);

        if (tasksFragment == null) { 

            // Create the fragment 

            tasksFragment = TasksFragment.newInstance();

            ActivityUtils.addFragmentToActivity(

                    getSupportFragmentManager(), tasksFragment,
R.id.contentFrame);

        }

 

        // Create the presenter 

        mTasksPresenter = new TasksPresenter( 

                Injection.provideTasksRepository(getApplicationContext()),
tasksFragment);

 

        // Load previously saved state, if available. 

        if (savedInstanceState != null) { 

            TasksFilterType currentFiltering =

                    (TasksFilterType)
savedInstanceState.getSerializable(CURRENT_FILTERING_KEY);

            mTasksPresenter.setFiltering(currentFiltering);

        }

    }

...

}

 

可以看到代码中，通过 Fragment 的静态方法获取到一个 Fragment ，并且添加到
activity 中，那么现在有一个疑问了， Presenter 是如何设置到 Fragment
中的呢？我们接下来看看 Presenter 类： 

**TasksPresenter.class**

public class TasksPresenter implements TasksContract.Presenter { 

    ...

    public TasksPresenter(\@NonNull TasksRepository tasksRepository, \@NonNull
TasksContract.View tasksView) { 

        mTasksRepository = checkNotNull(tasksRepository, "tasksRepository cannot
be null"); 

        mTasksView = checkNotNull(tasksView, "tasksView cannot be null!"); 

 

        mTasksView.setPresenter(this);

    }

 

    \@Override 

    public void start() { 

        loadTasks(false);

    }

    ...

}

 

可以看到是在 Presenter 的构造函数中 set 的，这也就没问题了，官方的 MVP
框架就介绍完了，大家去看一下源码就很清楚了。

### 其他可参考写法

例子，分析一下： 

![](media/b71f2f68cb28a83fb7e0339d6dbd4e50.jpg)

 

LoginActivity继承自LoginView；LoginPresenterImpl继承自LoginPresenter；LoginInteractorImpl继承自LoginInteractor，所以MVP模式三个层次之间是通过接口来进行交互的，看看源码： 

**LoginInteractorImpl.class类**

public class LoginInteractorImpl implements LoginInteractor { 

    \@Override 

    public void login(final String username, final String password, final
OnLoginFinishedListener listener) { 

        // Mock login. I'm creating a handler to delay the answer a couple of
seconds 

        new Handler().postDelayed(new Runnable() { 

            \@Override public void run() { 

                boolean error = false; 

                if (TextUtils.isEmpty(username)){ 

                    listener.onUsernameError();

                    error = true; 

                }

                if (TextUtils.isEmpty(password)){ 

                    listener.onPasswordError();

                    error = true; 

                }

                if (!error){ 

                    listener.onSuccess();

                }

            }

        }, 2000);

    }

}

**LoginActivity.class类**

public class LoginActivity extends Activity implements LoginView, View.OnClickListener
{ 

 

    private ProgressBar progressBar; 

    private EditText username; 

    private EditText password; 

    private LoginPresenter presenter; 

 

    \@Override 

    protected void onCreate(Bundle savedInstanceState) { 

        super.onCreate(savedInstanceState); 

        setContentView(R.layout.activity_login);

 

        progressBar = (ProgressBar) findViewById(R.id.progress);

        username = (EditText) findViewById(R.id.username);

        password = (EditText) findViewById(R.id.password);

        findViewById(R.id.button).setOnClickListener(this);

 

        presenter = new LoginPresenterImpl(this); 

    }

 

    \@Override protected void onDestroy() { 

        presenter.onDestroy();

        super.onDestroy(); 

    }

 

    \@Override public void showProgress() { 

        progressBar.setVisibility(View.VISIBLE);

    }

 

    \@Override public void hideProgress() { 

        progressBar.setVisibility(View.GONE);

    }

 

    \@Override public void setUsernameError() { 

        username.setError(getString(R.string.username_error));

    }

 

    \@Override public void setPasswordError() { 

        password.setError(getString(R.string.password_error));

    }

 

    \@Override public void navigateToHome() { 

        startActivity(new Intent(this, MainActivity.class));

        finish();

    }

 

    \@Override public void onClick(View v) { 

        presenter.validateCredentials(username.getText().toString(),
password.getText().toString());

    }

}

**LoginPresenterImpl.class类**

public class LoginPresenterImpl implements LoginPresenter, OnLoginFinishedListener
{ 

 

    private LoginView loginView; 

    private LoginInteractor loginInteractor; 

 

    public LoginPresenterImpl(LoginView loginView) { 

        this.loginView = loginView; 

        this.loginInteractor = new LoginInteractorImpl(); 

    }

 

    \@Override public void validateCredentials(String username, String password)
{ 

        if (loginView != null) { 

            loginView.showProgress();

        }

 

        loginInteractor.login(username, password, this); 

    }

 

    \@Override public void onDestroy() { 

        loginView = null; 

    }

 

    \@Override public void onUsernameError() { 

        if (loginView != null) { 

            loginView.setUsernameError();

            loginView.hideProgress();

        }

    }

 

    \@Override public void onPasswordError() { 

        if (loginView != null) { 

            loginView.setPasswordError();

            loginView.hideProgress();

        }

    }

 

    \@Override public void onSuccess() { 

        if (loginView != null) { 

            loginView.navigateToHome();

        }

    }

}

代码层次很清楚，View层接受用户的点击操作，回调Presenter层的相关接口，Presenter层再调用到Model层去执行登录操作，同时修改View层的Progress显示情况，Model层执行完登录操作之后，回调到Presenter层的对应接口，Presenter再去对View层的布局进行相应的修改。

**MVP优点：**

-   减低耦合，实现了 Model 与View 的真正分离，修改 View 而不影响 Model。

-   模块职责分明，层次分明，便于维护，多人开发首选。

-   Presenter 可以复用，一个 Presenter可以用于多个 View，不用去改 Presenter。

-   利于单元测试。模块分明，那么我们编写单元测试就变得很方便了，而不用特别是特别搭平台，人工模拟用户操作等等耗时耗力的事情。

**MVP缺点：**

-   增加代码量。小项目无须考虑MVP。

-   由于对视图的渲染放在了Presenter中，所以视图和Presenter的交互会过于频繁。还有一点需要明白，如果Presenter过多地渲染了视图，往往会使得它与特定的视图的联系过于紧密。一旦视图需要变更，那么Presenter也需要变更了。

**七.软件构架模式之MVVM**

  Model，View and ViewModel模式，MVVM 模式将 Presenter 改名为
ViewModel，基本上与 MVP
模式完全一致，ViewModel可以理解成是View的数据模型和Presenter的合体，MVVM采用双向绑定（data-binding）：View的变动，自动反映在
ViewModel，反之亦然，这种模式实际上是框架替应用开发者做了一些工作，开发者只需要较少的代码就能实现比较复杂的交互。 

![](media/ac25885cf5183cd2deab0047a0f3f6a3.png)

Model、View

类似MVP

ViewModel

注意这里的“Model”指的是View的Model，跟上面那个Model不是一回事。所谓View的Model就是包含View的一些数据属性和操作的东西。

例子
----

这种模式的关键技术就是数据绑定（Data Binding）。 

使用Data
Binding之后，xml文件中可以添加\<data\>\</data\>标签节点，要实现MVVM的ViewModel就需要把数据与UI进行绑定，data节点就为此提供了实现基础。

**具体使用流程：**

    在data标签中添加Variable变量

\<data\> 

\------user为一个POJO类

\<importtype="com.liangfeizc.databindingsamples.basic.User" /\>

\<variablename="user"type="User" /\>

\</data\>

  build.gradle 中添加的那个插件
- com.android.databinding会根据xml文件的名称 Generate 一个继承自 ViewDataBinding 的类。

修改 BasicActivity 的 onCreate 方法，用 DatabindingUtil.setContentView() 来替换掉 setContentView()，然后创建一个 user 对象，通过 binding.setUser(user) 与 variable 进行绑定。

    \@Override

    protected voidonCreate(Bundle savedInstanceState) 

    { 

        super.onCreate(savedInstanceState); 

        ActivityBasicBinding binding = DataBindingUtil.setContentView( this,
R.layout.activity_basic); 

        User user = new User("fei", "Liang"); 

        binding.setUser(user); 

    }

  数据与Variable绑定之后，xml的UI元素就可以使用了

\<TextView 

      android:layout_width="wrap_content” 

      android:layout_height="wrap_content” 

      android:text="\@{user.lastName}" /\>

 

**Xml元素绑定方法返回值**

    在data标签节点中添加

\<import type="com.liangfeizc.databindingsamples.utils.MyStringUtils" /\>

    UI元素使用

    \<TextView  

    android:layout_width="wrap_content” 

    android:layout_height="wrap_content” 

    android:text="\@{StringUtils.capitalize(user.firstName)}" /\> 

 

**Null Coalescing运算符**

  android:text="\@{user.displayName != null ? user.displayName : user.lastName}”

**MVVM优点：**

-   减低耦合。视图（View）可以独立于Model变化和修改，一个ViewModel可以绑定到不同的"View"上，当View变化的时候Model可以不变，当Model变化的时候View也可以不变。

-   **可重用性**。你可以把一些视图逻辑放在一个ViewModel里面，让很多view重用这段视图逻辑。

-   **独立开发**。开发人员可以专注于业务逻辑和数据的开发（ViewModel），设计人员可以专注于页面设计，使用Expression
    Blend可以很容易设计界面并生成xml代码。

-   **可测试**。界面素来是比较难于测试的，而现在测试可以针对ViewModel来写。

**MVVM缺点：**

-   数据绑定使得 Bug 很难被调试。你看到界面异常了，有可能是你 View 的代码有
    Bug，也可能是 Model 的代码有问题。数据绑定使得一个位置的 Bug
    被快速传递到别的位置，要定位原始出问题的地方就变得不那么容易了。 

-   一个大的模块中，model也会很大，虽然使用方便了也很容易保证了数据的一致性，当时长期持有，不释放内存，就造成了花费更多的内存。 

-   数据双向绑定不利于代码重用。客户端开发最常用的重用是View，但是数据双向绑定技术，让你在一个View都绑定了一个model，不同模块的model都不同。那就不能简单重用View了。 

**八.MVC,MVP,MVVM总结**

![](media/3d9b16430e37587bf592443698edac64.jpg)

 

MVP模式是从MVC模式演变来的，它们的基本思想有相通的地方：Controller/Presenter负责逻辑的处理，Model提供数据，View负责显示，所以他们之间并没有特别大的不同，都是用来将View和Model之间松耦合。作为一种新的模式，MVP与MVC有着一个重大的区别：在MVP中View并不直接使用Model，它们之间的通信是通过Presenter
(MVC中的Controller)来进行的，所有的交互都发生在Presenter内部，而在MVC中是允许Model和View进行交互的。还有重要的一点就是Presenter与View之间的交互是通过接口的。MVVM是通过MVP演变而来的，主要用了data
binding来实现双向交互，这就使得视图和控制层之间的耦合程度进一步降低，关注点分离更为彻底，同时减轻了Activity的压力。

**关键点：**

-   **MVC**  
      
       Controller是基于行为，并且能够在view之间共享  
      
       Controller负责接收用户交互等操作，并且决定需要显示的视图

-   **MVP**  
      
       View和Model更加的解耦了，Presenter负责绑定Model到View。  
      
       复杂的View可以对应多个Persenter。  
      
     
     Presenter保留有View层的事件逻辑，所有的点击之类的事件都直接委托给Presenter。  
      
       Presenter通过接口直接和View层解耦，所以更加方便的进行View的单元测试。  
      
       Presenter和其他两层都是双向调用的。  
      
       MVP有两种实现方式：”Passive View”，View基本包含0逻辑，
    Presenter作为View和Model的中间人，View和Model相互隔离，View和Model没有直接的数据绑定，取而代之的是View提供相关的setter方法供Persenter去调用，这么做的好处是View和Model干净的分离开了，所以更好的进行相关测试，缺点是需要提供很多的setter方法；”Supervising
    Controller”，Persenter处理用户交互等的操作，View和Model直接通过数据绑定连接，这种模式下，Persenter的任务就是将实体直接通过Model层传递给View层，这种方法的好处就是代码量少了，但是缺点就是测试难度增大，并且View的封装性变低。

-   **MVVM**

     用户直接交互的是View。

    View和ViewModel是多对一的关系。

View有ViewModel的引用，但是ViewModel没有任何关于View的信息。

支持View和ViewModel的双向数据绑定。
