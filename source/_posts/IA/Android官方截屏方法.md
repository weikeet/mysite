通过电脑显示手机屏幕的工具：Vysor

**一、应用内截屏**

**1.Activity截屏（带空白的状态栏）**

>   public Bitmap shotScreen(Activity activity) {

>   // 获取windows中最顶层的view 

>       View view = activity.getWindow().getDecorView();

>       // 打开DrawingCache的开关，允许当前窗口保存缓存信息

>       view.setDrawingCacheEnabled(true);

>       // 对view进行截图操作

>       view.buildDrawingCache();

>       // 返回DrawingCache的结果

>       Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache(), 0, 0,
>   view.getMeasuredWidth(), view.getMeasuredHeight());

>       // 关闭DrawingCache的开关

>       view.setDrawingCacheEnabled(false);

>   // 销毁缓存信息

>       view.destroyDrawingCache();

>       return bitmap;

>   }

**2.Activity截屏（去掉状态栏）**

>   public Bitmap shotActivityNoBar(Activity activity) { 

>       View view = activity.getWindow().getDecorView();

>   view.setDrawingCacheEnabled(true);

>       view.buildDrawingCache();

>       // 获取状态栏高度  

>       Rect rect = new Rect();

>       view.getWindowVisibleDisplayFrame(rect);

>       int statusBarHeights = rect.top;

>       Display display = activity.getWindowManager().getDefaultDisplay();

>       // 获取屏幕宽和高  

>       int widths = display.getWidth();

>       int heights = display.getHeight();

>        // 去掉状态栏的内容 

>       Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache(), 0,

>               statusBarHeights, widths, heights - statusBarHeights);

>   view.setDrawingCacheEnabled(false); 

>       view.destroyDrawingCache();

>       return bitmap;

>   }

**3.Fragment截屏**

>   public Bitmap getFragmentBitmap(Fragment fragment) {

>       // 获取Fragment的根布局

>       View view = fragment.getView(); 

>   view.setDrawingCacheEnabled(true);

>   view.buildDrawingCache();

>   Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache(), 0, 0,
>   view.getMeasuredWidth(), view.getMeasuredHeight());

>       view.setDrawingCacheEnabled(false);

>   view.destroyDrawingCache();

>       return bitmap;

>   }

**4.View控件截屏（截取某个View控件的图）**

>   TextView title = (TextView)findViewById(R.id.title_view);

>   title.setDrawingCacheEnabled(true);

>   title.buildDrawingCache();

>   Bitmap bitmap = Bitmap.createBitmap(title.getDrawingCache());

>   title.setDrawingCacheEnabled(false);

>   title.destroyDrawingCache();

或者手动draw：

>   TextView title = (TextView)findViewById(R.id.title_view);;

>   Bitmap bitmap = Bitmap.createBitmap(title.getWidth(), title.getHeight(),
>   Bitmap.Config.ARGB_8888);

>   // 手动将此视图（及其所有子项）渲染到给定的Canvas

>   Canvas canvas = new Canvas(bitmap);

>   title.draw(canvas);

使用下面的代码保存获取的bitmap

>   if (bitmap != null) {

>       try {

>           // 获取内置SD卡路径

>           String sdCardPath =
>   Environment.getExternalStorageDirectory().getPath();

>           // 图片文件路径

>           String filePath = sdCardPath + File.separator + "screenshot.png";

>           File file = new File(filePath);

>           FileOutputStream os = new FileOutputStream(file);

>       bitmap.compress(Bitmap.CompressFormat.PNG, 100, os);

>           os.flush();

>           os.close();

>       } catch (Exception e) {

>       e.printStackTrace();

>       }

>   }

**二、截取任意界面**

Android 在5.0
之后支持了实时录屏的功能。通过实时录屏我们可以拿到截屏的图像。同时可以通过在Service中处理实现后台的录屏。

用到的API：

**(1) MediaProjectionManager**

截屏授权令牌的管理者，通过 Context\#getSystemService 中MEDIA_PROJECTION_SERVICE
获取，其功能就是在onActivityResult()的反馈是RESULT_OK时，获取成功得到屏幕捕获授权的令牌——MediaProjection对象

**(2) MediaProjection**

一个授予应用程序捕获屏幕内容和或记录系统音频能力的令牌

**(3) ImageReader**

ImageReader允许应用程序直接访问呈现表面的图像数据，通过这个类我们可以把Surface转换成图片

步骤：

**1.创建MediaProjectionManager对象，存为成员变量**

>   mediaProjectionManager = (MediaProjectionManager)
>   getSystemService(Context.*MEDIA_PROJECTION_SERVICE*);

**2.使用MediaProjectionManager创建Intent对象**

>   Intent screenCaptureIntent =
>   mediaProjectionManager.createScreenCaptureIntent();

**3.启动录屏页面**

>   startActivityForResult(screenCaptureIntent, 101);

接下来会出现下图的系统弹窗，征得用户的同意。

![](media/f860c2c96f8ba72e5484431da5cb069c.png)

**4.在当前Activity重写onActivityResult()方法**

>   \@Override

>   protected void onActivityResult(int requestCode, int resultCode, Intent
>   data) {

>       super.onActivityResult(requestCode, resultCode, data);

>   }

**5.通过onActivityResult()回调的参数，获取MediaProjection对象**

>   MediaProjection mediaProjection =
>   mediaProjectionManager.getMediaProjection(resultCode, data);

**6.通过MediaProjection对象去获得VirtualDisplay对象，传入imageReader的getSurface()**

>   virtualDisplay = mediaProjection.createVirtualDisplay(“iHandy"

>           , widthPixels, heightPixels, screenDensity,

>           DisplayManager.*VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR*,

>           imageReader.getSurface(), null, null);

上一个方法用到的参数如何获取看下面的代码

>   widthPixels = getResources().getDisplayMetrics().widthPixels;

>   heightPixels = getResources().getDisplayMetrics().heightPixels;

>   DisplayMetrics metrics = new DisplayMetrics();

>   getWindowManager().getDefaultDisplay().getMetrics(metrics);

>   screenDensity = metrics.densityDpi;

>   imageReader = ImageReader.*newInstance*(widthPixels, heightPixels,
>   PixelFormat.*RGBA_8888*, 2);

注意上面的ImageReader的newInstance()方法的参数里面，第三个参数一定要用PixelFormat类中的属性，否则无法得到结果。

**7.从ImageReader中获取Image对象**

>   Image image = null;

>   try {

>       Thread.*sleep*(150);

>       image = imageReader.acquireLatestImage();

>       Log.*i*("WMLog", "image = " + image);

>   } catch (InterruptedException e) {

>       e.printStackTrace();

>   }

注意，一定要等待短暂的时间再去获取Image对象，否则得到的Image的对象是null，比较合适的时间是150毫秒。

**8.把Image对象转化成Bitmap对象**

>   int width = image.getWidth();

>   int height = image.getHeight();

>   Image.Plane[] planes = image.getPlanes();

>   ByteBuffer buffer = planes[0].getBuffer();

>   int pixelStride = planes[0].getPixelStride();

>   int rowStride = planes[0].getRowStride() - (pixelStride \* width);

>   Bitmap bitmap = Bitmap.*createBitmap*((rowStride / pixelStride) + width,
>   height, Bitmap.Config.*ARGB_8888*);

>   bitmap.copyPixelsFromBuffer(buffer);

>   image.close();

**9.把Bitmap对象存放成jpg格式**

>   String folderPath =
>   Environment.*getExternalStorageDirectory*().getAbsolutePath() +
>   File.*separator* + “ScreenShot”；

>   File folder = new File(folderPath);

>   if (!folder.exists()) {

>       folder.mkdir();

>   }

>   String fileName = System.*currentTimeMillis*() + ".jpg";

>   File file = new File(folder, fileName);

>   try {

>       FileOutputStream out = new FileOutputStream(file);

>       if (out == null) {

>           return;

>       }

>       [bitmap.compress(Bitmap.CompressFormat](about:blank).*JPEG*, 100, out);

>       out.flush();

>       out.close();

>       Uri uri = Uri.*fromFile*(file);

>       sendBroadcast(new Intent(Intent.*ACTION_MEDIA_SCANNER_SCAN_FILE*, uri));
>   // 发送广播让系统图库重新刷新

>   } catch (Exception e) {

>       e.printStackTrace();

>   }

最简洁的截屏程序：一个java文件和一个xml文件

\<此处有一个MainActivity_1.java文件，请到Android官方截屏方法.resources文件中查看\>

\<此处有一个activity_main_1.xml文件，请到Android官方截屏方法.resources文件中查看\>

**三、录屏**

**1.创建MediaProjectionManager对象**

>   mediaProjectionManager = (MediaProjectionManager)
>   getSystemService(*MEDIA_PROJECTION_SERVICE*);

**2.使用MediaProjectionManager创建Intent对象并启动征求用户同意的页面**

>   Intent captureIntent = mediaProjectionManager.createScreenCaptureIntent();

>   startActivityForResult(captureIntent, 102);

**3.重写onActivityResult()方法**

>   \@Override

>   protected void onActivityResult(int requestCode, int resultCode, Intent
>   data) {

>       super.onActivityResult(requestCode, resultCode, data);

>   }

**4.当回调的结果为RESULT_OK时，获取MediaProjection对象**

>   MediaProjection mediaProjection =
>   mediaProjectionManager.getMediaProjection(resultCode, data);

**5.初始化Recorder对象，注意有顺序要求，务必按照此顺序进行设置**

>   recorder = new MediaRecorder();

>   recorder.setAudioSource(MediaRecorder.AudioSource.*MIC*);

>   recorder.setVideoSource(MediaRecorder.VideoSource.*SURFACE*);

>   recorder.setOutputFormat(MediaRecorder.OutputFormat.*THREE_GPP*);

>   recorder.setAudioEncoder(MediaRecorder.AudioEncoder.*AAC*);

>   recorder.setVideoEncoder(MediaRecorder.VideoEncoder.*H264*);

>   recorder.setVideoSize(videoWidth, videoHeight);

>   recorder.setVideoEncodingBitRate(4 \* videoWidth \* videoHeight);

>   recorder.setVideoFrameRate(30);

>   recorder.setOutputFile(getFilePath());

>   try {

>       recorder.prepare();

>   } catch (IOException e) {

>       e.printStackTrace();

>   }

**6.通过MediaProjection对象去获得VirtualDisplay对象，传入recorder的getSurface()**

>   mediaProjection.createVirtualDisplay("iHandy", videoWidth, videoHeight,
>   screenDensity,

>           DisplayManager.*VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR*,
>   recorder.getSurface(), null, null);

>   recorder.start();

**7.接下来准备播放，初始化TextureView对象**

>   texture = (TextureView)findViewById(R.id.*texture*);

>   texture.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {

>       \@Override

>       public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int
>   width, int height) {

>           surface = new Surface(surfaceTexture);

>       }

>       \@Override

>       public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int
>   width, int height) {

>       }

>       \@Override

>       public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {

>           return false;

>       }

>       \@Override

>       public void onSurfaceTextureUpdated(SurfaceTexture surface) {

>       }

>   });

**8.使用MediaPlayer播放视频**

>   mediaPlayer = new MediaPlayer();

>       /\*设置显示时的宽高比与原视频一致\*/

>   mediaPlayer.setOnVideoSizeChangedListener(new
>   MediaPlayer.OnVideoSizeChangedListener() {

>       \@Override

>       public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {

>           if (mp != null) {

>               ViewGroup.LayoutParams videoParams = texture.getLayoutParams();

>               int textureWidth = texture.getWidth();

>               int textureHeight = texture.getHeight();

>               if (videoWidth \> videoHeight) {

>                   videoParams.width = textureWidth;

>                   videoParams.height = textureWidth \* videoHeight /
>   videoWidth;

>               } else {

>                   videoParams.width = textureHeight \* videoWidth /
>   videoHeight;

>                   videoParams.height = textureHeight;

>               }

>               texture.setLayoutParams(videoParams);

>           }

>       }

>   });

>   try {

>       mediaPlayer.setDataSource(fileName);

>       mediaPlayer.setSurface(surface);

>       mediaPlayer.prepare();

>       mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

>           \@Override

>           public void onPrepared(MediaPlayer mp) {

>               mediaPlayer.start();

>           }

>       });

>   } catch (IOException e) {

>       e.printStackTrace();

>   } finally {

>       play.setClickable(false);

>   }

最简洁的录屏程序：一个java文件和一个xml文件

\<此处有一个MainActivity.java文件，请到Android官方截屏方法.resources文件中查看\>

\<此处有一个activity_main.xml文件，请到Android官方截屏方法.resources文件中查看\>
