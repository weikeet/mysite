startForegroundService和startService最后都调用到了AMS中的startService方法，只不过参数fgRequired不一样，这个参数会保存到ServiceRecord的fgRequired成员变量里（ServiceRecord：AMS中用来记录Service信息的一个类）。

【AMS拒绝idle startService】

如果fgRequired为false，则不允许直接启动idle状态的App的Service，具体代码为：

>   // If this isn't a direct-to-foreground start, check our ability to kick off
>   an

>   // arbitrary service

>   if (!r.startRequested && !fgRequired) {

>       // Before going further -- if this app is not allowed to start services
>   in the

>       // background, then at this point we aren't going to let it period.

>       final int allowed = mAm.getAppStartModeLocked(r.appInfo.uid,
>   r.packageName,

>               r.appInfo.targetSdkVersion, callingPid, false, false);

>       if (allowed != ActivityManager.APP_START_MODE_NORMAL) {

>           Slog.w(TAG, "Background start not allowed: service "

>                   + service + " to " + r.name.flattenToShortString()

>                   + " from pid=" + callingPid + " uid=" + callingUid

>                   + " pkg=" + callingPackage);

>           if (allowed == ActivityManager.APP_START_MODE_DELAYED) {

>               // In this case we are silently disabling the app, to disrupt as

>               // little as possible existing apps.

>               return null;

>           }

>           // This app knows it is in the new model where this operation is not

>           // allowed, so tell it what has happened.

>           UidRecord uidRec = mAm.mActiveUids.get(r.appInfo.uid);

>           return new ComponentName("?", "app is in background uid " + uidRec);

>       }

>   }

其中r.startRequested表示这个Service是否正在被启动或者已经启起来了。

getAppStartModeLocked会先看Service所在uid是否为idle（后台1分钟后进入idle），如果不是idle，允许启动，否则会进入这句话：

>   int appRestrictedInBackgroundLocked(int uid, String packageName, int
>   packageTargetSdk) {

>       // Apps that target O+ are always subject to background check

>       if (packageTargetSdk \>= Build.VERSION_CODES.O) {

>           if (DEBUG_BACKGROUND_CHECK) {

>               Slog.i(TAG, "App " + uid + "/" + packageName + " targets O+,
>   restricted");

>           }

>           return ActivityManager.APP_START_MODE_DELAYED_RIGID;

>       }

>       ...

>   }

然后就起不来了。

【startForegroundService和startService区别】

代码上的区别就只有fgRequired，在上面的代码上可以体现出，如果调用的startForegroundService，AMS则不会拒绝启动Service。

fgRequired标记被赋值为true：只有调用startForegroundService的时候

fgRequired标记被赋值为false：

1.  startService：hmmm…逗我呢，岂不是我调了startForegroundService立即再调一次startService就行了？

2.  Service调用startForeground

3.  在scheduleServiceArgs之前，如果Service已经是foreground了

4.  Service被干掉

埋炸弹：scheduleServiceArgs（对应Service的onStartCommand）之前，如果fgRequired标记为true且之前没有埋过炸弹，会埋一个炸弹，炸弹有5秒的引爆时间

拆炸弹：

1.  Service调用startForeground

2.  Service被干掉

爆破保护：引爆炸弹时，fgRequired为false、或者Service已经死了，不stopService、不触发ANR
