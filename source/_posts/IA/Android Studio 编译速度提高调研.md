1.  现有项目：

    1.  环境：

        1.  Android Studio 2.2.2

        2.  Gradle :  2.14.1

    2.  gradle scripts

        1.  clean :

            1.  Executing tasks: [clean, :app:generateFastCompileDebugSources,
                :app:prepareFastCompileDebugUnitTestDependencies,
                :app:mockableAndroidJar,
                :app:generateFastCompileDebugAndroidTestSources]

        2.  build:

            1.  Executing tasks: [:app:generateFastCompileDebugSources,
                :app:prepareFastCompileDebugUnitTestDependencies,
                :app:mockableAndroidJar,
                :app:generateFastCompileDebugAndroidTestSources,
                :app:compileFastCompileDebugSources,
                :app:compileFastCompileDebugUnitTestSources,
                :app:compileFastCompileDebugAndroidTestSources]

        3.  rebuild:

            1.  Executing tasks: [clean, :app:generateFastCompileDebugSources,
                :app:prepareFastCompileDebugUnitTestDependencies,
                :app:mockableAndroidJar,
                :app:generateFastCompileDebugAndroidTestSources,
                :app:compileFastCompileDebugSources,
                :app:compileFastCompileDebugUnitTestSources,
                :app:compileFastCompileDebugAndroidTestSources]

        4.  run apk:

            1.  Executing tasks: [:app:clean,
                :app:generateFastCompileDebugSources,
                :app:prepareFastCompileDebugUnitTestDependencies,
                :app:mockableAndroidJar,
                :app:generateFastCompileDebugAndroidTestSources,
                :app:assembleFastCompileDebug]

        5.  Rerun,   修改代码-\>run ,    stop-\>run :

            1.  Executing tasks: [:app:assembleFastCompileDebug]

    3.  Instant Run 关：

        1.  清理时间: 1m4s

2.  Rebuild时间：1m3s

    1.  第一次Run Apk时间 :  1m58s

        1.  修改后Run:  2m6s

        2.  Rerun app: 1m1s - 2m2s

    2.  Instant Run 开

        1.  清理时间：1m4s

3.  rebuild时间 : 1m9s

    1.  第一次Run Apk时间 :  2m39s

        1.  修改后Run: 24s

        2.  Rerun app: 23s

        3.  stop-\>run: 26s

        4.  修改min sdk为21:  17s - 25s

    2.  各步骤耗时

        1.  环境： min sdk: 14, instant run 开

            1.  编译脚本耗时(clean )：

                1.  Run Init Scripts 40ms

                2.  Configure settings 10ms

                3.  Configure build 303ms

                4.  Calculate task graph 53s  - 瓶颈

                5.  Run Task 558ms

            2.  clean：1m13s, 54s,  1m7s

            3.  generateFastCompileDebugSources: 1m10s

                6.  Calculate task graph : 1m2s

                7.  run task :  7s

                    1.  prepare, preBuild, compileAidl,
                        generateBuildConfig,merge, generateAssets,
                        prepareDependencies, generateRes

                    2.  generate + merge, processed

            4.  prepareFastCompileDebugUnitTestDependencies: 1m8s

                8.  Calculate task graph : 1m7s

            5.  mockableAndroidJar:  1m (系统级别，可以忽略)

            6.  compileFastCompileDebugAndroidTestSources : 55s

        2.  build 耗时：

            1.  Run Init Scripts 38ms

            2.  Configure settings 9ms

            3.  Configure build 276ms

            4.  Calculate task graph 50s  - 瓶颈

            5.  Run Task 558ms

4.  设置org.gradle.jvmargs后

    1.  1024M (1g)

        1.  rebuild: 1m12s

        2.  run apk:  2m35

        3.  stop-\>run: 13s

        4.  修改后run: 22s

    2.  4068M (4.5g)

        1.  rebuild: 1m17s

        2.  run apk: 1m23s

        3.  stop-\>run: 16s

        4.  修改后run: 15s

    3.  6144M（6g）

        1.  rebuild: 1m10s,

        2.  run apk: 1m31s, 1m19s

        3.  stop -\> run: 13s

        4.  修改后run: 18s

5.  结论：

    1.  设置org.gradle.jvmargs,至少4g,
        可根据IDE提示来配置自己电脑所需的最低缓存， 在gradle.properties中配置

        1.  org.gradle.daemon=true

        2.  org.gradle.jvmargs=-Xmx6144m

6.  Instant Run -\> 开

    1.  IDE-Compiler:

        1.  Compile independent modules in parallel   10% - 20%

        2.  make project automatically

    2.  Gradle:

        1.  offline work:

            1.  依赖需要去网上下载最新版时，需要关闭offline

    3.  MIN SDK 21以上，会加快速度

7.  全部开启：

    1.  rebuild: 6s - 17s

    2.  run apk: 44s - 1m4s

    3.  stop -\> run : 1\~2s

    4.  修改后run: 5s

8.  其他

    1.  multiDexEnabled true ，不支持API LEVELE 20 以下

    2.  本地build不匹配设备的话，会重新rebuild在安装

    3.  gradle 3.0  & 3.1

    4.  dexOptions {        incremental true }, 实验阶段

9.  参考：

    1.  <https://docs.gradle.org/current/userguide/build_environment.html> 
        增加MaxHeapSize

    2.  <https://docs.gradle.org/2.14.1/userguide/gradle_daemon.html>  Gradle
        Daemon: 加快build速度

    3.  <https://docs.gradle.org/current/userguide/gradle_daemon.html>   Gradle
        daemon

    4.  <https://developer.android.com/studio/build/index.html>  理解build过程

    5.  <http://www.iteye.com/news/26754>   并行化编译和自动编译

To run dex in process, the Gradle daemon needs a larger heap.

It currently has 1994 MB.

For faster builds, increase the maximum heap size for the Gradle daemon to at
least 4608 MB (based on the dexOptions.javaMaxHeapSize = 4g).

To do this set org.gradle.jvmargs=-Xmx4608M in the project gradle.properties.

For more information see
<https://docs.gradle.org/current/userguide/build_environment.html>

6m26s

This build could be faster, please consider using the Gradle Daemon:
https://docs.gradle.org/2.14.1/userguide/gradle_daemon.html
