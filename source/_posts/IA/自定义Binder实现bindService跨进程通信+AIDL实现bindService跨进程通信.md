一、bindService

   
我们都知道，Service有start和bind两种方式，两者的关系涛哥和博哥当时已经研究得很透彻了，这里就不介绍了（有请涛哥上黑板画一画👏👏）

    我们直接来看bindService的接口：

>   public abstract boolean bindService(\@RequiresPermission Intent service,

>           \@NonNull ServiceConnection conn, \@BindServiceFlags int flags);

    第一个参数为service的intent；

第二个参数为ServiceConnection，当与Service连接成功或者断开时会有回调；

    第三个参数为flags，常用的是BIND_AUTO_CREATE；

一个“**错误示范**”（这个错误示范的灵感来自百度搜索bindService的第二条）：

我写了一个Demo，Demo里只有MainActivity和MyService，并实现了在MainActivity上bindService，然后调用Service的方法。

**· Activity代码：**

![](media/ade5b641b4075d98c54c5a79109c2563.png)

bindService的用法没毛病，onCreate的时候用Activity的context去bindService，在onDestroy的时候也用它去unbind。

ServiceConnection看起来好像也没什么问题，拿着Ibinder调它的方法。

**· Service代码：**

![](media/ac988feb87fecaf34ef1b994bc0a2408.png)

onBind的时候返回MyBinder，MyBinder提供了一些方法，看起来也没问题。

**· 运行一下：**

![](media/7af7e1484925765f98a82b040bc27f14.png)

结果也没问题，MainActivity确实调用到了MyService中的代码，但是为什么是错误示范呢？

**· ——在Manifest里把MyService的进程改成”:work”再运行一下：**

![](media/9cd6c34e3330db867bc6351a315e906c.png)

BinderProxy无法转换为MyBinder？？？

![](media/767843f6c430b9fc837237f262fb6fbf.jpg)

不是说Binder能跨进程吗，为什么回调回来的IBinder是android.os.BinderProxy而不是MyService\$MyBinder？

话说回来BinderProxy又是啥，代码在哪儿？

如何通过BinderProxy调用MyService\$MyBinder的方法？

以下内容有兴趣再看

二、自定义Binder

    上面的例子之所以无法跨进程，原因在于：

-   如果MainActivity和MyService在同一个进程，onServiceConnected中得到的IBinder对象就是MyService的onBind返回的MyBinder本身，所以没问题（可以观察到上面运行结果截图中，都是MyBinder\@cad85e3）

-   如果MainActivity和MyService不在一个进程，onServiceConnected中得到的IBinder对象是MyBinder的一个代理，它的类型为BinderProxy，强转为MyBinder类型是肯定有问题的。

-   所以当MainActivity和MyService不在一个进程时，我们只能对BinderProxy进行封装，通过BinderProxy采用Binder机制调用到MyBinder中以实现跨进程的调用。为了实现他，你需要深入了解Binder机制和代理模式，并且每实现一个接口都要写一大段代码，于是要对上面的代码作出下面一大段修改：

    -   首先新增IMyService接口：  
        

        ![](media/3417264f60bf3d278affee928679460c.png)

    -   然后新增继承自Binder的MyBinderNative抽象类，实现IMyService接口，抽象类实现onTransact方法，负责调用真正实现类的helloBinder方法和helloService方法。asInterface静态方法，如果iBinder是MyBinderNative，说明调用者就在Service进程，此时可以强制转换，如果不是（排除调错的情况，比如bind
        YourService时手抖调成MyBinderNative.asInterface），那一定是BinderProxy，此时使用组合的方式*new
        MyBinderProxy(iBinder)*进行代理：  
        

        ![](media/3f3bc34642977bdb9675313cff0a9d2a.png)

    -   接着新增MyBinderProxy类，也实现IMyService接口，代理类可以和MyBinderNative放在同一个文件内，也不需要public（当时分享代理设计模式的时候也提到过：用户并不需要知道自己用的是不是代理）。remote是Service的BinderProxy，非系统权限下需要指望AMS告诉我们其他非系统进程的BinderProxy，当然，一旦拿到这个BinderProxy，能干的事情就很多了，比如利用CP通过Parcel把自己进程的某个Binder传给其他进程，接收进程收到的是这个Binder的BinderProxy，接受进程通过这个BinderProxy可以回调到发送进程，这样能“实现”CP的回调。同时这种方法也可以用到AIDL中以实现Service。简单来说，MyBinderProxy通过调用remote的transact方法，能（借助Binder驱动）同步调用到Service所在进程的MyBinderNative的onTransact方法中去：  
        

        ![](media/851b299f0ee30fca1e164af498ba6cde.png)

    -   再接着MyService中定义MyBinder类实现MyBinderNative，在onBind中返回该实现类：  
        

        ![](media/1f1b1384bbc0deefa7bd27045eb8e4de.png)

    -   最后MainActivity中的ServiceConnection拿到IBinder之后通过MyBinderNative中定义的asInterface方法拿到MyBinder对象或者MyBinder的Proxy对象：  
        

        ![](media/d3d8fe5c1a0bc1a6834f1a9077f3e00a.png)

    -   这样才能跨进程使用，运行结果为：

        -   同进程，MainActivity通过asInterface拿到的binder就是Service中new出来的MyBinder对象本身：  
            

            ![](media/fbac2293be0c2a7cd285903faf1db957.png)

        -   跨进程，MainActivity通过asInterface拿到的binder是主进程自己创建的MyBinderProxy对象，其remote的是MyBinder的BinderProxy，通过trasact方法可以与其通信：  
            

            ![](media/4c70152fc3ad057599647b76dd879493.png)

    上面的结构图，这也是常见的跨进程代理模式的结构：

![](media/703b48a6cc40053f6188480a03f879fc.png)

三、AIDL

如果每次写Service都得像上面那样自己实现MyBinderNative和MyBinderProxy类，然后自己敲onTransact方法的话，是我早就疯了。

好在安卓为我们提供了AIDL，我们只需要稍微学习一下AIDL的语法，编译器就能帮我们自动生成类似MyBinderNative和MyBinderProxy的代码。

AIDL全称Android Interface Definition
Language，它是Android进程通信接口的描述语言，我们通常使用它来定义进程间的通信接口，上面红框划出来的部分都是自动生成的，Service只需要实现类似MyBinder的类，跨进程通信以及相同进程直接调用的优化都是透明的。上面的例子过于简单，我们换一个复杂的例子，这里举的是PhotoManager的AIDL：

![](media/f3045ffeb4274d81ee59198dca96ddae.png)

根据这个文件，编译器帮我们生成的是（感兴趣的同学可以去研究一下具体的语法，以及为何能够通过上面几个Listener实现跨进程回调，代码在libmax的PhotoManager模块下，几个Listener也需要在AIDL文件中定义，分别为IImageClarityCalcListener.aidl、IImagePHashCalcListener.aidl、IImageScanListener.aidl）：

![](media/bf76cc68f6298786abd96734e620d0f6.png)

![](media/99609ddb80b9d29fd07ec643481ff14f.png)

仔细看的话，其实这个文件和我们上面写的那一堆文件是几乎一样的（其实我是抄的AIDL。AIDL生成的文件里还包含DESCRIPTOR校验、还实现了android.os.Interface——代理（Proxy）采用的组合（可以思考：为什么是组合？），而服务（Stub）是继承，所以要封装asBinder将它们的Binder提取出来），它的类图为：

![](media/f1b2e047e4a383323d445404727497ce.png)

Service只需要在onBind中实现IPhotoManagerService.Stub接口即可支持跨进程通信：

![](media/aea4fffee1e48816dba83948c8db317f.png)

客户端通过Stub的asInterface静态方法拿到Stub（同进程）或者Stub.Proxy（跨进程）

>   iPhotoManagerService = IPhotoManagerService.Stub.*asInterface*(service);

误导向：了解AIDL的本质之后，通过startActivity也可以得到另一个进程的Activity的回调。

新建AIDL文件：

![](media/615ad0ccc92a99c593d81b699bafa6bd.png)

主进程MainActivity的onCreate里添加代码，start
ActivityB时new一个Binder传到Bundle里：

![](media/e3118f51150aac5547855f75e4dcaccd.png)

":sub"进程ActivityB的onCreate里添加代码，将Bundle里的BinderProxy取出来，通过Stub的asInterface拿到Stub.Proxy代理对象，通过代理调用callBack回调给ActivityA的Binder：

![](media/4bbdb481a83b78192e4c9ba331e0363e.png)

上面ActivityA start
ActivityB之后，ActivityA里每隔一秒会收到ActivityB发过来的token：

![](media/13d470213b8f517490e2e426c225d40b.png)
