ANR分为4种：

1.  Service Timeout：

    1.  startService启动前台Service，如果20s内没有执行完（onCreate和onStartCommand），会ANR

    2.  startService启动后台Service，据说如果200s内没有执行完，会ANR；

2.  常见的ANR——Broadcast Timeout：

    1.  前台（通过 Intent.FLAG_RECEIVER_FOREGROUND
        设置为前台，默认为后台）广播10s内没有响应完，会ANR

    2.  后台广播60s内没有响应完，会ANR

3.  CP Timeout：

    1.  publish provider超过10s（onCreate），会ANR；call操作超时不会ANR

4.  常见的ANR——InputDispatching Timeout：

    1.  响应用户输入事件超过5秒，包括触摸和按键（home键除外），会ANR

| 广播名                                          | 是否是前台广播 |
|-------------------------------------------------|----------------|
| android.bluetooth.adapter.action.STATE_CHANGED  | TRUE           |
| android.intent.action.ACTION_POWER_CONNECTED    | FALSE          |
| android.intent.action.ACTION_POWER_DISCONNECTED | FALSE          |
| android.intent.action.ACTION_SHUTDOWN           | TRUE           |
| android.intent.action.BATTERY_CHANGED           | FALSE          |
| android.intent.action.BOOT_COMPLETED            | FALSE          |
| android.intent.action.CLOSE_SYSTEM_DIALOGS      | TRUE           |
| android.intent.action.MEDIA_MOUNTED             | FALSE          |
| android.intent.action.NEW_OUTGOING_CALL         | TRUE           |
| android.intent.action.PACKAGE_ADDED             | FALSE          |
| android.intent.action.PACKAGE_REMOVED           | FALSE          |
| android.intent.action.PACKAGE_REPLACED          | FALSE          |
| android.intent.action.PACKAGE_RESTARTED         | FALSE          |
| android.intent.action.PACKAGE_RESTARTED         | FALSE          |
| android.intent.action.SCREEN_OFF                | TRUE           |
| android.intent.action.SCREEN_ON                 | TRUE           |
| android.intent.action.TIME_TICK                 | TRUE           |
| android.intent.action.USER_PRESENT              | FALSE          |
| android.intent.action.WALLPAPER_CHANGED         | FALSE          |
| android.net.conn.CONNECTIVITY_CHANGE            | FALSE          |
| android.net.conn.CONNECTIVITY_CHANGE            | FALSE          |
| android.net.wifi.RSSI_CHANGED                   | FALSE          |
| android.net.wifi.STATE_CHANGE                   | FALSE          |
| android.net.wifi.supplicant.CONNECTION_CHANGE   | FALSE          |
| android.net.wifi.WIFI_STATE_CHANGED             | FALSE          |
| android.provider.Telephony.SECRET_CODE          | TRUE           |
| hs.app.session.SESSION_START                    | TRUE           |
| hs.app.session.SESSION_END                      | TRUE           |
| android.provider.Telephony.SMS_RECEIVED         | TRUE           |
