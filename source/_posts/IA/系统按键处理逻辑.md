**1.分享背景**

**    1.1 为什么应用层无法响应系统按键**

**    1.2 系统按键何时被处理**

**2.分享内容**

**2.1 Home按键处理**

**2.2 截屏处理**

**2.3 power按键处理**

**1.分享背景**

       
在Android开发中有很多按键事件需要在App中捕获从而做出一些针对性的操作，例如返回键，数字键等都可以直接在dispatchKeyEvent，onKeyDown等回调方法中捕获并拦截，然而在应用层我们是无法拦截home键，power键，音量+/-键，recent键等。

为什么应用层无法拦截并处理这些按键事件？

这些按键事件是在什么时候被处理的呢？

       
带着这两个问题我们来研究一下系统按键的处理逻辑。首先借用下上次IMS分享的一张图，给大家回顾下Android输入事件的处理过程。

![](media/fc8854df7c908e6741a9162fdf6f933e.png)

**1.1 为什么应用层无法响应系统按键**

 Android输入系统提供了输入事件的拦截策略，能够调整输入事件的派发流程，提前处理输入事件，阻止将某些事件派发给目标窗口。

 在Android源码KeyEvent中对于Home键的定义有这样的注释：

>   */\*\* Key code constant: Home key.*

>   *\* This key is handled by the framework and is never delivered to
>   applications. \*/*

这些系统按键事件就是被拦截策略提前给拦截处理掉了，因而应用层无法接收到这些事件。拦截策略的具体实现是PhoneWindowManager，它在WMS创建的时候初始化。

/frameworks/base/services/java/com/android/server/SystemServer.java

>   wm = WindowManagerService.main(context, inputManager,

>           mFactoryTestMode != FactoryTest.FACTORY_TEST_LOW_LEVEL,

>           !mFirstBoot, mOnlyCore, new PhoneWindowManager());

**1.2 系统按键何时被处理**

按键事件的拦截策略的主要由PhoneWindowManager中的interceptKeyBeforeQueueing和interceptKeyBeforeDispatching方法控制，这些系统按键便在这两处被处理掉。这俩函数具体是怎么做的呢？

首先需要说明的是PhoneWindowManager中interceptKeyBeforeDispatching和interceptKeyBeforeQueueing的区别。

interceptKeyBeforeQueueing方法是在消息入队之前进行派发策略查询，这里一般处理一些实体按键的消息，处理的优先级最高，需要在第一时间响应，如power键或end
call键等。同时根据策略返回结果构造输入事件（KeyEntry）并注入派发队列（mInboundQueue）。

   
接下来，开始将派发队列（mInboundQueue）的事件分发给具体窗口，在这个过程中可能会执行额外的策略查询（interceptKeyBeforeDispatching），具体和interceptKeyBeforeQueueing返回结果有关。如果interceptKeyBeforeQueueing的返回结果是丢弃事件，则不会调用interceptKeyBeforeDispatching进行额外的策略查询，并将输入事件直接丢弃。否则在派发队列派发事件的时候利用interceptKeyBeforeDispatching进行额外的派发策略查询，并根据返回结果真正丢弃输入事件。
如果这里也没有拦截处理的话那么事件就被分发下去，将通过WindowManagerService按照窗口的层次一个个分发下去。

![](media/0513f023e30b04e5e587b9e2acdcf6c3.png)

   **    1.2.1  interceptKeyBeforeQueueing()**

interceptKeyBeforeQueueing()返回ACTION_PASS_TO_USER,表示该Key事件可以传递给InputDispatcher,返回\~ACTION_PASS_TO_USER表示被拦截了，不会参与后续处理。

![](media/9fdc2e5fc0848b57867f4f4cd7c508e5.png)

当返回ACTION_PASS_TO_USER时,会给InputDispatcher中的EventEntry.policyFlags赋值POLICY_FLAG_PASS_TO_USER，标识该输入事件可以继续往下处理。如果没有POLICY_FLAG_PASS_TO_USER这一标识，事件将不会。

>   DropReason dropReason = DROP_REASON_NOT_DROPPED;

>   mPendingEvent = mInboundQueue.dequeueAtHead();

>   if (!(mPendingEvent -\> policyFlags & POLICY_FLAG_PASS_TO_USER)) {

>       dropReason = DROP_REASON_POLICY;

>   } else if (!mDispatchEnabled) {

>       dropReason = DROP_REASON_DISABLED;

>   }

丢弃的原因有如下几种：

>   enum DropReason {

>      DROP_REASON_NOT_DROPPED = 0,

>      DROP_REASON_POLICY = 1,

>      DROP_REASON_APP_SWITCH = 2,

>      DROP_REASON_DISABLED = 3,

>      DROP_REASON_BLOCKED = 4,

>      DROP_REASON_STALE = 5,

>   };

>   1. DROP_REASON_NOT_DROPPED

>   不需要丢弃

>   2. DROP_REASON_POLICY

>   设置为DROP_REASON_POLICY主要有两种情形：

>   A. 在InputReader notify
>   InputDispatcher之前，Policy会判断不需要传递给应用的事件。

>   B. 在InputDispatcher
>   dispatch事件前，PhoneWindowManager使用方法interceptKeyBeforeDispatching()提前consume掉一些按键事件。

>   interceptKeyBeforeDispatching()主要对HOME/MENU/SEARCH按键的特殊处理，如果此时能被consume掉，那么在InputDispatcher中将被丢弃。

>    3.DROP_REASON_APP_SWITCH

>   当有App switch 按键如HOME/ENDCALL按键发生时，当InputReader向InputDispatcher
>   传递app switch按键时，会设置一个APP_SWITCH_TIMEOUT
>   0.5S的超时时间，当0.5s超时时，InputDispatcher 尚未dispatch到这个app
>   switch按键时，InputDispatcher 将会丢弃掉mInboundQueue中所有处在app
>   switch按键前的按键事件。这么做的目的是保证app
>   switch按键能够确保被处理。此时被丢弃掉的按键会被置为DROP_REASON_APP_SWITCH。

>   4. DROP_REASON_DISABLED

>   这个标志表示当前的InputDispatcher
>   被disable掉了，不能dispatch任何事件，比如当系统休眠时或者正在关机时会用到。

>   5. DROP_REASON_BLOCKED

>   输入事件阻碍了其他窗口获得事件而被丢弃。

>   6.DROP_REASON_STALE

>   设置的超时是10秒。如果超过了10秒，事件没有处理掉，那么就直接丢弃。

**1.2.2  interceptKeyBeforeDispatching()**

只有当interceptKeyBeforeQueueing()返回ACTION_PASS_TO_USER才会执行额外的派发策略查询。

>   if (entry -\> policyFlags & POLICY_FLAG_PASS_TO_USER) {

>       CommandEntry \* commandEntry = postCommandLocked(

>               &
>   InputDispatcher::**doInterceptKeyBeforeDispatchingLockedInterruptible**);

>       if (mFocusedWindowHandle != NULL) {

>           commandEntry -\> inputWindowHandle = mFocusedWindowHandle;

>       }

>       commandEntry -\> keyEntry = entry;

>       entry -\> refCount += 1;

>       return false; // wait for the command to run

>   } else {

>       entry -\> interceptKeyResult = KeyEntry::INTERCEPT_KEY_RESULT_CONTINUE;

>   }

interceptKeyBeforeDispatching()返回值含义：

当delay小于0时，表示处理了按键事件，或不希望此事件被派发给用户。

当delay等于0时，此事件可以正常地派发给用户。

当delay大于0时，表示尚未决定好如何处理这个事件。要求InputDispatcher先暂停对此事件的派发工作，稍后再进行一次策略查询。暂停的时间由delay决定。

![](media/00c567d0fde1c0a900dbb069f0190a51.png)

返回值的含义如下：

>   nsecs_t delay =
>   mPolicy-\>interceptKeyBeforeDispatching(commandEntry-\>inputWindowHandle,&event,
>   entry-\>policyFlags);

>   if (delay \< 0) {

>       entry-\>interceptKeyResult =
>   KeyEntry::INTERCEPT_KEY_RESULT_SKIP;//拦截事件

>   } else if (!delay) {

>       entry-\>interceptKeyResult =
>   KeyEntry::INTERCEPT_KEY_RESULT_CONTINUE;//放行事件

>   } else {

>       entry-\>interceptKeyResult =
>   KeyEntry::INTERCEPT_KEY_RESULT_TRY_AGAIN_LATER;//待会处理

>       entry-\>interceptKeyWakeupTime = now() + delay;

>   }

>   if (entry-\>interceptKeyResult == KeyEntry::INTERCEPT_KEY_RESULT_SKIP) {

>       if (\*dropReason == DROP_REASON_NOT_DROPPED) {

>           \*dropReason = DROP_REASON_POLICY;

>       }

>   }

**2.分享内容**

**2.1 Home按键处理**

**课堂提问：home广播是在哪一步发送？**

![](media/f59a3a916ac8daa2249e8f7fcc8c2540.png)

之前在做一个点击home键后弹出一个呈现rate
alert的Activity的feature，发现一个问题：在home广播的onReceive方法里通过startActivity启动的Activity会延时5秒才能被启动。这是为什么呢？

下面来分析下原因：

当我们按下Home键时，会调用AMS.stopAppSwitches()方法,该方法设置了一个当前时间+5秒的比较时间，后续启动的Activity都会和这个时间进行比较。

![](media/cb45d2bf72b9fe3050970b0e73290ad9.png)

![](media/d9f22926ba892bdea916750351e61868.png)

那么有什么方法可以避免吗？答案是有的，那就是通过PendingIntent去启动Activity。具体原因我卡在了AMS的分析中，若大家有兴趣可以研究并告知一下我。。。

上文中我们提到了home广播，那么这个广播是在哪儿发出的呢？答案是sendCloseSystemWindows().

![](media/67c6bde900ec5d6bc24717f64a7085b2.png)

ActivityManagerService.java

![](media/8af8ffbdeb7e059a3f56caa79f87aac6.png)

可以看出该广播是**前台无序广播**。

**2.2 截屏处理**

        Android原生支持通过按下音量➖+
power键进行截屏，这个组合键的操作是在interceptKeyBeforeQueueing()中完成的。原生Android系统两个键按下的间隔不超过150ms会触发截屏操作，和power键，音量➖键的按下顺序无关。截屏操作会消耗掉此次power键事件音量-键事件。下面分按键按下的先后顺序分别讨论：

先按power键后按音量➖键，此时会直接触发截屏，并且消耗掉这俩按键事件。

    先按音量➖键后按power键，此时情况复杂一点。
此时PhoneWindowManager以音量-键进入派发队列为组合键的前导，然后在150ms内等待电源键的到来。如果电源键在150ms内到来，则组合键生效，启动截屏，此时的音量-键则被PhoeWindowManager消耗掉，需要InputDispatcher将其抛弃。如果电源键在150ms内没能到来，则说明用户仅仅是按下音量-键而已，需要将其派发给用户。这就出现了一个问题，在音量➖键被注入派发队列后，到等待电源键到来的时间内（150ms）,如果InputDispatcher通过interceptKeyBeforeDispatching()向PhoneWindowManager询问音量➖键是否应该发送给用户时，PhoneWindowManager就迷糊了，在电源键按下或等待超时之前，它不知道是否应该将其派发给用户。此时，PhoneWindowManager在interceptKeyBeforeDispatching()返回一个大于0的延迟时间，告诉InputDispatcher:还是等等再说吧。

    二者的区别就是派发音量➖键是否需要等待。

![](media/a9db83143dd29e35e6f23cc0e215faae.png)

这里引入了**Messenger**概念，下面简单介绍下Messenger跨进程通信的原理。      

Messenger可以翻译为信使，Messenger是一种轻量级的IPC方案，通过它可以在不同进程中传递Message对象，在Message中放入我们需要传递的数据，就可以实现数据的进程间传递了。Messenger的底层实现是AIDL，为什么这么说呢？我们大致看一下Messenger这个类的构造方法就明白了。下面是Messenger的两个构造方法，从构造航发的实现上我们可以明显看出AIDL的痕迹，不管是IMessenger还是Stub.asInterface，这种使用方法都表明它的底层是AIDL。

>   public Messenger(Handler target) {

>       mTarget = target.getIMessenger();

>   }

>   public Messenger(IBinder target) {

>       mTarget = IMessenger.Stub.asInterface(target);

>   }

![](media/06b6227780fff958e40b1d61924f44f7.png)

Messenger的适用场景：

Messenger是以串行的方式处理客户端发来的消息，如果大量的消息同时发送到服务端，服务端仍然只能一个个处理，如果有大量的并发请求，那么用Messenger就不太合适了。同时，Messenger的作用主要是为了传递消息，很多时候我们可能需要跨进程调用服务端的方法，这种情形用Messenger
就无法做到了，但是我们可以使用AIDL来实现跨进程的方法调用。

**2.3 power按键处理**

power键的处理分为按下和抬起，首先分析按下power键的处理流程。

![](media/8831ba65fd43f19949d2befeebedc27a.png)

接下来是抬起power键的处理流程。

![](media/4e4eb939de37f30e1b0edc67b50e3cf4.png)

下面简单介绍下长按power键后，点击对话框中的关机流程为例，分析一次完整的关机流程。

![](media/8f222f9f7d2f850ffd2aab8823e73a0d.png)

![](media/ef26f0f2628800e3459f501329c42a3a.png)

课后问题：

经过interceptKeyBeforeQueueing拦截策略查询后，如果按键是需要被丢弃的，为什么还会将该事件注入派发队列（mInboundQueue）?（两张ticket）
