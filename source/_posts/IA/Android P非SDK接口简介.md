**Android P非SDK接口简介**

**什么是非SDK接口？**

非SDK接口也称为“私有API”,包括Android提供的hide注释的方法，private修饰的类，成员变量，成员方法。

**非SDK接口如何管控？**

非SDK接口通过名单机制进行管控。

白名单：SDK

浅灰名单：仍可以访问的非 SDK 函数/字段。

深灰名单：

对于目标 SDK 低于 API 级别 28 的应用，允许使用深灰名单接口。

对于目标 SDK 为 API 28 或更高级别的应用：行为与黑名单相同

黑名单：受限，无论目标 SDK 如何。 平台将表现为似乎接口并不存在。
例如，无论应用何时尝试使用接口，平台都会引发 NoSuchMethodError/NoSuchFieldException，即使应用想要了解某个特殊类别的字段/函数名单，平台也不会包含接口。

**在Android P平台使用黑名单中的非SDK接口会产生什么后果？**

![](media/34424048ac821b6314f49e12aee11d89.png)

**如何检测自己的app使用了哪些非SDK的接口？**

①使用logcat查看。

日志消息说明了访问方法：直接、通过反射或者通过 JNI。 最后，日志消息显示调用的非
SDK 接口属于灰名单还是黑名单。消息将显示在正在运行的应用的 PID 下。
例如，日志中的条目看上去可能类似下面这样：

>   Accessing hidden field Landroid/os/Message;-\>flags:I (light greylist, JNI)

②利用AOSP 中提供的静态分析工具*veridex*可以方便查询当前app使用了哪些非SDK接口。

-   下载veridex-mac.zip。*veridex*工具支持linux，windows和macOS三种操作系统，解压缩之后，你会在README.txt中看到具体的操作指南，这里以Mac环境为例做个示范

![](media/ba242f0f02354f4f1264e3b64c964603.png)

-   解压生成veridex-mac目录，并将待检测的apk文件拷贝至该目录

-   执行 ./appcompat.sh --dex-file=app-rocketCleanPro-armeabi-debug.apk

 利用veridex工具检测rocketCleanPro得到的部分结果

>   \#4: Linking light greylist
>   Landroid/content/pm/IPackageStatsObserver\$Stub;-\>\<init\>()V use(s):

>    
>    Lcom/ihs/device/clean/junk/appinfo/task/AppInfoScanProcessor\$1;-\>\<init\>(Lcom/ihs/device/clean/junk/appinfo/task/AppInfoScanProcessor;Lcom/ihs/device/common/HSAppInfo;Ljava/util/concurrent/CountDownLatch;)V

>    
>    Lcom/ihs/device/clean/junk/cache/app/sys/task/SysCacheScanProcessor\$1;-\>\<init\>(Lcom/ihs/device/clean/junk/cache/app/sys/task/SysCacheScanProcessor;Lcom/ihs/device/clean/junk/cache/app/sys/HSAppSysCache;Ljava/util/concurrent/CountDownLatch;)V

>   \#103: Reflection dark greylist Llibcore/icu/ICU;-\>addLikelySubtags use(s):

>        Landroid/support/v4/text/ICUCompatApi21;-\>\<clinit\>()V

>        Landroid/support/v4/text/ICUCompatIcs;-\>\<clinit\>()V

 
