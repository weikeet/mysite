为什么要做ADWrapper：

1.  一处广告位可能需要配多个ADPlacement，比如说是DonePage的插屏广告，放到外面来做会有较多的重复代码，添加ADPlacement需要改动的也会比较多

2.  方便PM进行数据统计，目前flurry的记录方式为每个广告placement记录各自的事件，不方便PM进行统一的处理

3.  广告优化，通过封装我们可以在后面对广告相关的内容进行统一的优化，例如已经有的根据CPM获取广告的模式等等

结构：

![](media/37effeea05db04a69c778ebf2ae2de51.jpg)

提供的接口：

**AcbInterstitialAdManagerWrapper**

**setFetchCpmFirst(boolean cpmFirst)；**

**preload(int count, \@AdConstants.AppPlacement String appPlacement)**

**fetchOne(\@AdConstants.AppPlacement String appPlacement)**

**createLoaderWithPlacement(\@AdConstants.AppPlacement String appPlacement)**

**isAdAvailableInPlacement(String appPlacement)**

**getAdsCountInPlacement(\@AdConstants.AppPlacement String appPlacement)**

**recordAdChanceEvent(\@AdConstants.AppPlacement String appPlacement)**

**AcbInterstitialAdLoaderWrapper**

**load(int totalCount, \@NonNull final AcbInterstitialAdLoaderWrapperListener
listener)**

**cancel()**

**AcbInterstitialAdWrapper**

**setInterstitialAdListener(final AcbInterstitialAdWrapperListener listener)**

**show()**

**show(Activity activity, AcbInterstitialAd.AnimType animType)**

**release()**

**AcbExpressAdActivator**

**activePlacementInProcess(\@AdConstants.AppPlacement String... appPlacements)**

**deactivePlacementInProcess(\@AdConstants.AppPlacement String...
appPlacements)**

**commit()**

**AcbExpressAdManagerWrapper**

**preload(\@AdConstants.AppPlacement String appPlacement, int count)**

**isAdAvailableInPlacement(String appPlacement)**

**getAdsCountInPlacement(\@AdConstants.AppPlacement String appPlacement)**

**recordAdChanceEvent(\@AdConstants.AppPlacement String appPlacement)**

**AcbExpressAdWrapperView**

**setExpressAdViewListener(final AcbExpressAdWrapperViewListener listener)**

AcbNativeAdActivator

public AcbNativeAdActivator activePlacementInProcess(\@AdConstants.AppPlacement
String... appPlacements)；

public AcbNativeAdActivator
deactivePlacementInProcess(\@AdConstants.AppPlacement String... appPlacements)；

public void commit()；

ç

public static void setCpmFirst(boolean cpmFirst);

public static boolean isCpmFirst();

public static AcbNativeAdLoaderWrapper
createLoaderWithPlacement(\@AdConstants.AppPlacement String appPlacement);

public static List\<AcbNativeAdWrapper\> fetchOne(\@AdConstants.AppPlacement
String appPlacement);

public static void preload(int count, \@AdConstants.AppPlacement String
appPlacement);

public static boolean isAdAvailableInPlacement(String appPlacement);

public static void recordAdChanceEvent(\@AdConstants.AppPlacement String
appPlacement);

public static void addNativeAdToCache(AcbNativeAdWrapper nativeAdWrapper);

AcbNativeAdLoaderWrapper

public void load(final AcbNativeAdLoadListenerWrapper
acbNativeAdLoadListenerWrapper);

public void cancel();

AcbNativeAdWrapper

public AcbNativeAdWrapper(\@AdConstants.AppPlacement String appPlacement,
\@CommonConstants.AdPlacement String adPlacement, AcbNativeAd acbNativeAd);

public void release();

public void setNativeClickListener(final
AcbNativeAdWrapper.AcbNativeClickListenerWrapper nativeClickListenerWrapper);

public boolean isAdReleased();

public boolean isExpired();

public AcbNativeAd getAcbNativeAd();

public String getAdPlacement();

public String getAppPlacement();

AcbNativeAdWrapperContainerView

public AcbNativeAdWrapperContainerView(Context context);

public void fillNativeAd(AcbNativeAdWrapper acbNativeAdWrapper);

以及其他AcbNativeAdWrapperContainer原有方法（继承实现）

AcbErrorWrapper

public String getMessage();

public int getErrorCode();

**XXXRelationship
类：该类为提供APPPlacement到AdPlacement的关系的类，需要各个APP自己覆盖**

**具体实现： Interstitial 在AdActiveHelper中创建内部类InterstitialAdRelation
，并在该内部类中实现两个方法**

**🌰**

**public static List\<String\> getWorkProcessPlacementList() {**

**    return new ArrayList\<String\>() {{**

**    add(“AdPlacement");**

**}};**

**}**

**该方法⬆️返回的为在work进程的所有AdPlacement**

**🌰**

**public static List**\<**String**\> getAdPlacementList(**String**
*appPlacement*) {

**List**\<**String**\> adPlacementsList = **new** ArrayList\<\>();

**switch** (*appPlacement*) {

    **case AppPlacement**:

adPlacementsList.add(“AdPlacement”);

    **break**;

}

    **return** adPlacementsList;

}

**该方法⬆️返回的为传入AppPlacement对应的所有AdPlacement**

Express 广告添加
 [com.optimizer.test.ad.](http://com.optimizer.test.ad/)**ExpressRelationship.java **

**🌰：**

**public class ExpressRelationship** {

**public static List**\<**String**\> getAdPlacementList(**String**
*appPlacement*) {

**List**\<**String**\> adPlacementsList = **new** ArrayList\<\>();

**switch** (*appPlacement*) {

     **case AppPlacement**:

    adPlacementsList.add(“AdPlacement”);

     **break**;

}

     **return** adPlacementsList;

}

}

-   AppPlacement
    ：为需要展示广告的时机，举例说明，比如我们要在DonePage动画完成后展示广告，Done页面动画完成了，这时需要展示广告的时机出现了，这个时机就是一个(DonePage)AppPlacement

-   AdPlacement : 为广告PM配置的广告位，原来使用的就是这个placement

-   两者关系为一个AppPlacement可以对应多个AdPlacement，按照一定的优先级顺序进行广告库操作，以fetch为例，对某个AppPlacement进行fetch操作会对其对应的所有AdPlacement依次进行fetch操作，直到拿到指定数目(该数目是指要拿的广告的数目，绝大多数情况下为一个)的广告或者所有fetch都已经完成

-   inner，
    负责实际调用广告库，并上报包含AdPlacement和AppPlacement相关的flurry，每一次AdPlacement操作上报一条,
    即这一层的调用实际上是AdPlacement相关的，这一层理论上来说不应该被直接调用

-   wrapper层，负责将AppPlacement 转化为AdPlacement执行相应的load,
    fetch等方法，并上报仅包含AppPlacement的flurry,
    每一次AppPlacement操作上报一次，这一层是被直接调用的，需要注意的是目前这一层改为了loadOne(),
    fecthOne()。  

-   另外Manager中addNativeAdToCache方法支持将取到的ad再放回到缓存，在相同情况下（cpm相同,
    或非cpm）load或fetch总是先去区缓存里面的。 

-   用AcbNativeAdWrapperContainerView代替原来的AcbNativeAdWrapperContainer 

-   AcbNativeAdActivator用来actve或deactive相应的AppPlacement的，只有在commit之后才会生效

-   AcbErrorWrapper appplacement load失败返回的bean类。

-   AcbExpressAdManagerWrapper.recordAdChanceEvent()： 记录时机专用

接口改动：

Interstitial

-   原来使用AcbInterstitialAdManager—\>AcbInterstitialAdManagerManager   

-   原来使用 AcbInterstitialAd
    /AcbInterstitialAdProxy —\> AcbInterstitialAdWrapper (这个里面封装了跨进程的方法，直接用就可以了)

-   原来使用AcbInterstitialAdLoader —\>AcbInterstitialAdLoaderWrapper

-   而在需要展示广告的时机调用 xxx**AdManagerWrapper.**recordAdChanceEvent（）

Express：

-   激活使用Activator

-   AcbExpressAdView —\> AcbExpressAdWrapperView

-   AcbExpressAdViewListener—\>AcbExpressAdWrapperViewListener

-   AcbExpressAdManager —-\> AcbExpressAdManagerWrapper

Native:

1.1 ，配置AppPlacement-\>AdPlacement的映射关系

1.2，在AdActivehelper中调用AcbNativeAdActivator 的active deactive方法并commit

1.3,  使用AcbNativeAdManagerWrapper的fetchOne 或
AcbNativeAdLoaderWrapper的loadOne方法获取AcbNativeAdWrapper，
并调用AcbNativeAdManagerWrapper.recordAdChanceEvent()（根据实际需求，在适当时机）

1.4  使用AcbNativeAdWrapperContainerView显示AcbNativeAdWrapper

Reward:

1.1, 配置AppPlacement-\>AdPlacement的映射关系

1.2, 在AdActivehelper中调用AcbRewardAdActivator 的active deactive方法并commit

1.3, 使用AcbRewardAdManagerWrapper的fetchOne 或
AcbRewardAdLoaderWrapper的loadOne方法获取AcbRewardAdWrapper，
并调用AcbRewardAdManagerWrapper.recordAdChanceEvent()（根据实际需求，在适当时机）

1.4, 使用AcbRewardAdWrapper进行广告的监听显示等

Log 及 Flurry

Log：

TAG在AdConstants中的常量，根据广告类型的不同使用不同的TAG查看Log

Flurry

记录的事件如下

| eventid                                                             | key               | value                                                                                         | 注释                                                                    |
|---------------------------------------------------------------------|-------------------|-----------------------------------------------------------------------------------------------|-------------------------------------------------------------------------|
| **IA_APP**\_XXX_(Interstitial/Native/Express)Ad (XXX为AppPlacement) | AppFetch          | “fetch_failed” “fetch_success”                                                                | 一次AppPlacement时机去拿广告的结果                                      |
|                                                                     | AppLoad           | “start_load” “load_success" "load_failed" “load_canceled"                                     | 一次APPPlacement时机去load广告操作                                      |
|                                                                     | AppPreload        | "start_preload"                                                                               | AppPlacement需要预加载广告时记录该事件                                  |
|                                                                     | Chance            | 换成时间间隔                                                                                  | AppPlacement出现时，记录该事件                                          |
|                                                                     | AdFetch           | “fetch_failed" + adPlacement “fetch_success" + adPlacement                                    | 调用一次库接口或者代理接口                                              |
|                                                                     | AdLoad            | “load_succeed_” + adPlacement “load_failed_” + adPlacement “load_canceled_"  + adPlacement    | 调用一次库接口或者代理接口                                              |
|                                                                     | AdPreload         | “preload_" + adPlacement                                                                      | 调用一次库接口或者代理接口                                              |
|                                                                     | AdViewed          | adPlacement                                                                                   | 广告展示                                                                |
|                                                                     | AdClicked         | adPlacement                                                                                   | 广告点击                                                                |
| **IA_AD**\_XXX_(Interstitial/Native/Express)Ad XXX为AdPlacement     | AdFetch           | “fetch_failed_” + appPlacement “fetch_success_" + appPlacement                                | 调用一次库接口或者代理接口                                              |
|                                                                     | AdLoad            | “load_succeed_” + appPlacement “load_failed_” + appPlacement “load_canceled_" + appPlacement  | 调用一次库接口或者代理接口                                              |
|                                                                     | AdPreload         | “preload_" + appPlacement                                                                     | 调用一次库接口或者代理接口                                              |
|                                                                     | AdViewed          | appPlacement                                                                                  | 广告展示                                                                |
|                                                                     | AdClicked         | appPlacement                                                                                  | 广告点击                                                                |
|                                                                     | LoadFetchInterval | "ad_placement_" + adPlacement + “_interval_”+间隔                                             | 调用广告库load或者fetch接口为一次获取事件，该flurry为两次事件的时间间隔 |

而adXXX代表的则是原来的adplacement。
