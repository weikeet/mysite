Java
静态代码分析工具能够在代码构建过程中帮助开发人员快速、有效的定位代码缺陷并及时纠正这些问题。Sonar
(SonarQube)是一个开源平台，用于管理源代码的质量。该工具目前在业界的使用范围很广，从csdn在2016年所做的调查问卷来看，Sonar的使用占比41%。

使用方法：

-   下载SonarQube并解压*https://www.sonarqube.org/downloads/*

-   开启服务 /etc/sonarqube/bin/[OS]/sonar.sh console

-   浏览器登录 [http://localhost:9000](http://localhost:9000/) 

-   Gradle配置

buildscript {

  repositories {

    maven {

      url "<https://plugins.gradle.org/m2/>"

    }

  }

  dependencies {

    classpath "org.sonarsource.scanner.gradle:sonarqube-gradle-plugin:2.6.2"

  }

}

apply plugin:“org.sonarqube"

subprojects {

    sonarqube {

        properties {

            property "sonar.host.url", "<http://localhost:9000/>"
*//本地SonarQube平台的配置*

*       * }

    }

}

更多gradle配置*https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner+for+Gradle* 

-   terminal 运行 

**./gradlew sonarqube \\**

**  -Dsonar.host.url=**<http://localhost:9000> **\\**

**  -Dsonar.login=[your token]**

看起来是这个样子的

![](media/c6016b0cec1a8da22144ca242f8b2f8c.png)

这个样子的

![](media/ada82ec24b1fec1de0790125f15b6692.png)

它还提供一个lint工具 sonarlint

Android Studio -\> Preferences -\> Plugin 中 找到SonarLint的插件，并安装

安装完成后，在Android Studio -\> Preferences 中 设置远程的Sonar Server的地址。

![](media/289fdcf6c215895067995d180029b666.png)

![](media/68d50eb47a7937bd688e5b4514f2cffb.png)

![](media/a0c464e348742ae2d339f051fe89bcfe.png)

可以在服务器中定制代码检查规则，将常用的规则优先级提高，一些不必要的规则优先级降低。

也可以自定义代码检查规则

1 实现plugin接口

2 自定规则

3 打包plugin，拷贝到plugins文件夹

4 命令行执行

参考：

<https://docs.sonarqube.org/display/PLUG/Writing+Custom+Java+Rules+101>

<https://segmentfault.com/a/1190000008659108>

<https://docs.sonarqube.org/display/DEV/Adding+Coding+Rules>

<https://www.jianshu.com/p/a93e46f83a1e>
