调查结果：

竞品名称:  ClearMaster  (猎豹清理大师国际版 com.cleanmaster.mguard) 

版本号: 6.12.6版本 (GooglePlay 最新版)

结论: 竞品目前并未更新targetSDK至26，竞品最近版本targetSDKversion是23

    竞品采用 TYPE_PHONE  + SYSTEM_ALERT_WINDOW,从而在安装时获取悬浮窗权限
(与我们相同)

    当用户手动关闭竞品悬浮窗权限后,竞品会从 TYPE_PHONE 模式切换到 TYPE_TOAST
模式

    由于targetSDKversion并不是26，因此对我们参考意义不大

package: name='com.cleanmaster.mguard' versionCode='61266322'
versionName='6.12.6' platformBuildVersionName='6.0-2166767'

install-location:'auto'

sdkVersion:'23'

targetSdkVersion:’23'

1 应用安装时，自动获取悬浮窗权限

  Window \#5 Window{ef677c5 u0 com.cleanmaster.mguard}:

    mDisplayId=0 stackId=0 mSession=Session{d560699 31717:u0a10095}
mClient=android.os.BinderProxy\@703be3c

    mOwnerUid=10095 mShowToOwnerOnly=true package=com.cleanmaster.mguard
appop=SYSTEM_ALERT_WINDOW

    mAttrs=WM.LayoutParams{(0,0)(1440x2392) sim=\#20 ty=2002 fl=\#1000508 fmt=1
wanim=0x7f0d00ca or=1 colorMode=0}

2 当用户手动取消悬浮窗权限后，动态改为TOAST

  Window \#3 Window{57f420e u0 com.cleanmaster.mguard}:

    mDisplayId=0 stackId=0 mSession=Session{702336e 14238:u0a10892}
mClient=android.os.BinderProxy\@96d9009

    mOwnerUid=10892 mShowToOwnerOnly=true package=com.cleanmaster.mguard
appop=TOAST_WINDOW

    mAttrs=WM.LayoutParams{(0,0)(1440x2392) sim=\#20 ty=2005 fl=\#1000508 fmt=1
wanim=0x1030004 or=1 colorMode=0}
