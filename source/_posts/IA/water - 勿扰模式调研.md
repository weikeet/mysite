**\<此处有一个勿扰模式调研.pdf文件，请到water -
勿扰模式调研.resources文件中查看\>**

**1.接/打电话**

 申请权限方式，可一直监听状态变化：

   
 在应用层监听通话状态只有三种，从TelephonyManager.java中注释可知这三种状态含义如下： 

-   **CALL_STATE_IDLE**   
      
    空闲态(没有通话活动) 

-   **CALL_STATE_RINGING**   
      
    包括响铃、第三方来电等待 

-   **CALL_STATE_OFFHOOK**   
      
    包括dialing拨号中、active接通、hold挂起等  
      
    在AndroidManifest注册：  
      
    Requires
    permission:[Manifest.permission.READ_PHONE_STATE](https://developer.android.com/reference/android/Manifest.permission.html#READ_PHONE_STATE)  
      
    不申请权限方式：  
      
    **AudioManager可调用函数：**

1.  [getMode](https://developer.android.com/reference/android/media/AudioManager.html#getMode())()——返回当前音频模式，如

-   **MODE_NORMAL**  
      
    Normal audio mode: not ringing and no call established.  
      
    --普通音频模式：没有振铃，也没有建立呼叫。

-   **MODE_RINGTONE**  
      
    Ringing audio mode. An incoming is being signaled.  
      
    振铃音频模式。传入正在发出信号。

-   **MODE_IN_CALL**  
      
    In call audio mode. A telephony call is established.  
      
    在通话音频模式下。正处于通话中。

-   **MODE_IN_COMMUNICATION**  
      
    In communication audio mode. An audio/video chat or VoIP call is
    established.  
      
    在通信音频模式下。建立音频/视频聊天或VoIP呼叫。  
      
       
      
    **2.麦克风开启和外放开启**  
      
    外放开启可通过代码实现，无需获取权限，麦克风是否开启可通过是否正在打电话或者是否正在视频聊天来判断，也可以通过发起音频录音的手段，如果不能录音，则可以判断MIC被占用，但这个方式需要获取权限。相关方法及详解如下：  
      
    **AudioManager可调用函数：**

1.  [getMode](https://developer.android.com/reference/android/media/AudioManager.html#getMode())()——返回当前音频模式，如

-   **MODE_NORMAL**  
      
    Normal audio mode: not ringing and no call established.  
      
    --普通音频模式：没有振铃，也没有建立呼叫。

-   **MODE_RINGTONE**  
      
    Ringing audio mode. An incoming is being signaled.  
      
    振铃音频模式。传入正在发出信号。

-   **MODE_IN_CALL**  
      
    In call audio mode. A telephony call is established.  
      
    在通话音频模式下。正处于通话中。

-   **MODE_IN_COMMUNICATION**  
      
    In communication audio mode. An audio/video chat or VoIP call is
    established.  
      
    在通信音频模式下。建立音频/视频聊天或VoIP呼叫。

1.  [isMusicActive](https://developer.android.com/reference/android/media/AudioManager.html#isMusicActive())()  
      
    Checks whether any music is active.  
      
    检查是否有任何音乐活动。

2.  [isSpeakerphoneOn](https://developer.android.com/reference/android/media/AudioManager.html#isSpeakerphoneOn())()  
      
    Checks whether the speakerphone is on or off.  
      
    检查是否打开或关闭扬声器。

3.  录制音频方式检测MIC是否被占用  
      
    需要权限  
      
    \<uses-permission android:name="android.permission.RECORD_AUDIO" /\>  
      
       
      
    <https://stackoverflow.com/questions/35633513/how-to-check-whether-microphone-is-used-by-any-background-app>  
      
    **3.静音模式与手机震动**  
      
    可通过代码实现，无需获取权限，方法及详解如下：  
      
    **AudioManager可调用函数：**

4.   *getRingerMode*()，会返回 [RINGER_MODE_NORMAL](https://developer.android.com/reference/android/media/AudioManager.html#RINGER_MODE_NORMAL)（普通模式）, [RINGER_MODE_SILENT](https://developer.android.com/reference/android/media/AudioManager.html#RINGER_MODE_SILENT)（静音模式）,
    or [RINGER_MODE_VIBRATE](https://developer.android.com/reference/android/media/AudioManager.html#RINGER_MODE_VIBRATE)（振动模式）.

-   **RINGER_MODE_NORMAL**  
      
    Ringer mode that may be audible and may vibrate. It will be audible if the
    volume before changing out of this mode was audible. It will vibrate if the
    vibrate setting is on.  
      
    铃声模式可能听得见，可能会振动。
    如果可以听到更改此模式之前的音量，则会听到声音。
    如果振动设置打开，它将振动。

-   **RINGER_MODE_SILENT**  
      
    Ringer mode that will be silent and will not vibrate. (This overrides the
    vibrate setting.)  
      
    铃声模式将保持沉默，不会振动。 （这会覆盖振动设置。）  
      
    **4.键盘上浮**  
      
    可通过代码实现，无需获取权限，代码如下：  
      
    InputMethodManager imm =
    (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);  
      
      
      
    boolean isOpen=imm.isActive();  
      
      
      
    isOpen若返回true，则表示输入法打开  
      
    **5.蓝牙连接中**  
      
    蓝牙设备有很多，包括智能手表，蓝牙耳机，蓝牙手柄等，我们就算获取到也需要去判断是属于哪种设备。比如连接的是智能手表，可能对我们发全屏、PUSH并不会有什么影响，而连接蓝牙耳机也不一定用户正在通话。  
      
    检测蓝牙状态需要权限  
      
    \<uses-permission android:name="android.permission.BLUETOOTH"/\>  
      
    **6.飞行模式**  
      
    可通过代码实现，无需获取权限，代码如下：  
      
    boolean isAirplaneMode
    = Settings.System.getInt(mContext.getContentResolver(),
    Settings.System.AIRPLANE_MODE_ON, 0) ;   
      
    **7.前置摄像头与后置摄像头**  
      
    安卓没有提供方法单独检测当前是否正在使用摄像头，只能与一些场景结合。或许有其他方法，但找了半天也没找到。。。
