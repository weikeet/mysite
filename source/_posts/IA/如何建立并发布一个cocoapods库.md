# 建立并发布一个自己的Cocoapods库



### 首先，cocoapods库是什么？我们需要先了解一下podfile和podspec文件！

我们在写ios程序的时候，在打开工程之前，往往会使用

``` bash
pod install
```

来安装我们的项目依赖的pod库，这个命令会去查找当前目录下的podfile文件，并根据其中的内容自动下载并关联所需要的pod库

一个最简单的podfile文件结构如下

``` bash
# source用来表示pod会去哪里下载所需要的pod库
source 'https://git.enerjoy.fun/han.fu/fhcocoaspecs'

# target XXX do pod YYY 表示对于ios的targetXXX cocoapods会去下载YYY库并将它与targetXXX关联
target 'FHTestLib_Example' do
  pod 'FHTestLib'

# 以end表示target结束
end
```

pod在下载库的时候首先会去下载库的podspec文件，这个podspec文件中包含了这个库的具体信息，例如依赖的其他库，版本，代码具体存放位置等等

一个简单的podspec文件如下

```bash
Pod::Spec.new do |s|
  s.name             = 'FHTestLib'
  # s.version 当前库的版本
  s.version          = '0.1.0'
  s.summary          = 'A short description of FHTestLib.'
  # s.swift_versions 定义了库使用的swift版本
  s.swift_versions   = '5.0'


  s.description      = <<-DESC
  This man is so lazy that he leaves nothing.
                       DESC

  s.homepage         = 'https://git.enerjoy.fun/han.fu/fhtestcocoalib'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'han.fu' => 'han.fu@enerjoy.life' }
  # s.source 定义了库代码的具体的git地址，pod在下载库代码的时候会自动去这个地址下载特定tag的commit。
  # 下载的tag名跟我们本库的version是一样的
  s.source           = { :git => 'https://git.enerjoy.fun/han.fu/fhtestcocoalib.git', :tag => s.version.to_s }
	# s.ios.deployment_target 定义了部署的ios版本
  s.ios.deployment_target = '10.0'

	# s.source_files定义了库代码包含下载git地址下的那个路径下的文件
  s.source_files = 'FHTestLib/Classes/**/*'

	# s.resource_bundles定义了库代码包含下载git地址下的那些资源文件
  s.resource_bundles = {
    'FHTestLib' => ['FHTestLib/Assets/*.png']
  }

	# s.dependency定义了本库依赖哪些其他的库
  s.dependency 'LEFramework', '~> 1.1.0.beta18'
  s.dependency 'SQLite.swift', '~> 0.12.0'
end

```

下载到podspec文件后，pod会根据其中的内容进行检查

- 依赖库是否可用

- 依赖之间有无冲突

- ios版本或swift版本是否符合要求

检查通过之后就会依次继续下载依赖的库和当前库本身的代码文件，并将它和target关联，最后生成.xcworkspace文件供我们打开xcdoe项目







### 进入正题，建立一个自己的Cocoapods库吧！

根据之前所说，我们建立一个cocoapods库的步骤应该是

1. 编写库代码并调试完成

2. 提交库代码至代码git。

3. 给当前提交新建一个tag，名称和你要发布的库的版本号一致，如1.0.0，并推送到remote

4. 使用命令，建立一个新的podSpec文件。

   ```bash
   pod spec create XXX
   # XXX为要创建的podSpec文件名
   ```

5. 根据podSpec文件的格式和规范编写其中的内容。

这是一个很繁琐的过程，如果我们需要从头编写一个新的cocoapods库的话，cocoapod还为我们提供了一个全自动的模板命令

```bash
	pod lib create XXX
	# XXX为要创建的库的名称
```

输入这行命令，cocoapods会自动帮你建立一个xcode工程，包含一个待开发的库，一个调试库用的demo app target，并且自动生成podspec文件和podfile文件，甚至生成结束后还会自动帮你打开Xcode！真正的一键完成！

<img src="/Users/han.fu/Library/Application Support/typora-user-images/image-20200327121148134.png" alt="image-20200327121148134" style="zoom:50%;" />

我们需要做的仅仅是：直接在这个新项目里开始写代码，然后运行调试通过即可。





### 一鼓作气！发布CocoaPod库

建立库结束后，我们后续的工作就是发布我们库的podspec文件到一个pod仓库,

cocoapod官方为我们提供了一个trunk仓库，这是一个开源库，所有提交到这个仓库的库都能被所有人搜索到并使用。如果我们不想让所有人都能下载到我们pod库，那么我们需要建立一个私有的pod仓库。

建立的方法很简单，只用在gitLab上新建一个项目即可，这个git库就可作为我们的存放podSpec的仓库。通过命令cocoapod会自动发布podspec到这个仓库。



以下将介绍向我们的私有pod仓库中发布podspec的流程。

发布一个cocoapods库的步骤应该是

1. 确保库代码已经编写完成并且调试通过。

2. 修改podspec中的版本号为我们这次要发布的版本

3. 修改podSpec中的s.source为我们代码存放的git地址

4. 提交库代码到git中并且打上tag，tag名为我们要发布的版本号

5. 使用命令将我们刚建立的私有pod仓库的地址添加到pod repo中

   ```bash
   pod repo add XXX "http://xxxxxxx"
   # XXX为pod 维护的repo名称 后面的是podspec仓库的地址
   ```

6. 使用命令将当前库的podSpec文件发布到刚才建立的podSpec仓库中

   ```bash
   pod repo push XXX YYYY.podspec --verbose --allow-warnings
   # XXX为我们刚才添加到pod repo中的repo名称
   # YYYY.podspec为我们要提交的podspec文件名
   ```

pod repo push 命令是一个联合命令，他的内部会执行几个动作

1. 检查本podSpec的文件格式是否符合要求

2. 检查本podSpec的中声明的依赖库是否有冲突

3. 下载本podSpec中包含的所有库文件和所有依赖库，并使用xcodeBuild 编译本库检查是否有错误。

4. 如果所有检查都通过，使用本podSpec中表明的name和version建立目录，并使用git 提交到我们前面指定的repo仓库中。

   --verbose 参数会输出详细的信息，方便查错

   --allow-warnings是允许在存在warning的情况下发布库，如果不加的话，就需要解决所有前三个动作中检查出的所有warning才可以发布。

如果发现有冲突或者错误，pod将不会发布这个podSpec，如果所有检查都已经通过，则我们可以在我们的私有仓库看到有一个新的目录，里面存放着我们新发布的podspec

![image-20200327131805429](/Users/han.fu/Library/Application Support/typora-user-images/image-20200327131805429.png)

![image-20200327131752634](/Users/han.fu/Library/Application Support/typora-user-images/image-20200327131752634.png)





之后想要在其他项目中引用我们的库，就可以直接在那个项目的podfile文件中写

```bash
# source 填写我们的podspec的私有仓库
source 'https://git.enerjoy.fun/han.fu/fhcocoaspecs'

target 'Target_Name' do
  pod 'FHTestLib'
	# pod 填写我们的库名

end
```

cocoapod就会自动去source的地址查找下载我们库的podspec了