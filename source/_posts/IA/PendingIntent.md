PendingIntent可以被理解为一种特殊的异步处理机制，在将来某个待定的时刻发生，它的核心可以粗略地汇总成四个字——“异步激发”。

很明显，这种异步激发常常是要跨进程执行的。比如说A进程作为发起端，它可以从系统“获取”一个PendingIntent，然后A进程可以将PendingIntent对象通过binder机制“传递”给B进程，再由B进程在未来某个合适时机，“回调”PendingIntent对象的send()动作，完成激发。

        在Android系统中，最适合做集中性管理的组件就是AMS（Activity Manager
Service），所以它义不容辞地承担起管理所有PendingIntent的职责。这样我们就可以画出如下示意图：(AMS在Android的SystemServer进程中启动)

![](media/267164f4174dd561392f9204468557a4.png)

注意其中的第4步“递送相应的intent”。这一步递送的intent是从何而来的呢？简单地说，当发起端获取PendingIntent时，其实是需要同时提供若干intent的。这些intent和PendingIntent只是配套的关系，而不是聚合的关系，它们会被缓存在AMS中。日后，一旦处理端将PendingIntent的“激发”语义传递到AMS，AMS就会尝试找到与这个PendingIntent对应的若干intent，并递送出去。

**1.获取PendingIntent**

-   public static PendingIntent **getActivity**(Context context, int
    requestCode, Intent intent, int flags) 

-   public static PendingIntent **getBroadcast**(Context context, int
    requestCode, Intent intent, int flags) 

-   public static PendingIntent **getService**(Context context, int requestCode,
    Intent intent, int flags) 

-   public static PendingIntent **getActivities**(Context context, int
    requestCode, Intent[] intents, int flags) 

-   public static PendingIntent **getActivities**(Context context, int
    requestCode, Intent[] intents, int flags, Bundle options)

上面的getActivity()的意思其实是，获取一个PendingIntent对象，而且该对象日后激发时所做的事情是启动一个新activity。也就是说，当它异步激发时，会执行类似Context.startActivity()那样的动作。相应地，getBroadcast()和getService()所获取的PendingIntent对象在激发时，会分别执行类似Context..sendBroadcast()和Context.startService()这样的动作。至于最后两个getActivities()，用得比较少，激发时可以启动几个activity。

第一个和第三个参数比较好理解，第二个参数requestCode表示PendingIntent发送方的请求码，多数情况下为0即可，起区分作用，第四个参数flags可以包含一些既有的标识，比如FLAG_ONE_SHOT、FLAG_NO_CREATE、FLAG_CANCEL_CURRENT、FLAG_UPDATE_CURRENT等等。

**FLAG_NO_CREATE**：如果AlarmManager管理的PendingIntent已经存在，那么将不进行任何操作，直接返回已经存在的PendingIntent，如果PendingIntent不存在了，那么返回null

**FLAG_CANCEL_CURRENT**：如果AlarmManager管理的PendingIntent已经存在，那么将会取消当前的PendingIntent，从而创建一个新的PendingIntent

**FLAG_UPDATE_CURRENT**：如果AlarmManager管理的PendingIntent已经存在，可以让新的Intent更新之前PendingIntent中的Intent对象数据，例如更新Intent中的Extras

**FLAG_ONE_SHOT**：利用
FLAG_ONE_SHOT获取的PendingIntent只能使用一次，即使再次利用上面三个方法重新获取，再使用PendingIntent也将失败。

PendingIntent的匹配规则：

如果两个PendingIntent它们内部的Intent相同并且requestCode也相同，那么这两个PendingIntent就是相同的。

其中Intent的匹配规则是：如果两个Intent的ComponentName和Intent-filter都相同，那么这两个Intent就是相同的。需要注意的是Intent的extra并不参与匹配。

 我们以getActivity()的代码来说明问题：

publicstatic PendingIntent getActivity(Context context, int requestCode,

                                        Intent intent, int flags, Bundle
options)

{

    String packageName = context.getPackageName();

    String resolvedType = intent != null ?

                         
intent.resolveTypeIfNeeded(context.getContentResolver()) : null;

    try

    {

        intent.setAllowFds(false);

        IIntentSender target =
ActivityManagerNative.getDefault().getIntentSender(

                                    ActivityManager.INTENT_SENDER_ACTIVITY,
packageName,

                                    null, null, requestCode, new Intent[] {
intent },

                                    resolvedType != null ? new String[] {
resolvedType } : null,

                                    flags, options);

        return target != null ?new PendingIntent(target) : null;

    }

    catch (RemoteException e)

    {}

    returnnull;

}

其中那句new
PendingIntent(target)创建了PendingIntent对象，其重要性自不待言。然而，这个对象的内部核心其实是由上面那个getIntentSender()函数得来的。而这个IIntentSender核心才是我们真正需要关心的东西。

说穿了，此处的IIntentSender对象是个binder代理，它对应的binder实体是AMS中的PendingIntentRecord对象。PendingIntent对象构造之时，IIntentSender代理作为参数传进来，并记录在PendingIntent的mTarget域。日后，当PendingIntent执行异步激发时，其内部就是靠这个mTarget域向AMS传递语义的。

我们前文说过，PendingIntent常常会经由binder机制，传递到另一个进程去。而binder机制可以保证，目标进程得到的PendingIntent的mTarget域也是合法的IIntentSender代理，而且和发起端的IIntentSender代理对应着同一个PendingIntentRecord实体。示意图如下：

![](media/af8b86176cfed3c52a4cd38f23177218.png)

**2.AMS里的PendingIntentRecord**

 那么PendingIntentRecord里又有什么信息呢？它的定义截选如下：

classPendingIntentRecordextendsIIntentSender.Stub

{

    finalActivityManagerService owner;

   finalKey key;  // 最关键的key域

    finalint uid;

    finalWeakReference\<PendingIntentRecord\> ref;

    boolean sent = false;

    boolean canceled = false;

    String stringName;

    . . . . . .

}

请注意其中那个key域。Key的作用：1.记录信息，2.承担“键值”的作用。

这里的Key是个PendingIntentRecord的内嵌类，其定义截选如下：

finalstaticclassKey{

    finalint type;

    final String packageName;

    final ActivityRecord activity;

    final String who;

    finalint requestCode;

    final Intent requestIntent;        // 注意！

    final String requestResolvedType;

    final Bundle options;

   Intent[] allIntents;    //
注意！记录了当初获取PendingIntent时，用户所指定的所有intent

    String[] allResolvedTypes;

    finalint flags;

    finalint hashCode;

    . . . . . .

    . . . . . .

}

请注意其中的allIntents[]数组域以及requestIntent域。前者记录了当初获取PendingIntent时，用户所指定的所有intent（虽然一般情况下只会指定一个intent，但类似getActivities()这样的函数还是可以指定多个intent的），而后者可以粗浅地理解为用户所指定的那个intent数组中的最后一个intent。现在大家应该清楚异步激发时用到的intent都存在哪里了吧。

**3.AMS里的PendingIntentRecord总表**

 在AMS中，管理着系统中所有的PendingIntentRecord节点，所以需要把这些节点组织成一张表：

final HashMap\<PendingIntentRecord.Key, WeakReference\<PendingIntentRecord\>\>
mIntentSenderRecords

这张哈希映射表的键值类型就是刚才所说的PendingIntentRecord.Key。

以后每当我们要获取PendingIntent对象时，PendingIntent里的mTarget是这样得到的：AMS会先查mIntentSenderRecords表，如果能找到符合的PendingIntentRecord节点，则返回之。如果找不到，就创建一个新的PendingIntentRecord节点。因为PendingIntentRecord是个binder实体，所以经过binder机制传递后，客户进程拿到的就是个合法的binder代理。如此一来，前文的示意图可以进一步修改成下图：

![](media/feb85b48a717e48d92abb8396db8c5f4.png)

**4.PendingIntent的激发动作**

当需要激发PendingIntent之时，主要是通过调用PendingIntent的send()函数来完成激发动作的。PendingIntent提供了多个形式的send()函数，然而这些函数的内部其实调用的是同一个send()，其函数原型如下：

public void send(Context context, int code, Intent intent, OnFinished
onFinished, Handler handler, String requiredPermission) throws CanceledException

<https://my.oschina.net/youranhongcha/blog/196933>
