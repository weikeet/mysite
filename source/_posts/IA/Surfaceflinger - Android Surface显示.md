我们知道Android系统的显示是SurfaceFlinger实现的，那SurfaceFlinger是什么呢，上一节简单讲述了Android第三个开机动画，接下来主要分析一下应用程序的显示，以此来窥探SurfaceFlinger。

**1. Android系统进程**

为了解显示流程，先看Android系统的几个进程，如图：

![](media/dac72f303c79b3b33408f720dce10100.png)

**zygote是什么？有什么作用？**

zygote意为“受精卵“。Android是基于Linux系统的，而在Linux中，所有的进程都是由init进程直接或者是间接fork出来的，zygote进程也不例外。

每一个App其实都是一个单独的dalvik虚拟机和一个单独的进程。当系统里面的第一个zygote进程运行之后，在这之后再开启App，就是开启一个新的进程。而为了实现资源共用和更快的启动速度，Android系统开启新进程的方式，是通过zygote进程fork实现的。所以说，除了第一个zygote进程，其他应用所在的进程都是zygote的子进程，这也正是“受精卵”的作用。

**SystemServer是什么？有什么作用？**

SystemServer也是一个进程，而且是由zygote进程fork出来的。

系统里面重要的服务都是在这个进程里面开启的，比如ActivityManagerService、WindowManagerService等等。

ActivityManagerService，简称AMS，服务端对象，负责系统中所有Activity的生命周期。

WindowManagerService，简称WMS，服务端对象，负责系统中所有Window的显示与隐藏。

**App如何启动？**

Launcher上点击图标—\>Launcher执行startActivity—\>转调到AMS—\>AMS通过socket调到zygote—\>zygote
fork app 进程—\>
App启动。Launcher其实也App，只是它是系统的第一个App当然也是zygote fork出来的。

![](media/a22e3b4b0ca94980f23fa263722cced0.png)

**2. App层Surface**

App启动后打开Activity，然后就是我们熟悉的Activity生命周期。当我们看到Activity的界面时SurfaceFlinger服务已经把App绘制的内容“投射”到显示器上了。那SurfaceFlinger是如何“投射”呢？开机启动动画分析中，我们知道，BootAnimation拿到了Surface后就“为所欲为”了，其实就是调OpenGL和EGL库绘制。同理，Activity如果拿到了Surface那不也是“为所欲为”。这样的话我们就分析一下Activity是如何拿到Surface。

这里直接剧透一下，Surface在ViewRootImpl中，不信看代码：

>   // ViewRootImpl

>   final Surface mSurface = new Surface(); //
>   ViewRootImpl的成员变量，还初始化了

>   // Surface

>   public Surface() {  // 不过，看到这个构造函数实现瞬间吐血，从头找吧

>   }

继续找下去，发现在ViewRootImpl的relayoutWindow方法中，对mSurface进行了操作，原来是懒加载机制，看一下源码：

>   // ViewRootImpl

>   final IWindowSession mWindowSession;

>   // relayoutWindow方法中Surface才真正完整

>   private int relayoutWindow(WindowManager.LayoutParams params, int
>   viewVisibility,

>           boolean insetsPending) throws RemoteException {

>       ……省略……

>       // 转调了Session!

>       int relayoutResult = mWindowSession.relayout(

>               mWindow, mSeq, params,

>               (int) (mView.getMeasuredWidth() \* appScale + 0.5f),

>               (int) (mView.getMeasuredHeight() \* appScale + 0.5f),

>               viewVisibility, insetsPending ?
>   WindowManagerGlobal.RELAYOUT_INSETS_PENDING : 0,

>               mWinFrame, mPendingOverscanInsets, mPendingContentInsets,
>   mPendingVisibleInsets,

>               mPendingStableInsets, mPendingOutsets, mPendingBackDropFrame,

>               mPendingMergedConfiguration, mSurface);

>       ……省略……

>   }

ViewRootImpl转调了Session，这个Session在com.android.server.wm包内，隶属于WindowManager

>   // Session

>   final WindowManagerService mService;

>   public int relayout(IWindow window, int seq, WindowManager.LayoutParams
>   attrs,

>           int requestedWidth, int requestedHeight, int viewFlags,

>           int flags, Rect outFrame, Rect outOverscanInsets, Rect
>   outContentInsets,

>           Rect outVisibleInsets, Rect outStableInsets, Rect outsets, Rect
>   outBackdropFrame,

>           MergedConfiguration mergedConfiguration, Surface outSurface) {

>       ……省略……

>   // 转调了WMS，这一点挺顺的，window相关的毕竟是WMS负责

>       int res = mService.relayoutWindow(this, window, seq, attrs,

>               requestedWidth, requestedHeight, viewFlags, flags,

>               outFrame, outOverscanInsets, outContentInsets, outVisibleInsets,

>               outStableInsets, outsets, outBackdropFrame, mergedConfiguration,
>   outSurface);

>       ……省略……

>   }

Session有点像对外接口，转调了WindowManagerService对应方法，WindowManagerServie通过调用createSurfaceControl方法填充空壳的Surface。

>   // WindowManagerService

>   public int relayoutWindow(Session session, IWindow client, int seq,

>           WindowManager.LayoutParams attrs, int requestedWidth,

>           int requestedHeight, int viewVisibility, int flags,

>           Rect outFrame, Rect outOverscanInsets, Rect outContentInsets,

>           Rect outVisibleInsets, Rect outStableInsets, Rect outOutsets, Rect
>   outBackdropFrame,

>           MergedConfiguration mergedConfiguration, Surface outSurface) {

>       ……省略……

>       result = createSurfaceControl(outSurface, result, win, winAnimator);

>   ……省略……

>   }

>   private int createSurfaceControl(Surface outSurface, int result, WindowState
>   win,

>           WindowStateAnimator winAnimator) {

>       ……省略……

>       // 创建WindowSurfaceController

>       WindowSurfaceController surfaceController;

>       try {

>           Trace.traceBegin(TRACE_TAG_WINDOW_MANAGER, "createSurfaceControl");

>           surfaceController = winAnimator.createSurfaceLocked(win.mAttrs.type,
>   win.mOwnerUid);

>       } finally {

>           Trace.traceEnd(TRACE_TAG_WINDOW_MANAGER);

>       }

>       // 填充空壳的Surface，这一步其实使Surface和Surfaceflinger服务挂上关系

>       if (surfaceController != null) {

>           surfaceController.getSurface(outSurface);

>           if (SHOW_TRANSACTIONS) Slog.i(TAG_WM, "  OUT SURFACE " + outSurface
>   + ": copied");

>       } else {

>           // For some reason there isn't a surface.  Clear the

>           // caller's object so they see the same state.

>           Slog.w(TAG_WM, "Failed to create surface control for " + win);

>           outSurface.release();

>       }

>       return result;

>   }

从上面代码我们可以知道，真正完整的Surface创建者其实是SurfaceController，那么我们继续看一下SurfaceController是如何创建出Surface的。

>   // WindowSurfaceController

>   void getSurface(Surface outSurface) {

>       outSurface.copyFrom(mSurfaceControl); // SurfaceControl中有Surface的数据

>   }

>   public WindowSurfaceController(SurfaceSession s, String name, int w, int h,
>   int format,

>           int flags, WindowStateAnimator animator, int windowType, int
>   ownerUid) {

>       ……省略……

>       // 买一送一服务，要SurfaceControl？Background的SurfaceControl也给你...

>       mSurfaceControl = new SurfaceControlWithBackground(

>               s, name, w, h, format, flags, windowType, ownerUid, this);

>       ……省略……

>   }

mSurfaceControl中有Surface数据，看一下创建mSurfaceControl做了什么。

>   // SurfaceControlWithBackground

>   public SurfaceControlWithBackground(SurfaceSession s, String name, int w,
>   int h, int format,

>           int flags, int windowType, int ownerUid,

>           WindowSurfaceController windowSurfaceController) throws
>   OutOfResourcesException {

>       super(s, name, w, h, format, flags, windowType,
>   ownerUid); // 有点像装饰模式

>       // We should only show background behind app windows that are
>   letterboxed in a task.

>       if ((windowType != TYPE_BASE_APPLICATION && windowType !=
>   TYPE_APPLICATION_STARTING)

>               \|\|
>   !windowSurfaceController.mAnimator.mWin.isLetterboxedAppWindow()) {

>           return;

>       }

>       mWindowSurfaceController = windowSurfaceController;

>       mLastWidth = w;

>       mLastHeight = h;

>       mWindowSurfaceController.getContainerRect(mTmpContainerRect);

>       mBackgroundControl = new SurfaceControl(s, "Background for - " + name,

>               mTmpContainerRect.width(), mTmpContainerRect.height(),
>   PixelFormat.OPAQUE,

>               flags \| SurfaceControl.FX_SURFACE_DIM);

>   }

SurfaceControlWithBackground继承SurfaceControl，直接看源头，SurfaceControl源码如下：

>   // SurfaceControl

>   public class SurfaceControl {

>       private static final String TAG = "SurfaceControl";

>       private static native long nativeCreate(SurfaceSession session, String
>   name,

>               int w, int h, int format, int flags, long parentObject, int
>   windowType, int ownerUid)

>               throws OutOfResourcesException;

>       private static native void nativeRelease(long nativeObject);

>       private static native void nativeDestroy(long nativeObject);

>       ……省略……

>       public SurfaceControl(SurfaceSession session, String name, int w, int h,
>   int format, int flags,

>               SurfaceControl parent, int windowType, int ownerUid)

>                       throws OutOfResourcesException {

>           ……省略……

>           mName = name;

>       // native代码？有点熟悉吗？

>           mNativeObject = nativeCreate(session, name, w, h, format, flags,

>               parent != null ? parent.mNativeObject : 0, windowType,
>   ownerUid);

>           ……省略……

>       }

>   }

可以看出，SurfaceControl其实是对native层的一个封装，这时候大家是不是想起了开机启动的那个SurfaceControl。然后有没有感觉到BootAnimation拿到的Surface和ViewRootImpl拿到的Surface其实是一样，只不过前者是native，后者是java包着native。

那我们就看一下SurfaceControl.java对应的native代码：

>   // android_view_SurfaceControl.cpp

>   static jlong nativeCreate(JNIEnv\* env, jclass clazz, jobject sessionObj,

>           jstring nameStr, jint w, jint h, jint format, jint flags, jlong
>   parentObject,

>           jint windowType, jint ownerUid) {

>       ScopedUtfChars name(env, nameStr);

>       sp\<SurfaceComposerClient\>
>   client(android_view_SurfaceSession_getClient(env, sessionObj));

>       SurfaceControl \*parent =
>   reinterpret_cast\<SurfaceControl\*\>(parentObject);

>       sp\<SurfaceControl\> surface = client-\>createSurface(

>               String8(name.c_str()), w, h, format, flags, parent, windowType,
>   ownerUid);

>       if (surface == NULL) {

>           jniThrowException(env, OutOfResourcesException, NULL);

>           return 0;

>       }

>       surface-\>incStrong((void \*)nativeCreate);

>       return reinterpret_cast\<jlong\>(surface.get());

>   }

果不其然，sp\<SurfaceControl\> surface =
client-\>createSurface这一行和开机启动动画创建Surface的代码一模一样。

其实殊途同归嘛，毕竟最终将画面渲染到帧缓冲器的是SurfaceFlinger服务。

至此，App层Surface的来源我们搞清楚了，趁机再描述一下：

ViewRootImpl持有一个空壳Surface，当触发relayoutWindow时，实际触发的是WMS的relayoutWindow，

而WMS触发relayoutWindow时会通过WindowSurfaceController提供给空壳的Surface数据，

而WindowSurfaceController提供的数据来源其实来自SurfaceControl，SurfaceControl其实是执行的native代码。

简化抽象一下就是:

![](media/3cb0846f7ebda99252006ec571b08c01.png)

**3. Native层Surface**

问题又来了，native层是如何创建Surface的，这个Surface又是什么？

又回到了native层，不过没关系，有了开机启动的基础，我们还是有一个大致的方向的。

接下来我们分析一下native代码，以下涉及的类都是C++的类

回顾一下：

**Surface**

开机启动中了解过，就是一个画布，我们调用OpenGL和EGL库就可以在上面作画，然后SurfaceFlinger服务会将画布内容投到帧缓冲区中。

**SurfaceComposerClient**

不是有个SurfaceFlinger服务吗，这个SurfaceComposerClient就是负责和服务通信。

**SurfaceControl**

SurfaceControl包含了SurfaceComposerClient，又包含了Surface，还有一些release等，其实就是一个小管家

然后看一个类图，这个类图会涉及一些C++语法，不过没关系，主要看一下SurfaceComposerClient，Client，Composer和SurfaceFlinger即可

![](media/bd43de87b83c5c8eeaef250756663398.jpg)

B：Binder

n：native

p：proxy

BnInterface是服务端的接口，BpInterface是客户端的代理接口

根据类图可以知道，SurfaceComposerClient和SurfaceFlinger服务通信是通过Client和Composer实现的，看一下client-\>createSurface做了什么：

>   // Client

>   status_t Client::createSurface(

>           const String8& name,

>           uint32_t w, uint32_t h, PixelFormat format, uint32_t flags,

>           const sp\<IBinder\>& parentHandle, uint32_t windowType, uint32_t
>   ownerUid,

>           sp\<IBinder\>\* handle,

>           sp\<IGraphicBufferProducer\>\* gbp)

>   {

>       ……省略……

>       sp\<MessageBase\> msg = new MessageCreateLayer(mFlinger.get(),

>               name, this, w, h, format, flags, handle,

>               windowType, ownerUid, gbp, \&parent);

>       mFlinger-\>postMessageSync(msg);

>       return static_cast\<MessageCreateLayer\*\>( msg.get() )-\>getResult();

>   }

从这里面看到，我们用的Surface在SurfaceFlinger服务对应的是MessageBase，这个东西是什么？反正是msg，不细追了，看一下SurfaceFlinger.postMessageSync，很理想Queue出现了。

>   // SurfaceFlinger

>   mutable MessageQueue mEventQueue;

>   status_t SurfaceFlinger::postMessageAsync(const sp\<MessageBase\>& msg,

>           nsecs_t reltime, uint32_t /\* flags \*/) {

>       return mEventQueue.postMessage(msg, reltime);

>   }

**4. SurfaceFlinger服务消息、合成、映射和投射**

**（1）消息**

其实SurfaceFlinger服务内部维护一个消息队列，Client创建的Surface其实就是申请创建一个Msg，我们要的数据在Msg中。

App创建完整的Surface，对SurfaceFlinger是生成一个有数据的Msg。

时序图如下：

![](media/b7ef8e4fb11944b4ae4bc7d06cb9fd2d.jpg)

为什么要有Queue机制呢，因为SurfaceFlinger是服务，面对的是多个Client，抽象的描述如下：

![](media/a8412306f55daf13af96743a602ca0a9.png)

然后是如何显示呢，SurfaceFlinger服务是这样处理的：

![](media/912805d3da467bbe7847c8663f653fa9.png)

**（2）映射**

每个Msg对应App是Surface，App在这个Msg上加入绘制数据，而SurfaceFlinger服务工作时，提取Msg上的绘制数据合成最终投射到帧缓存区中。这部分数据对应一个以下模型：

![](media/60f0c664cfbd66b51ab76e26b6ac9f8b.jpg)

Android系统在硬件抽象层中提供了一个Gralloc模块，封装了对帧缓冲区的所有访问操作。

SurfaceFlinger服务加载Gralloc模块，并且获得一个gralloc设备和一个fb设备，这也是为什么SurfaceFlinger服务具备“投射”能力。

有了gralloc设备之后，SurfaceFlinger就可以申请分配一块图形缓冲区(如上图gralloc-buffer)，并且将这块图形缓冲区映射到应用程序的地址空间来(上图左侧蓝区)，以便可以向里面写入要绘制的画面内容。最后，SurfaceFlinger通过fb设备来将已经准备好了的图形缓冲区渲染到帧缓冲区中去，即将图形缓冲区的内容绘制到显示屏中去。相应地，当不再需要使用一块图形缓冲区的时候，就可以通过gralloc设备来释放它，并且将它从地址空间中解除映射。

SurfaceFlinger在这一点处理挺机制的，不多申请任何一块内存。

这时候我们就明确了，Msg中那份数据是什么，其实就是和gralloc映射的那份内存，这个内存我们称之为Buffer。

上面一直说合成然自然就是指合成Buffer。

那Buffer在App是如何被用呢，我们知道Buffer对应App一块内存，而App绘制图形是通过OpenGL和EGL库来完成的，这个过程我们称为Render。

Android提供了两种方式：Canvas ，OpenGL
ES。这两种方式都应该算是标准，提供了一套固定的API，实现和平台无关。

>   Canvas: Android提供了Canvas 2D
>   API用来进行普通图形的绘制的，类似TextView这种应该都是用Canvas API来完成的

>   OpenGL ES: OpenGL ES相关的API是为了3D图形的绘制而准备的

>   Android中OpenGL ES的实现方式有2种:

>   1. 软件实现，用cpu去绘图

>   2. 硬件厂商根据自己GPU提供的实现

>   从Android 4.0开始，支持了硬件加速的Canvas，通过调用EGL和OpenGL ES API实现。

**（3）合成&投射：**

为什么合成？因为有多个绘制数据。这部分多个数据，对于App是多个Surface，对于SurfaceFlinger服务是多个Layer，对于gralloc是多个Buffer。

App层是这样，如下图Launcher这种场景有4个Surface，分别是壁纸，Launcher应用，NavigationBar和Statusbar。其中的每一种只需要在自己的Surface上绘制即可。

![](media/0aac584196065aabc8809ae2cd70380b.png)

SurfaceFlinger这层，是下图这种模式，将左侧四个合成投射到帧缓冲区。

![](media/90a2557c5828f48e8fc17d5e06e1e5a8.png)

**软件硬件加速**

上图的合成路线是不是有点怪异，其实这个怪异是硬件加速和软件加速导致的。

为了理清怪异，先看一下Android中合成的实现方式。

1. 在GPU中合成（利用OpenGL ES进行合成，即OpenGL ES实现）

2. 在display的硬件中进行buffer的合成。（名hardware overlay机制，即硬件设备实现）

有了两种实现方式，我们自己就可以指定合成规则：

1.
将应用内容渲染到暂存缓冲区中，然后将状态栏渲染在它上面，导航栏渲染在它上面，最后将暂存缓冲区传递给显示硬件。

2. 将所有三个缓冲区传递给显示硬件，让硬件处理并显示。

很明显第二种绝对优于第一种，但是硬件有局限性，有些能力达不到（如渐变），所以不能完全用第一种。

Android用的是如下方式：

>   1. SurfaceFlinger提供layer
>   list，询问如何处理这些layer（App设置的硬件加速和软件加速标记）

>   2. 每个layer被标记为overlay或者GLES composition（软件加速标记）

>   3.
>   SurfaceFlinger真正处理那些GLES的合成，而不用去管overlay的合成，最后将overlay的layer和GLES合成后的buffer发送给gralloc（硬件设备处理）。

注：如果屏幕上的画面基本不变化，这时候用GLES
合成的效率要高于overlay(overlay主要是为了render快速变化的图形等)；android
4.4往上支持4个oveylay，如果要合成超过4个layer，系统就会对剩余的使用GLES合成，所以app的layer个数对手机的功耗影响挺大。

**5. 总结一下：**

ViewRootImpl以懒加载方式通过WMS拿到完整的Surface，这个Surface对应SurfaceFlinger的一个Msg，SurfaceFlinger处理这个Msg会调用Gralloc创建一块共享内存，而这块内存就是Surface绘制的缓冲区，然后SurfaceFlinger根据标记对软件加速的layer
list合成再和其他的硬件加速layer list送给硬件合成并显示，

至此，App层Surface我们已经很清楚它的来源和如何经过SurfaceFlinger投射到显示器上。

接下来，我们看一下Activity如何在这个Surface上绘制，是不是整个App的显示就完整，不过这个绘制就不在这分析了，后续分享会有的...

问题1：文中涉及的3个SurfaceFlinger都是那些？

问题2：绘图的缓存区有几份都是那些？

问题2：设置了硬件加速，并且每一个Surface绘制的数据也都符合硬件加速，那么都会被硬件加速吗，为什么？

思考：SurfaceView是如何显示的？
