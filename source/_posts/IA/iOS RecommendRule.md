**推荐机制原则：**

**Content**：被展示

**Placement**：展示content

推荐机制下，每个placement自定义接口，每个content只要实现了placement的接口并且是有效content就能展示

**推荐机制类图：**

![](media/8619fd82347a80ce532663f9a905dbcc.png)

Placement: HomeMain

Content: PhotoVault, SpamCall

**推荐机制配置：**

根Path为**Application.ContentRecommendRule**

Content path：.**Content**

Placement path：.**Placements**

每个Content有三项配置：

**Enable**: 是否打开

**IntervalTimeHours**: 展示时间间隔

**MaxDisplayTimes**: 最多展示次数

每个Placement有三项配置：

**ContentMaxDisplayPerDay**: 每天最多展示多少content

**DisplayIntervalTimeHour**: 展示Content的时间间隔

**Priority**：展示的Content的优先级

![](media/51a3a49e0f82537628d638aa4d09f9e0.png)

**注意事项：**

ContentName和PlacementName为了保持一致，请使用静态常量的方式（RecommendRuleConstants提供）

Protocol中非option修饰的方法必须实现（没实现竟然还能编译过，表示很不理解，
Semantic Issue不能有否则会有Crash）

**placemen**t列表&owner

1.recommend set content — jianwen.wang

2.autofill content — hao.su

3.password safe & password keeper — jianwen.wang

4.password sync — jinlei.wang

5.private note — jianwen.wang

6.photo vault & photo vault shake — long.liu

![](media/0638d153f8f4a47fe5acaadaf39fd2c9.png)

![](media/97af35280efe97eb279104b54acff709.png)

结论：振亮方案
