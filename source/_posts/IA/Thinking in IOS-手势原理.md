本次分享将围绕以下几个方面进行：

1、什么是手势？

2、为什么要有手势？

3、UITouch、UIEvent和UIResponder

4、UIGestureRecognizer

1、什么是手势？

如下图所示的这些手指同屏幕之间产生的交互被称为手势

![](media/23f204b38600fddc5bb9a7d2313e66b5.jpg)

2、为什么要有手势？

   
手势极大的丰富了人机交互的方式，很多手势即实用又有趣，例如Spread手势缩放图片，如果没有这种手势，可以想象我们想放大或缩小一张图片多困难。

3、UIEvent、UITouch和UIResponder

在iOS系统中，当用户手指开始接触屏幕到所有手指都离开屏幕，这整个过程叫做一个multitouch
sequence.这个过程可能有多点触碰。

而在这整个过程中，系统需要一个对象来存储相关的信息，这个对象就是UIEvent。一旦一个新的触摸过程开始，一个新的UIEvent对象就被建立，并且随着触摸状态的改变不断更新自己的信息，一个完整的触摸过程对应一个UIEvent的实例。

那UITouch又是什么呢？前面提到过，在一次触摸过程中，可能有多个手指在触摸，即所谓的多点触控，而一个UITouch的实例则对应一个手指的触摸过程，当UITouch的实例所对应的手指的触摸状态发生改变时，UITouch的状态就会被更新。因为一个触摸过程可能是多点触碰的，因此一个UIEvent实例会有多个UITouch。

UIEvent包含一个属性为UITouch的集合

>   \@property(nonatomic, readonly, nullable) NSSet \<UITouch \*\> \*allTouches;

在iOS中，对于触摸的处理是通过UIResponder这个类来实现的，这是一个抽象类，UIApplication、UIViewController和UIView都继承子UIResponder，通过以下方法就可以接受到触摸事件

>   \- (void)touchesBegan:(NSSet\<UITouch \*\> \*)touches withEvent:(nullable
>   UIEvent \*)event;

>   \- (void)touchesMoved:(NSSet\<UITouch \*\> \*)touches withEvent:(nullable
>   UIEvent \*)event;

>   \- (void)touchesEnded:(NSSet\<UITouch \*\> \*)touches withEvent:(nullable
>   UIEvent \*)event;

>   \- (void)touchesCancelled:(NSSet\<UITouch \*\> \*)touches
>   withEvent:(nullable UIEvent \*)event;

>   \- (void)touchesEstimatedPropertiesUpdated:(NSSet\<UITouch \*\> \*)touches
>   NS_AVAILABLE_IOS(9_1);

   
我们可以在view中实现这四个方法，从而就可以达到处理触摸的效果。例如，如果想要识别用户的长按手势，只需要在touchBegan这个方法中记录下touch开始的时间，然后在touchesEnded方法中记录与开始时间的间隔，如果这个时间间隔大于某个值，你即可以认为这是一次长按过程，然后进行相应的处理。

4、UIGestureRecognizer

   
在实际的开发过程中，我们并不会去实现上面介绍的方法来识别手势，因为太麻烦了，iOS的Framework开发者给我们提供了UIGestureRecognizer这样一个类，并且封装了一系列常用的手势，例如UITapGestureRecognizer，通常我们直接使用这些封装好的手势操作，就可以满足我们大部分的需求。

    使用的方式例子如下：

>       UITapGestureRecognizer \*settingTapGesture = [[UITapGestureRecognizer
>   alloc] initWithTarget:self action:\@selector(settingViewTapAction:)];

>       [self.settingView addGestureRecognizer:settingTapGesture];

 
  UIGestureRecognizer的实现原理也很简单（当然我们看不到源码，摊手表情），UIView存在UIGestureRecognizer对象的情况会将Touch事件先交给UIGestureRecognizer对象进行处理，当Touch事件符合UIGestureRecognizer内设置的条件后，就会触发在创建UIGestureRecognizer时传入的action。

控制UIGestureRecognizer开关的属性如下：

userInteractionEnabled 开启或关闭用户事件。

multipleTouchEnabled 设置是否接收多点触摸事件。

   
需要注意的是UIView在隐藏或透明度小于0.01的情况下是不接收触摸事件的，UIImageView的userInteractionEnabled默认为NO。

UIGestureRecognizer系统已经提供好的手势如下：

![](media/065e3d19665e1b35b76b016ed052cf3c.png)

需要说明的是这些手势只有一个是离散型手势，那就是UITapGestureRecognizer，一旦识别就无法取消，而且只会调用一次手势操作事件。

换句话说其他手势是连续型手势，而连续型手势的特点就是：会多次调用手势操作事件，而且在连续手势识别后可以取消手势。

从下图可以看出两者调用操作事件的次数是不同的：

![](media/0f9ee61bf65c4edfa661210e09a105f4.png)

    手势识别过程中，有如下几种状态：

>   //手势状态枚举值

>   typedef NS_ENUM(NSInteger, UIGestureRecognizerState) {

>       UIGestureRecognizerStatePossible,   // 手势尚未识别，它是默认状态

>       UIGestureRecognizerStateBegan,      // 开始接收连续类型手势

>       UIGestureRecognizerStateChanged,    // 接收接连续类型手势状态变化

>       UIGestureRecognizerStateEnded,      // 结束接收连续类型手势

>       UIGestureRecognizerStateCancelled,  // 取消接收连续类型手势

>       UIGestureRecognizerStateFailed,     // 离散类型的手势识别失败

>       UIGestureRecognizerStateRecognized = UIGestureRecognizerStateEnded
>   // 离散类型的手势识别成功

>   };

![](media/51e47dc6de0b8af89136b55a0695020a.png)

连续类型手势识别过程

![](media/e2566fce133fae2325eff00d997aab49.png)

离散类型手势识别过程

    以下是7种手势的详细介绍：

1 点击手势——UITapGestureRecognizer

>   //设置点击次数，默认为单击

>   \@property (nonatomic) NSUInteger  numberOfTapsRequired;

>   //设置同时点击的手指数

>   \@property (nonatomic) NSUInteger  numberOfTouchesRequired;

2 捏合手势——UIPinchGestureRecognizer

>   //设置缩放比例

>   \@property (nonatomic)          CGFloat scale;

>   //设置捏合速度,只读

>   \@property (nonatomic,readonly) CGFloat velocity;

    3 旋转手势——UIRotationGestureRecognizer

>   //设置旋转角度

>   \@property (nonatomic)          CGFloat rotation;

>   //设置旋转速度

>   \@property (nonatomic,readonly) CGFloat velocity;

4 滑动手势——UISwipeGestureRecognizer

>   /设置触发滑动手势的触摸点数

>   \@property(nonatomic) NSUInteger                       
>   numberOfTouchesRequired;

>   //设置滑动方向

>   \@property(nonatomic) UISwipeGestureRecognizerDirection direction;  

>   //枚举如下

>   typedef NS_OPTIONS(NSUInteger, UISwipeGestureRecognizerDirection) {

>       UISwipeGestureRecognizerDirectionRight = 1 \<\< 0,

>       UISwipeGestureRecognizerDirectionLeft  = 1 \<\< 1,

>       UISwipeGestureRecognizerDirectionUp    = 1 \<\< 2,

>       UISwipeGestureRecognizerDirectionDown  = 1 \<\< 3

>   };

5 长按手势——UILongPressGestureRecognizer

>   //设置触发前的点击次数

>   \@property (nonatomic) NSUInteger numberOfTapsRequired;    

>   //设置触发的触摸点数

>   \@property (nonatomic) NSUInteger numberOfTouchesRequired;

>   //设置最短的长按时间

>   \@property (nonatomic) CFTimeInterval minimumPressDuration;

>   //设置在按触时时允许移动的最大距离 默认为10像素

>   \@property (nonatomic) CGFloat allowableMovement;

6 平移手势——UIPanGestureRecognzer

>   //设置触发拖拽的最少触摸点，默认为1

>   \@property (nonatomic)          NSUInteger minimumNumberOfTouches;

>   //设置触发拖拽的最多触摸点

>   \@property (nonatomic)          NSUInteger maximumNumberOfTouches;  

>   //获取当前位置

>   \- (CGPoint)translationInView:(nullable UIView \*)view;            

>   //设置当前位置

>   \- (void)setTranslation:(CGPoint)translation inView:(nullable UIView
>   \*)view;

>   //设置获取平移速度

>   \- (CGPoint)velocityInView:(nullable UIView \*)view;

7 屏幕边缘平移手势——UIScreenEdgePanGestureRecognzer

>   //设置在屏幕哪个边缘触发手势

>   \@property (readwrite, nonatomic, assign) UIRectEdge edges;

>   typedef NS_OPTIONS(NSUInteger, UIRectEdge) {

>       UIRectEdgeNone   = 0,

>       UIRectEdgeTop    = 1 \<\< 0,

>       UIRectEdgeLeft   = 1 \<\< 1,

>       UIRectEdgeBottom = 1 \<\< 2,

>       UIRectEdgeRight  = 1 \<\< 3,

>       UIRectEdgeAll    = UIRectEdgeTop \| UIRectEdgeLeft \| UIRectEdgeBottom
>   \| UIRectEdgeRight

>   } NS_ENUM_AVAILABLE_IOS(7_0);

 
  如果系统提供的手势不能满足你，你也可以自定义手势。自定义手势需要继承：UIGestrureRecognizer,并且需要导入头文件\#import
\<UIKit/UIGestureRecognizerSubclass.h\>，实现以下四个方法：

>   – touchesBegan:withEvent:  

>   – touchesMoved:withEvent:  

>   – touchesEnded:withEvent:  

>   \- touchesCancelled:withEvent:

   
最后我们介绍下在storyboard中添加手势，在storyboard的控件栏中我们可以看到这些手势控件：

![](media/db2db527d3af93472e4a357ae328ab3f.png)

    使用方法：

    1 直接将手势控件拖到要添加的视图上

![](media/a9461c6f8b5172c6871a249771c36c2f.png)

    2 关联手势事件

![](media/64a15138eb84a855ea094d79a7c343d4.png)

    3 设置手势属性

![](media/67c7dc21cd782c2af0dd52e98ea58796.png)
