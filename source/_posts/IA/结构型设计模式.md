使用设计模式的目的：为了代码可重用性、让代码更容易被他人理解、保证代码可靠性。

对设计模式分类有两条准则，第一个是目的准则，即设计模式用来完成什么工作的，根据**设计模式的目的**分为创建型、结构型、行为型三种。创建型模式与对象的创建有关；结构型模式处理类与对象的组合；行为型模式对类或对象怎样交互和怎样分配职责进行描述。第二个是范围准则，指定模式主要**用于类还是用于对象**。

|      | 目的   |                                              |                                                                       |                                                                                           |
|------|--------|----------------------------------------------|-----------------------------------------------------------------------|-------------------------------------------------------------------------------------------|
|      | 创建型 | **结构型**                                   | 行为型                                                                |                                                                                           |
| 范围 | 类     | Factory Methods                              | **Adapter（类）**                                                     | Interpreter Template Methods                                                              |
|      | 对象   | Abstract Factory Builder Prototype Singleton | **Adapter（对象） Bridge Composite Decorator Facade Flyweight Proxy** | Chain of Responsibility Command Iterator Mediator Memento Observer State Strategy Visitor |

 

今天给大家分享的是结构型模式，其包括Adapter（类、对象）、Bridge、Composite、Decorator、Facade、Flyweight、Proxy。

Adapter（适配器）-类对象结构型

意图

将一个类的接口转换成客户希望的另外一个接口。Adapter模式使得原本由于接口不兼容而不能一起工作的那些类可以一起工作。
     

适用情景

-   使用一个已经存在的类，而它的接口不符合你的需求。

-   创建一个可以复用的类，该类可以与其他不相关的类或不可预见的类（即那些接口可能不一定兼容的类）协同工作。

-   需要一个统一的输出接口，而输入端的类型不可预知。

结构图&&代码示例

类Adapter：

![](media/f7b3c0ee5e80ed59488f0ce0e1364b3d.png)

Adapter继承Adaptee实现Target接口

//Target角色

public interface FiveVolt {

public int getVolt5();

}

//Adaptee角色，需要被转换的对象

public class Volt220 {

public int getVolt220() {

return 220;

}

}

//Adapter角色，将220V的电压转换成5V的电压

public class VoltAdapter extends Volt220 implements FiveVolt {

    \@Override

    public int getVolt5(){

    return 5;

}

}

public class Test {

    public static void main(String[] args) {

VoltAdapter adapter = new VoltAdapter();

System.out.println(“输出电压：”+adapter.getVolt5());

}

}

对象Adapter：

![](media/1d0acfbb80356d53a90b186c2a565b76.png)

Adapter组合Adaptee（拥有该对象）实现Target接口

//Target角色

public interface FiveVolt {

public int getVolt5();

}

//Adaptee角色，需要被转换的对象

public class Volt220 {

public int getVolt220() {

return 220;

}

}

//对象适配器

public class VoltAdapter implements FiveVolt {

Volt220 mVolt220;

public VoltAdapter(Volt220 adaptee) {

mVolt220 = adaptee;

}

public int getVolt220() {

    return mVolt220.getVolt220();

}

\@Override

public int getVolt5() {

return 5;

}

}

public class Test {

    public static void main(String[] args) {

VoltAdapter adapter = new VoltAdapter(new Volt220);

System.out.println(“输出电压：”+adapter.getVolt5());

}

}

总结

类Adapter与对象Adapter对比：对象Adapter直接将要被适配的对象传递到Adapter中，使用组合的形式实现接口兼容效果。这比类适配器方式更为灵活，它的另一个好处是被适配对象中的方法不会暴露出来，而类适配器出于继承了被适配对象，因此，被适配对象类的函数在Adapter类中也都含有，这使得Adapter类出现一些奇怪的接口，用户使用成本较高。因此对象适配器模式更加灵活、实用。

实际开发中Adapter通常应用于进行不兼容的类型转换的场景，还有一种就是输入有无数种情况，但是输出类型是统一的，我们可以通过Adapter返回一个统一的输出，而具体的输入留给用户处理，内部只需知道输出的是符合要求的类型即可。使用Adapter模式的过程中建议尽量使用对象适配器的实现方式，多用合成或者聚合，少用继承。当然，具体问题具体分析，根据需要来选用实现方式，最适合的才是最好的。

Android中Adapter使用范例为ListView的Adapter，用户的Item
View各式各样，但最终都是属于View类型，ListView只需要知道getView返回的是一个View即可，具体是什么View类型并不需要ListView关心。

Bridge（桥接）-对象结构型

意图

将抽象部分与它的实现部分分离，使它们都可以独立地变化。          

适用情景

-   不希望在抽象和它的实现部分之间有一个固定的绑定关系。例如这种情况可能是因为，在程序运行时刻实现部分应可以被选择或者切换。

-   类的抽象以及它的实现都应该可以通过生成子类的方法加以扩充。这时Bridge模式使你可以对不同的抽象接口和实现部分进行组合，并分别对它们进行扩充。

-   对一个抽象的实现部分的修改应对客户不产生影响，即客户的代码不必重新编译。

-   想对客户完全隐藏抽象的实现部分。

-   想在多个对象间共享实现，但同时要求客户并不知道这一点

结构图&&代码示例

![](media/e66c8618e7b1e91cffa72df521a70cb2.png)

只有一个Implementor实现的时候，就没有必要创建一个抽象的Implementor了。

public interface DisplayScreen {

public double getSize();

public String getMaterial();

}

public class SumSungDisplayScreen implements DisplayScreen {

\@Override

public double getSize() {

           return 15.1;

}

\@Override

public String getMaterial() {

           return“OLED”;

}

}

public class LGDisplayScreen implements DisplayScreen {

\@Override

public double getSize() {

           return 14.3;

}

\@Override

public String getMaterial() {

           return“TFT”;

}

}

public abstract class Computer {

//声明一个私有成员变量引用实现部分的对象

private DisplayScreen mDisplayScreen;

//通过实现部分对象的引用构造抽象部分的对象参数为实现部分对象的引用

public Computer (DisplayScreen displayScreen) {

mDisplayScreen = displayScreen;

}

//通过调用实现部分具体的方法实现具体的功能

public abstract void displayComputerConfig();

}

public class HPComputer extends Computer {

       String maker = “HP”;

public HPComputer (DisplayScreen displayScreen) {

super(displayScreen);

}

//对父类抽象部分中的方法进行扩展

public void displayComputerConfig () {

//对Computer中的方法进行扩展

System.out.printlen(maker + “size:” + mDisplayScreen.getSize());

System.out.printlen(maker + “material:” + mDisplayScreen.getMaterial());

 

}

}

public class LenovoComputer extends Computer {

       String maker = “Lenovo”;

public LenovoComputer (DisplayScreen displayScreen) {

super(displayScreen);

}

//对父类抽象部分中的方法进行扩展

public void displayComputerConfig () {

//对Computer中的方法进行扩展

System.out.printlen(maker + “size:” + mDisplayScreen.getSize());

System.out.printlen(maker + “material:” + mDisplayScreen.getMaterial());

}

}

public class Client {

public static void main(String[] args) {

        SumSungDisplayScreen sumsungScreen = new SumSungDisplayScreen();

HPComputer hp = new HPComputer(sumsungScreen);

hp.displayComputerConfig();

LGDisplayScreen lgdScreen = new LGDisplayScreen ();

LenovoComputer lenovo = new LenovoComputer(lgdScreen);

lenovo. displayComputerConfig();

}

}

总结

Bridge桥接模式可以应用到许多开发中，但是它应用得却不多，一个很重要的原因是对于抽象与实现的分离的把握，是不是需要分离、如何分离？对设计者来说要有一个恰到好处的分寸。不管怎么说，Bridge模式的优点我们毋庸置疑，分离抽象与实现、灵活的扩展以及对客户来说透明的实现等。但是使用Bridge模式也有一个不明显的缺点，就是不容易设计，对开发者来说要有一定的经验要求。因此，对Bridge模式应用来说，理解简单实用难。

Android中Bridge模式应用实例为View视图层级中，CheckBox、CompoundButton、Button、TextView、View之间构成一个继承关系的视图层级，每一个层级仅仅对这一类型控件的描述，定义了该类控件所佑佑的基本属性和行为，但是将他们真正绘制到屏幕的部分是由与View相关的功能实现类DisplayList、HardwareLayer和Canvas负责。

Composite（组合） -对象结构型

意图

将对象组合成树形结构以表示“整体-部分”的层次结构。Composite使得用户对单个对象和组合对象的使用具有一致性。 
        

适用情景

-   想表示对象的部分-整体层次结构。

-   希望用户忽略组合对象与单个对象的不同，用户将统一地使用组合结构中的所有对象。

结构图&&代码示例

![](media/2b60e6461f1850acc5c3a01c9ce99a4c.png)

public abstract class Department {

private String name;

private String leaderName;

private int memberSize;

private List\< Department \> departments = new ArrayList\<\>();

public Department (String name, String leaderName, int memberSize) {

[this.name](http://this.name) = name;

this.leaderName = leaderName;

this.memberSize = memberSize;

}

//具体的逻辑方法由子类实现

public abstract void doSomething();

public abstract void addChild(Department child);

public abstract void removeChild(Department child);

public abstract Department getChildren(int index);

}

public class Organization extends Department {

//存储节点的容器

public Organization (String name, String leaderName, int memberSize) {

super(name, leaderName, memberSize);

}

\@Override

public void doSomething() {

System.out.println(name);

If (null != departments) {

for (Department c : departments) {

c.doSomething();

}

}

}

\@Override

public void addChild(Department child) {

departments.add(child);

}

\@Override

public void removeChild(Department child) {

departments.remove(child);

}

\@Override

public Department getChildren(int index) {

departments.get(index);

}

}

 

public class Team extends Department {

public Team (String name, String leaderName, int memberSize) {

super(name, leaderName, memberSize);

}

\@Override

public void doSomething() {

System.out.println(“组名称:”+name);

System.out.println(“组负责人:”+ leaderName);

System.out.println(“组人员:”+ memberSize);

}

\@Override

public void addChild(Department child) {

throw new UnsupportedOperationException(name +“暂时没有下属部门!”);

}

\@Override

public void removeChild(Department child) {

throw new UnsupportedOperationException(“name +“暂时没有下属部门!”);

}

\@Override

public Department getChildren(int index) {

throw new UnsupportedOperationException(“name +“暂时没有下属部门!”);

}

}

public class Client {

Public static void main(String[] args) {

//构造一个根节点

Department ihandy = new Organization(“IHandy”,“xxx”,300);

 

Department ia = new Organization (“IA”,“xxx”,38);

Department iaDev = new Team (“IADev”,“xxx”,15);

ia.addChild(iaDev);

ihandy.addChild(ia);

 

Department game = new Organization (“Game”,“xxx”,28);

Department gameDev = new Team (“GameDev”,“xxx”,13);

game.addChild(gameDev);

ihandy.addChild(game);

 

//执行方法

ihandy.doSomething();

}

}

总结

Composite模式所提供的属性层次结构使得我们能够一视同仁地对待单个对象和对象集合，不过这是以牺牲类的单一原则换来的，而且组合模式是通过继承来实现的，这样的做法缺少弹性。

Composite模式可以清楚地定义分层次的复杂对象，表示对象的全部或部分层次，它让高层模块忽略了层次的差异，方便对整个层次结构进行控制。高层模块可以一致地使用一个组合结构或其中单个对象，不必关心处理的是单个对象还是整个组合结构，简化了高层模块的代码。在组合模式中增加新的枝干构件和叶子构件都很方便，无须对现有的类库进行任何修改，符合“开闭原则”。组合模式为树形结构的面向对象实现提供了一种灵活的解决方案，通过叶子对象和枝干对象的递归组合，可以形成复杂的树形结构，但对树形结构的控制却非常简单。缺点是在新增构件时不好对枝干中的构件类型进行限制，不能依赖类型系统来施加这些约束，因为在大多数情况下，它们都来自于相同的抽象层，此时，必须进行类型检查来实现，这个实现过程较为复杂。

Android中最为经典的Composite模式应用为View和ViewGroup的嵌套组合。

Decorator（装饰）-对象结构型

意图

动态地给一个对象添加一些额外的职责。就增加功能来说，Decorator模式相比生成子类更为灵活。 
        

适用情景

-   在不影响其他对象的情况下，以动态、透明的方式给单个对象添加职责。

-   处理那些可以撤销的职责。

-   当不能采用生成子类的方法进行扩充时。一种情况是可能有大量独立的扩展，为支持每一种组合将产生大量的子类，使得子类数目呈爆炸式增长。另一种情况可能是因为类定义被隐藏，或类定义不能生成子类。

结构图&&代码示例

![](media/680a9bf4fe8fdf6ed3d99889beab6558.png)

public abstract class Person{

          public abstract void dress();

}

public class XiaoMing extends Person {

\@Override

public void dress () {

System.out.println(“穿内衣！”)

}

}

public abstract class PersonCloth extends Person {

//持有一个Component对象的引用

private Person person;

//构造方法需要一个Component类型的对象

public PersonCloth (Person person) {

this. person = person;

}

\@Override

public void dress () {

    person. dress();

}

}

public class ClothToAttendMeeting extends PersonCloth {

protected ClothToAttendMeeting (Person person) {

super(person);

}

private void dressShirt() {

System.out.println(“穿个衬衫!”);

}

private void dressSuit() {

System.out.println(“穿个西装!”);

}

private void dressNecktie() {

System.out.println(“带个领带!”)

}

private void dressLeatherShoes() {

System.out.println(“穿个皮鞋!”)

}

\@Override

public void dress () {

super. dress();

    dressShirt();

dressSuit();

dressNecktie();

    dressLeatherShoes();

}

}

public class ClothToGetTogetherWithFriends extends PersonCloth {

protected ClothToGetTogetherWithFriends (Person person) {

super(person);

}

private void dressShorts() {

System.out.println(“穿个短裤!”);

}

private void dressVest() {

System.out.println(“穿个大背心!”);

}

private void dressSlipper() {

System.out.println(“穿个拖鞋!”)

}

\@Override

public void dress () {

super. dress();

dressShorts();

    dressVest();

dressSlipper();

}

}

public class Client {

public static void main(String[] args) {

    Person xiaoming = new XiaoMing ();

    PersonCloth xiaomingToAttendMeeting = new ClothToAttendMeeting (xiaoming);

    xiaomingToAttendMeeting.dress();

 

    PersonCloth xiaomingToGetTogetherWithFriends = new
ClothToGetTogetherWithFriends (app);

    xiaomingToGetTogetherWithFriends.dress();

    //不改变对象

    xiaoming.dress();

}

}

总结

Decorator模式是以对客户端透明的方式扩展对象的功能，是继承关系的一个替代方案，应该为所装饰的对象增强功能。

Android中Decorator模式应用实例为activity的oncreate方法中进行相关初始化操作，在oncreate方法中用户方法的调用就类似Decorator模式中装饰者的职责，只不过这里没有保持对组件类的引用。

Facade（外观）-对象结构型

意图

为子系统中的一组接口提供一个一致的界面，Facade模式定义了一个高层接口，这个接口使得这一子系统更加容易使用。 
       

适用情景

-   为一个复杂子系统提供一个简单接口。子系统往往因为不断演化而变得越来越复杂，甚至可能被替换。大多数模式使用时都会产生更多更小的类，使得子系统更具可重用性的同时也更容易对子系统进行定制、修改（像咱们的libmax、libdevice、model等可重用模块），这种易变性使得隐藏子系统的具体实现变得尤为重要。Facade可以提供一个简单统一的接口，对外隐藏子系统的具体实现、隔离变化。

-   当你需要构建一个层次结构的子系统时（像android结构体系，framewor层与application层），使用Facade模式定义子系统中每层的入口点。如果子系统之间是相互依赖的，你可以让它们仅通过Facade接口进行通信，从而简化了它们之间的依赖关系。

结构图&&代码示例

![](media/f09fd8d51c40a1efeeaa9370c5fc3e62.png)

public interface Phone {

public void dail();

public void hangup();

}

class PhoneImpl implements Phone {

\@Override

public void dail() {

System.out.println(“打电话”);

}

\@Override

public void hangup() {

System.out.println(“挂电话”);

}

}

public interface Camera {

public void open();

public void takePicture();

public void close();

}

class SamsungCamera implements Camera {

\@Override

public void open() {

System.out.println(“打开相机”);

}

\@Override

public void takePicture() {

System.out.println(“拍照”);

}

\@Override

public void close() {

System.out.println(“关闭相机”);

}

}

public class MobilePhone {

private Phone mPhone = new PhoneImpl();

private Camera mCamera = new SamsungCamera();

public void dail() {

mPhone.dail();

}

public void videoChat() {

System.out.println(“视频聊天接通中”);

mCamera.open();

mPhone.dail();

}

public void hangup() {

mPhone.hangup();

}

public void takePicture() {

mCamera.open();

mCamera.takePicture();

}

public void closeCamera() {

mCamera.close();

}

}

public class Test {

public static void main(String[] args) {

MobilePhone nexus6 = new MobilePhone();

nexus6.takePicture();

nexus6.videoChat();

}

}

总结

示例代码中外观模式就是统一接口封装。将子系统的逻辑、交互隐藏起来，为用户提供一个高层次的接口，使得系统易用，同时也对外隐藏了具体的实现，这样即使具体的子系统发生变化，用户也不会感知到，内部变化对用户不可见，从而达到将变化隔离开来，使得系统更灵活。

Facade模式在日常中使用率高，精髓在于封装二字。通过一个高层次结构为用户提供统一的API入口，使得用户通过一个类型就基本能够操作整个系统，这样减少了用户使用成本，提升系统灵活性。

优点对客户程序隐藏子系统细节，减少了客户对于子系统的耦合；对子系统接口封装，使得系统易于使用。

缺点外观类接口膨胀。由于子系统的接口都有外观类统一的对外暴露，使得外观类的API接口较多，也在一定程度上增加了用户使用维护成本；没有遵守开闭原则，当业务出现变更时，可能需要直接修改外观类。

Android中的ContextImpl内部封装了很多不同子系统的操作，例如Activity的跳转、发送广播、启动服务、设置壁纸等，这些工作并不是在ContextImpl中实现，而是转交给了具体的子系统进行处理。通过Context这个抽象了、定义了一组接口，ContextImpl实现Context定义的接口，用户可以通过Context这个接口统一进行与Android系统的交互，这样用户无需了解每个子系统的详情。

Flyweight（享元）-对象结构型

意图

运用共享技术有效地支持大量细粒度的对象。         

适用情景

-   系统中存在大量的相似对象。

-   细粒度的对象都具备较接近的外部状态，而且内部状态与环境无关，也就是说对象没有特定身份。

-   需要缓冲池的场景。

结构图&&代码示例

![](media/f068e79cf7d7a558cc63c4d9bebaf66a.png)

//定义一个Ticket接口，展示车票信息的方法

public interface Ticket {

public void showTicketInfo (String bunk);

}

class TrainTicket implements Ticket {

public String from;

public String to;

public String bunk;//铺位

public int price;

TrainTicket(String from, String to) {

this.from = from;

[this.to](http://this.to) = to;

}

\@Override

public void showTicketInfo(String bunk) {

price = new Random().nextInt(300);

}

}

//没有使用缓存模式，每次都创建一个对象

public class TicketFactory {

public static Ticket getTicket(String from, String to) {

return new TrainTicket(from,to);

}

}

//车票工厂，以出发地和目的地为key缓存车票

public class TicketFactory {

static Map\<String,Ticket\> sTMap = new ConcurrentHashMap\<String,Ticket\>();

public static Ticket getTicket(String from,String to) {

String key = from+”-”+to;

if (sTMap.containsKey(key)) {

return sTMap.get(key);

}else{

Ticket ticket = new TrainTicket(from,to);

sTMap.put(key,ticket);

return ticket;

}

}

}

public class Test {

public static void main(String[] args) {

Ticket ticket = TicketFactory.getTicket(“北京”,”天津”);

}

}

![](media/06f6578b4d658d04da9a0e5c4e47299c.png)

//定义一个Ticket接口，展示车票信息的方法

public abstract class Ticket {

public abstract void showTicketInfo (String bunk);

}

class TrainTicket extends Ticket {

public String from;

public String to;

public String bunk;//铺位

public int price;

TrainTicket(String from, String to) {

this.from = from;

[this.to](http://this.to) = to;

}

\@Override

public void showTicketInfo(String bunk) {

price = new Random().nextInt(300);

}

}

//没有使用缓存模式，每次都创建一个对象

public class TicketFactory {

public static Ticket getTicket(String from, String to) {

return new TrainTicket(from,to);

}

}

//车票工厂，以出发地和目的地为key缓存车票

public class TicketFactory {

static Map\<String,Ticket\> sTMap = new ConcurrentHashMap\<String,Ticket\>();

public static Ticket getTicket(String from,String to) {

String key = from+”-”+to;

if (sTMap.containsKey(key)) {

return sTMap.get(key);

}else{

Ticket ticket = new TrainTicket(from,to);

sTMap.put(key,ticket);

return ticket;

}

}

}

public class Test {

public static void main(String[] args) {

Ticket ticket = TicketFactory.getTicket(“北京”,”天津”);

}

}

总结

Flyweight模式简单，但作用在一些场景很重要。可以大大减少应用程序创建的对象，降低程序内存占用率，增强程序性能，提高系统的复杂性。在使用时需要分离出外部状态和内部状态，外部状态固化不应随内部状态改变而改变，否则导致系统的逻辑混乱。

有点大幅度降低内存中对象数量。但是该模式使得系统复杂性提高，因为为了使对象可以共享，需要将一些状态外部化，使得程序逻辑复杂化，且读取外部状态使得运行时间稍微延长。

在Android中，Message.Obtain()函数返回一个Message对象，运用的Flyweight模式，不是每次都new一个Message对象，而是会从被回收的对象池中获取Message对象，因为Message内部构建一个链表来维护一个被回收的Message对象的对象池，当用户调用obtain函数时，会优先从池中取，如果池中没有则新建一个Message对象。

Proxy（代理）-对象结构型

意图

为其他对象提供一种代理以控制对这个对象的访问。         

适用情景

-   当无法或不想直接访问某个对象或访问某个对象存在困难时，可以通过一个代理对象来间接访问，为了保证客户端使用的透明性，委托对象与代理对象需要实现相同的接口。

-   需要延迟加载一些占用内存较大的对象的时候使用。

结构图&&代码示例

![](media/98bee3c58676e76127d3291873c0c38e.png)

public abstract class JudgeQuestion{

       public abstract void requirement();

       public abstract void isAgree();

}

public class XiaoMing extends JudgeQuestion {

\@Override

public void requirement () {

System.out.println(“提出诉求”);

}

\@Override

public void isAgree () {

       System.out.println(“同意”);

}

}

public class Lawyer extends JudgeQuestion {

//持有真实对象的引用

private JudgeQuestion someone;

public Myself (JudgeQuestion someone) {

        this.someone = someone;

}

\@Override

public void requirement () {

        someone. requirement();

}

\@Override

public void isAgree () {

        someone.isAgree();

}

}

public class Client {

public static void main(String[] args) {

        JudgeQuestion xiaoming = new XiaoMing ();

        Lawyer lawyer = new Lawyer (xiaoming);

        lawyer. requirement();

        lawyer. isAgree();

}

}

动态代理 

public class DynamicProxy implements InvocationHandler {

    private Object obj;//被代理的类引用

    public DynamicProxy(Object obj){

        this.obj = obj;

    }

    \@Override

    public Object invoke(Object proxy,Method method,Object[] args) throws
Throwable {

        Object result = method.invoke(obj,args);

        return result;

    }

}

总结

Proxy模式应用广泛，缺点就是对类的增加，这是所有设计模式都有的通病。

Android中使用Proxy模式的实例为进程间通讯-Binder机制、ActivityManagerProxy代理类，其具体代理的是ActivityManagerNative的子类ActivityManagerService。

最终总结

Adapter模式主要是为了解决两个已有接口之间不匹配的问题，它不考虑这些接口是怎样实现也不考虑它们各自可能会如何演化。这种方式不需要对两个独立设计的类中的任一一个进行重新设计就能使得它们协同工作。当你发现两个不兼容的类必须同时工作时，就使用Adapter模式，可以避免代码重复。Adapter复用已有接口。

Bridge模式对抽象接口与它的实现部分进行桥接，会在系统演化时适应新的实现。

Facade模式定义了一个新接口,通过这个新接口操作包含多个子系统的整个系统。

Decorator使你能够不需要生成子类即可给对象添加职责，避免了静态实现所有功能组合，从而导致子类急剧增加。不用定义新类。

Composite旨在构造类，使多个相关的对象能够以统一的方式处理，而多重对象可以被当做一个对象来处理，它重点不在于修饰而在于表示。不用定义新类。

Proxy保留了指向另一个对象的引用，用来解决直接访问实体不方便或不符合需要（如实体在远程设备上，访问受限或者实体是持久存储的）的问题，为这个实体提供一个替代者。

很多情况下是使用组合模式解决问题的。

在实用设计模式之前一定要想清除利弊关系，不要造成设计模式的滥用。因为所有的设计模式都会导致类数量的增加，某些时候甚至会导致类的泛滥、失控，而且设计模式基本上都会有定义抽象类、接口的工作，做这些工作最佳时机应该在需求变更不频繁的时候，否则一旦发生修改会导致大量工作。所以计模式一般在代码需要重构的时候考虑，在需求简单、功能单一的情况下，没有必要考虑设计模式，用最简单、易读的代码实现功能才是真理。
