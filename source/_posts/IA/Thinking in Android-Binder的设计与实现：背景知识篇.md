![](media/68d96d7f173da91ed352bf31e183b265.jpg)

   
我们作为计算机从业人员都知道进程的概念，但是在编写Android程序过程中我们却很少感受到进程的存在，或者说很少感受到进程我们正在进行跨进程通讯，举个🌰，我们使用系统服务，比方说监听传感器，传感器的服务肯定是运行在系统进程的，然而我们短短几行代码就可以获得传感器的服务，如下：

>   SensorManager sensorManager = (SensorManager)
>   context.getSystemService(Context.SENSOR_SERVICE);

>   sensorManager.registerListener(new SensorEventListener() {

>       \@Override

>       public void onSensorChanged(SensorEvent event) {

>       //做想做的一些事情    

>       }

>       \@Override

>       public void onAccuracyChanged(Sensor sensor, int accuracy) {

>       }

>   }, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
>   SensorManager.SENSOR_DELAY_GAME);

   
再比如我们想要将Activity放到另外一个进程，仅需要在AndroidManifest文件注册Activity的地方中加入一行android:process=“进程名”，我们想要用主进程的Activity启动该Activity的方式也同启动同进程Activity别无二致，想要在Activity间传递数据也非常容易。我们几乎感受不到进程间通讯所带来的阻力，我们使用系统服务，仿佛在服务本身就运行在当前进程一样。在Android开发中，四大组件也都是支持进程间通讯的，在我们的程序中广泛的使用了ContentProvider进行同步的跨进程通讯，为我们带来了很大的方便。在底层，这一切都是通过Binder跨进程通讯机制来实现的。

   
在前几次Android操作系统重要服务（AMS、WMS）的分享中，我们也多次提及进程间通讯机制Binder，可以说理解Binder也是理解Android操作系统源码至关重要的前提，我们可以看到一般分析Android操作系统源码的书籍，都会将Binder放在头几章介绍，网上分析Binder的博客现在也已经很多了，分享一些感觉比较好的供大家参考。

《深入剖析Android新特性》作者强波的博客*https://paul.pub/android-binder-driver/*

 
  《Android系统源代码情景分析》作者罗升阳老师的博客*https://blog.csdn.net/Luoshengyang/article/details/6618363*

《深入理解Android》作者邓凡平的博客*https://blog.csdn.net/Luoshengyang/article/details/6618363*

负责MIUI系统的大神Gityuan的博客*http://gityuan.com/2015/10/31/binder-prepare/*

低调的女神universus的博客*https://blog.csdn.net/universus/article/details/6211589*

universus的博客是2011年的，可以说是最早分析Binder的博客之一了，并且分析的深入浅出，高屋建瓴，很多后来的解读Binder的博客也都参考了universus对Binder的理解，甚至有歪果仁专门将这篇文章翻译为英文的，在技术领域从来都是我们翻译歪果仁的文章。。。可惜的是，这位大神在写完这篇文章之后就销声匿迹了，成为了一个都市传说。

Binder其实也不是Android提出来的一套新的进程间通信机制，它是基于OpenBinder来实现的。OpenBinder最先是由Be
Inc.开发的，接着Palm Inc.也跟着使用。现在OpenBinder的作者Dianne
Hackborn就是在Google工作，负责Android平台的开发工作。

   
OpenBinder的介绍网址：<http://www.angryredplanet.com/~hackbod/openbinder/docs/html/BinderIPCMechanism.html> 
  

    Dianne Hackbor就是下面这位女神，我们曾在Fragment的分享中提到过她：

![](media/08517e6beea94f8187069a49c7993917.jpg)

    Dianne Hackbor的个人网站：*http://www.angryredplanet.com/\~hackbod/*

    Google官网上关于Dianne
Hackbor对Android多进程通讯设计与实现描述的一篇博客：<https://android-developers.googleblog.com/2010/04/multitasking-android-way.html>

   
理解Binder比较难的地方在于需要有较多的操作系统背景知识，本次分享将先介绍理解Binder所需要的背景知识，一些我们在Android应用层开发较少涉及到的概念，我们只有对这些概念有一个较清晰的认识后，才能进一步去理解Binder。

    接下来我们将围绕以下几个点进行本次的分享：

1.  什么是进程？什么是线程？

2.  什么是用户空间、内核空间？

3.  什么是系统调用？

4.  什么是驱动程序？

5.  传统的进程间通讯方式有哪些？

6.  为什么Android会选择Binder作为主要的进程间通讯方式？

**1、什么是进程？什么是线程？**

进程（process），是计算机中已运行程序的实体。进程为曾经是分时系统的基本运作单位。在面向进程设计的系统（如早期的UNIX，Linux
2.4及更早的版本）中，进程是程序的基本执行实体；在面向线程设计的系统（如当代多数操作系统、Linux
2.6及更新的版本）中，进程本身不是基本运行单位，而是线程的容器。程序本身只是指令、数据及其组织形式的描述，进程才是程序（那些指令和数据）的真正运行实例。若干进程有可能与同一个程序相关系，且每个进程皆可以同步（循序）或异步（平行）的方式独立运行。现代计算机系统可在同一段时间内以进程的形式将多个程序加载到存储器中，并借由时间共享（或称时分复用），以在一个处理器上表现出同时（平行性）运行的感觉。同样的，使用多线程技术（多线程即每一个线程都代表一个进程内的一个独立执行上下文）的操作系统或计算机体系结构，同样程序的平行线程，可在多CPU主机或网络上真正同时运行（在不同的CPU上）。

 线程（英语：thread）是操作系统能够进行运算调度的最小单位。它被包含在进程之中，是进程中的实际运作单位。一条线程指的是进程中一个单一顺序的控制流，一个进程中可以并发多个线程，每条线程并行执行不同的任务。在Unix
System V及SunOS中也被称为轻量进程（lightweight
processes），但轻量进程更多指内核线程（kernel thread），而把用户线程（user
thread）称为线程。

    总结：**进程是资源分配的基本单位，线程是调度的基本单位。**

**2、什么是用户空间、内核空间？**

**3、什么是系统调用？**

**4、什么是驱动程序？**

**5、传统的进程间通讯方式有哪些？**

**6、为什么Android会选择Binder作为主要的进程间通讯方式？**
