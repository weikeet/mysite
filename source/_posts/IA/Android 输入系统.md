**内容结构：**

**一、输入系统简介**

**二、IMS的构成**

**三、输入事件的读取加工、派发（InputReader & InputDispatcher）**

**四、输入事件的发送、接收与反馈**

**五、View事件分发机制**

**一、输入系统简介：**

Android 输入子系统名义上是由遍历系统多个层的事件管道组成。

在最底层，物理输入设备会生成描述状态更改（例如按键按压和触摸接触点）的信号。设备固件以某种方式编码和传输这些信号，例如向系统发送
USB HID 报告或在 I2C 总线上产生中断。然后，信号由 Linux
内核中的设备驱动程序解码。Linux
内核为许多标准的外围设备提供驱动程序，特别是那些符合 HID
协议的外围设备。然而，原始设备制造商 (OEM)
通常必须为在低级别紧密集成到系统的嵌入式设备（如触摸屏）提供自定义驱动程序。

输入设备驱动程序负责通过 Linux
输入协议将设备特定信号转换为标准输入事件格式。Linux 输入协议在
linux/input.h内核头文件中定义了一组标准事件类型和代码。这样一来，内核之外的组件就不需要关注物理扫描代码、HID
用途、I2C 消息、GPIO 引脚等方面的详细信息。

       接下来，Android EventHub 组件通过打开与每个输入设备关联的 evdev
驱动程序从内核读取输入事件。然后，Android InputReader
组件根据设备类别解码输入事件，并生成 Android 输入事件流。在此过程中，Linux
输入协议事件代码将根据输入设备配置、键盘布局文件和各种映射表，转化为 Android
事件代码。具体映射表在/system/usr/keylayout/目录下的kl文件中。

  下面通过实验表述上述过程：

>   Android系统提供了getevent与sendevent两个工具供开发者从设备节点中直接读取输入事件或写入输入事件。

>   模拟截屏操作：

>   一个事件由type、code、 value组成.

>   type: EV_KEY, EV_ABS, EV_REL,EV_SW

>   code: 硬件扫描码

>   value: down/up/move/…

>   sendevent /dev/input/event1 1 116 1

>   sendevent /dev/input/event1 0 0 0

>   sendevent /dev/input/event1 1 114 1

>   sendevent /dev/input/event1 0 0 0

    最后，InputReader 将输入事件发送到
InputDispatcher，后者将这些事件转发到相应的窗口。借用一张图描述Android输入系统的整体流程。

![](media/c96f7d1e102f67fbbab5b2ba8d234a53.png)

上图描述了描述了输入事件的处理流程以及输入系统中最基本的参与者，它们是：

\* Linux内核，接受输入设备的中断，并将原始事件的数据写入到设备节点中。

\*
设备节点，作为内核与IMS的桥梁，它将原始事件的数据暴露给用户空间，以便IMS可以从中读取事件。

\*
InputManagerService，一个Android系统服务，它分为Java层和Native层两部分。Java层负责与WMS的通信。而Native层则是InputReader和InputDispatcher两个输入系统关键组件的运行容器。

\*
EventHub，直接访问所有的设备节点。并且正如其名字所描述的，它通过一个名为getEvents()的函数将所有输入系统相关的待处理的底层事件返回给使用者。这些事件包括原始输入事件、设备节点的增删等。

\*
InputReader，I是IMS中的关键组件之一。它运行于一个独立的线程中，负责管理输入设备的列表与配置，以及进行输入事件的加工处理。它通过其线程循环不断地通过getEvents()函数从EventHub中将事件取出并进行处理。对于设备节点的增删事件，它会更新输入设备列表于配置。对于原始输入事件，InputReader对其进行翻译、组装、封装为包含了更多信息、更具可读性的输入事件，然后交给InputDispatcher进行派发。

\*
InputReaderPolicy，它为InputReader的事件加工处理提供一些策略配置，例如键盘布局信息等。

\*
InputDispatcher，是IMS中的另一个关键组件。它也运行于一个独立的线程中。InputDispatcher中保管了来自WMS的所有窗口的信息，其收到来自InputReader的输入事件后，会在其保管的窗口中寻找合适的窗口，并将事件派发给此窗口。

\*
InputDispatcherPolicy，它为InputDispatcher的派发过程提供策略控制。例如截取某些特定的输入事件用作特殊用途，或者阻止将某些事件派发给目标窗口。一个典型的例子就是HOME键被InputDispatcherPolicy截取到PhoneWindowManager中进行处理，并阻止窗口收到HOME键按下的事件。

\*
WMS，虽说不是输入系统中的一员，但是它却对InputDispatcher的正常工作起到了至关重要的作用。当新建窗口时，WMS为新窗口和IMS创建了事件传递所用的通道。另外，WMS还将所有窗口的信息，包括窗口的可点击区域，焦点窗口等信息，实时地更新到IMS的InputDispatcher中，使得InputDispatcher可以正确地将事件派发到指定的窗口。

\*
ViewRootImpl，对于某些**窗口**，如壁纸窗口、SurfaceView的窗口来说，窗口即是输入事件派发的终点。而对于其他的如Activity、对话框等使用了Android控件系统的**窗口**来说，输入事件的终点是控件（View）。ViewRootImpl将窗口所接收到的输入事件沿着控件树将事件派发给感兴趣的控件。

**上面介绍了输入系统涉及的模块，下面借用一张图表述IMS是如何与App进程通信的：**

![](media/c9ec827ea54b68aa5b943acc13995a6e.png)

输入系统进程间通信会涉及哪些内容呢？

1.system_server进程：负责读取和分发事件。

2.App进程：负责处理输入事件。

上面这两个进程会涉及哪些双向通信呢？

1.system_server进程会向应用进程发送输入事件。

2.App进程会告知system_server事件处理完成，否则会引发ANR。

这里大家可能会有疑惑，binder系统能否实现上面所说的双向通信呢？

答案是不行的。因为binder分为server和client，每次都由client主动发出请求，server收到请求后进行答复，这样的缺点就是每次请求只能单方发起，server不能主动发送数据给client，这样自然不能称为双向通信。

因此在Android输入系统中使用了socketpair+Binder的机制，实现双向通信。

至于android在事件处理上为什么使用socketpair而不是直接使用Binder机制，猜测应该是google为了保证事件响应的实时性，因此在选择进程间传递事件的方式中，选择了高的**socketpair**的方式，由于socketpair在数据管理过程中基本不涉及到内存的数据拷贝，只是在进程读写时涉及到2次数据拷贝，这个是不可避免的数据拷贝，因此这种方式能够很好的保证系统对事件的响应，但是仅仅是socketpair是不够的，因为socketpair的通信方式并不能够通知对方有数据更新，因此android在事件处理过程中加入了另一种进程间通信方式**eventfd**，每次system_serve通知ViewRootImpl只是向其传递一个数字’1’，唤醒App进程处理数据。

预备知识：

socketpair机制：

传统的C/S模型，如下：

![](media/870800466260f1cdd215dfb23f53a0b7.png)

总是一方发起请求，等待另一方回应。当一次传输完成之后，client端发起新的请求之后，server端才作出回应。
那如何才能做到双向通信？ 
一种解决办法就是client端既是client，又是server，server端既是client也是server。

![](media/bd40f36065d674c163c0dd7f61a43a5d.png)

socketpair正是基于这种模式实现双向通信，我们先看一下它是如何创建的。

>   int sockets[2];//文件描述符，一边被client端持有，一端被server端持有

>   socketpair(AF_UNIX, SOCK_SEQPACKET, 0, sockets)

>   //设置socket描述符的选项

>   int bufferSize = SOCKET_BUFFER_SIZE;

>   setsockopt(sockets[0], SOL_SOCKET, SO_SNDBUF, &bufferSize,
>   sizeof(bufferSize));

>   setsockopt(sockets[0], SOL_SOCKET, SO_RCVBUF, &bufferSize,
>   sizeof(bufferSize));

>   setsockopt(sockets[1], SOL_SOCKET, SO_SNDBUF, &bufferSize,
>   sizeof(bufferSize));

>   setsockopt(sockets[1], SOL_SOCKET, SO_RCVBUF, &bufferSize,
>   sizeof(bufferSize));

system_server进程中的IMS持有sockets[0]， App进程持有sockets[1]。

当我们通过按键或者触摸屏幕时会在/dev/input/event
n节点产生输入事件，通过IMS一系列处理后通过sockets[0]输入到kernel
buffer中，App进程便可以通过sockets[1]读取出这一事件，从而消费该输入事件。

当消费事件成功后，App进程必须通过sockets[1]写入finish信息至kernel
buffer，然后IMS通过sockets[0]消费该信息。如果没有发送该finish信息，会引发ANR。

内核中，对应于每个进程都有一个文件描述符表，表示这个进程打开的所有文件。文件描述符就是这个表的索引。

然而socketpair创建的文件描述符只适用于父子进程或者线程间通信，不能用于两个进程之间通信。如果要实现两个进程之间的双向通信，则需要将socketpair创建的一个描述符fd传递给另一个进程，这相当于两个不同的进程访问同一个文件。

![](media/afe4342c0752f1a3bdf08a6d8457ab8d.png)

在这里必须声明，“进程间传递文件描述符”这个说法是错误的。
在处理文件时，内核空间和用户空间使用的主要对象是不同的。对用户程序来说，一个文件由一个文件描述符标识。该描述符是一个整数，在所有有关文件的操作中用作标识文件的参数。文件描述符是在打开文件时由内核分配，只在一个进程内部有效。两个不同进程可以使用同样的文件描述符，但二者并不指向同一个文件。基于同一个描述符来共享文件是不可能的。
 

这里说的“进程间传递文件描述符”是指，A进程打开文件fileA,获得文件描述符为fdA,现在A进程要通过某种方法，根据fdA,使得另一个进程B,获得一个新的文件描述符fdB,这个fdB在进程B中的作用，跟fdA在进程A中的作用一样。即在fdB上的操作,便是对fileA的操作。
这看似不可能的操作，是怎么进行的呢？Binder驱动支持跨进程传递文件描述符。

进程间传递文件描述符并不是传递文件描述符的值,
而是要在接收进程中创建一个新的文件描述符,
并且该文件描述符和发送进程中被传递的文件描述符指向内核中相同的文件表项。

**epoll机制：**

从设备节点中获取原始输入事件面临一个问题，就是这些事件是偶发的。也就是说，大部分情况下设备节点这些文件描述符中都是无数据可读的，同时又希望有事件到来时可以尽快地对事件作出反应。为解决这个问题，我们不希望不断地轮询这些描述符，也不希望为每个描述符创建一个单独的线程进行阻塞时的读取，因为这都将会导致资源的极大浪费。

此时最佳的办法是使用epoll机制。epoll可以使用一次等待监听多个描述符的可读/可写状态。等待返回时携带了可读的描述符或自定义的数据，使用者可以据此读取所需的数据后可以再次进入等待。因此不需要为每个描述符创建独立的线程进行阻塞读取，避免了资源浪费的同时又可以获得较快的响应速度。

epoll事先通过epoll_ctl()来注册一个文件描述符，一旦基于某个文件描述符就绪时，内核会采用类似callback的回调机制，迅速激活这个文件描述符，当进程调用epoll_wait()
时便得到通知。

如何使用epoll机制？

>   1.mEpollFd = epoll_create(EPOLL_SIZE_HINT);

>   创建一个epoll的句柄，告诉内核这个监听的设备描述符数目为EPOLL_SIZE_HINT

>   2.epoll_ctl(mEpollFd, EPOLL_CTL_ADD, fd, &eventItem)

>   epoll的事件注册函数

>   第一个参数是epoll_create()的返回值，

>   第二个参数表示动作，用三个宏来表示：

>   EPOLL_CTL_ADD：注册新的fd到mEpollFd中；

>   EPOLL_CTL_MOD：修改已经注册的fd的监听事件；

>   EPOLL_CTL_DEL：从mEpollFd中删除一个fd；

>   第三个参数是需要监听的fd，（此处为"/dev/input/xxx”设备文件的描述符）

>   第四个参数是告诉内核需要监听什么事件，类型为structepoll_event， 结构如下

>   typedef union epoll_data {

>       void \*ptr;

>       int fd;

>       __uint32_t u32;//存储device id

>       __uint64_t u64;

>   } epoll_data_t;

>   struct epoll_event {

>       __uint32_t events; /\* Epoll events \*/

>       epoll_data_t data; /\* User data variable \*/

>   };

>   events可以是

>   EPOLLIN: 触发该事件，表示对应的文件描述符上有可读数据。

>   EPOLLOUT: 触发该事件，表示对应的文件描述符上可以写数据；

>   …

>   EPOLLIN \| EPOLLOUT: 可读可写均会触发

>   3.epoll_wait(mEpollFd, mPendingEventItems, EPOLL_MAX_EVENTS, timeoutMillis);

>   第一个参数是epoll_create()的返回值，

>   第二个参数存储从内核得到事件的集合， mPendingEventItems类型为"struct
>   epoll_event \*”

>   第三个参数EPOLL_MAX_EVENTS告诉内核mPendingEventItems数组成员个数

>   第四个参数超时时间

**Looper机制（Java&Native）：**

Handler、Looper、MessageQueue、Message大家都比较熟悉，今天主要介绍下Java层和Native层的消息机制如何关联起来。

![](media/cf7f8e8a13ad18f089a79c8e4f47c18c.png)

Android的主线程就是ActivityThread，通过Looper和Handler机制实现其生命周期的函数调用，也正是因为Looper机制才使得我们的主线程不会退出。

>   public static void main(String[] args) {

>       ...

>       Looper.prepareMainLooper();

>       ...

>       Looper.loop();//开启消息循环

>       throw new RuntimeException("Main thread loop unexpectedly exited");

>   }

Looper的轮询过程是交由epoll负责的，当没有消息或者没有事件上报的话，App主线程是处于阻塞状态的，此时它会让出CPU资源。注：阻塞一般是被动的，在抢占资源中得不到资源，被动的挂起在内存，等待某种资源或信号量（即有了资源）将他唤醒。（释放CPU，不释放内存）

![](media/b21f44cb936b037ab92e91d4b6a826a9.png)

MessageQueue通过mPtr变量保存NativeMessageQueue对象，从而使得MessageQueue成为Java层和Native层的枢纽，既能处理上层消息，也能处理native层消息；下面列举Java层与Native层的对应图

![](media/7a5fd07d9e0bc19618942c8920498181.png)

\*
红色虚线关系：Java层和Native层的MessageQueue通过JNI建立关联，彼此之间能相互调用，搞明白这个互调关系，也就搞明白了Java如何调用C++代码，C++代码又是如何调用Java代码。

\*
蓝色虚线关系：Handler/Looper/Message这三大类Java层与Native层并没有任何的真正关联，只是分别在Java层和Native层的handler消息模型中具有相似的功能。都是彼此独立的，各自实现相应的逻辑。

**关于mWakeEventFd**

>   Looper::Looper(bool allowNonCallbacks)  {

>   mWakeEventFd = eventfd(0, EFD_NONBLOCK \| EFD_CLOEXEC);

>   rebuildEpollLocked();

>   }

>   void Looper::rebuildEpollLocked() {

>   mEpollFd = epoll_create(EPOLL_SIZE_HINT);

>   struct epoll_event eventItem;

>   memset(& eventItem, 0, sizeof(epoll_event)); // zero out unused members of
>   data field union

>   eventItem.events = EPOLLIN;

>   eventItem.data.fd = mWakeEventFd;

>   int result = epoll_ctl(mEpollFd, EPOLL_CTL_ADD, mWakeEventFd, & eventItem);

>   }

eventfd也是linux新增的一个API，用于进程/线程之间的通信，通过它能够实现进程/线程间的等待/通知机制。之前版本中是通过pipe，得到read&write
fd，来实现的wake up looper功能。

>   int eventfd(unsigned int initval, int flags);创建一个eventfd文件描述符

Native Looper中使用这一机制用来线程间的等待/通知机制

当需要唤醒Looper时,调用wake(), 会往mWakeEventFd中write 1.

>   void Looper::wake() {

>   uint64_t inc = 1;

>   ssize_t nWrite = TEMP_FAILURE_RETRY(write(mWakeEventFd, &inc,
>   sizeof(uint64_t)));

>       ...

>   }

epoll_wait() 则会返回。然后调用awoken(),读出该值。

>   void Looper::awoken() {

>   uint64_t counter;

>       TEMP_FAILURE_RETRY(read(mWakeEventFd, &counter, sizeof(uint64_t)));

>       ...

>   }

**二、IMS的构成**

当用户触摸屏幕或者按键操作，首次触发的是硬件驱动，驱动收到事件后，将该相应事件写入到输入设备节点，
这便产生了最原生态的内核事件。接着，输入系统取出原生态的事件，经过层层封装后成为KeyEvent或者MotionEvent
；最后，交付给相应的目标窗口(Window)来消费该输入事件。可见，输入系统在整个过程起到承上启下的衔接作用。

    Input模块的主要组成：

    \*
Native层的InputReader负责从EventHub取出事件并处理，再交给InputDispatcher；

    \*
Native层的InputDispatcher接收来自InputReader的输入事件，并记录WMS的窗口信息，用于派发事件到合适的窗口；

\*
Java层的InputManagerService跟WMS交互，WMS记录所有窗口信息，并同步更新到IMS，为InputDispatcher正确派发事件到ViewRootImpl提供保障；

![](media/72779eb868501d2b43a7b45b50281d42.png)

首先给大家做一个简单的介绍：IMS是如何通过epoll机制与App通信的。

![](media/29f441672fe3cd540becc3cb041d527a.png)

通过下图说明App进程如何响应输入事件。

![](media/57a0c4fe01f2e86649b4c8ad73a2e8e2.png)

**三、输入事件的读取加工、派发（InputReader & InputDispatcher）**

![](media/396f45f8e92835b30b7aa4fb271412c8.png)

简单总结下上文涉及到的三个线程。

-   Input系统—InputReader线程：通过EventHub从/dev/input节点获取事件，转换成EventEntry事件加入到InputDispatcher的mInboundQueue。

-   Input系统—InputDispatcher线程：从mInboundQueue队列取出事件，转换成DispatchEntry事件加入到connection的outboundQueue队列。再然后开始处理分发事件，取出outbound队列，放入waitQueue.

-   Input系统—UI线程：创建socket
    pair，分别位于”InputDispatcher”线程和focused窗口所在进程的UI主线程，可相互通信。

-        UI主线程：通过setFdEvents()，
    监听socket客户端，收到消息后回调NativeInputEventReceiver();

-       “InputDispatcher”线程：
    通过IMS.registerInputChannel()，监听socket服务端，收到消息后回调handleReceiveCallback；

**四、输入事件的发送、接收和反馈**

经过上述讨论可知，InputDispatcher已经选择好输入事件的目标窗口，接下来便准备将该事件发送给目标窗口。然而，InputDispatcher运行于system_server进程，而窗口运行与其所在的应用进程。它们之间如何通信呢？

答案就是之前提到的InputChannel，它封装了socketpair，socketpair用来实现在本机内进行进程间的通信。在此先告诉大家，InputDispatcher持有socketpair的一端，窗口所在的应用进程持有socketpair的另外一端。因此InputDispatcher向其持有的InputChannel中写入输入事件，可以有窗口从自己的InputChannel中读取。并且窗口可以将事件处理完毕的反馈写入InputChannel中，InputDispatch而再将其反馈进行读取。

那么InputDispatcher(位于system_server进程)和app进程是什么时候分别和InputChannel建立联系呢？

下面从Activity启动过程开始分析。

![](media/dd3a5440680c68877bd61621df0a0a46.png)

openInputChannel创建的InputChannel对是在WMS中创建的，通过该InputChannel对，IMS得以和App进程进行通信。

然而InputChannel对是在WMS中创建的，它需要将其中一端跨进程交给App进程(Binder机制)，另一端交给IMS。

交给IMS时，IMS通过自己InputDispatcher线程的Native层Looper监听该InputChannel，用于接收App进程处理完输入事件所发送的反馈信息。

交给App进程时，通过Binder将该InputChannel写回至App进程。在ViewRootImpl中创建InputEventReceiver，通过Native层Looper监听该InputChannel，用于接收·IMS发送的输入事件。

此处引入了WindowManagerGlobal,ViewRootImpl,IWindowSession概念，下面解释下这些概念。

什么是ViewRootImpl?

ViewRootImpl实现了ViewParent接口，作为整个控件树的根部，它是控件树正常运作的动力所在，控件的测量、布局、绘制以及输入事件的派发处理都由ViewRootImpl触发。另一方面，它是WindowManagerGlobal工作的实际实现者，因此它还需要负责与WMS交互通信以调整窗口的位置大小，以及对来自WMS的事件（如窗口尺寸改变等）作出相应的处理。

每一个窗口对应着一个ViewRootImpl对象,也就是说一个Activity对应一个ViewRootImpl。

![](media/178f3a876580d176012bf44605fd5d32.png)

接着分析WindowManagerGlobal，在这里首先需要搞清楚WindowManager是什么。

准确的说，WindowManager是一个继承自ViewManager的接口。ViewManager定义了三个函数，分别用于添加/删除一个控件，以及更新控件的布局。

ViewManager接口的另一个实现者是ViewGroup，它是容器类控件的基类，用于将一组控件容纳到自身的区域中，这一组控件被称为子控件。ViewGroup可以根据子控件的布局参数（LayoutParams）在其自身的区域中对子控件进行布局。

大家可以将WindowManager与ViewGroup进行一下类比：设想WindowManager是一个ViewGroup，其区域为整个屏幕，而其中的各个窗口就是一个一个的View。WindowManager通过WMS的帮助将这些View按照其布局参数（LayoutParams）将其显示到屏幕的特定位置。二者的核心工作是一样的，因此WindowManager与ViewGroup都继承自ViewManager。

![](media/956722b4857f081eae443dc9e8af9361.png)

WindowManagerGlobal是WindowManager的最终实现者。它维护了当前进程中所有已经添加到系统中的窗口的信息。另外，在一个进程中仅有一个WindowManagerGlobal的实例。

WindowManagerGlobal是如何完成窗口管理的？

![](media/03fef0c1b11a4fbd7565332a720b8572.png)

从上图可以看出来，view的添加、更新、删除操作都是托管给ViewRootImpl进行的。

最后分析下IWindowSession.

ViewRootImpl的构造函数中初始化了mWindowSession和mWindow

>   public ViewRootImpl(Context context, Display display) {

>       ...   

>   mWindowSession =
>   WindowManagerGlobal.getWindowSession();//从WindowManagerGlobal中获取一个IWindowSession的实例。它是ViewRootImpl和WMS进行通信的代理。

>   mWindow = new
>   W(this);//创建一个W类型的实例，W是IWindow.Stub的子类。即它将在WMS中作为新窗口的ID，并接收来自WMS的回调。

>       ...

>   }

要使App进程可以请求WMS服务，必须在WMS服务这边创建一个类型为Session的Binder本地对象，同时App进程这边获取Session的代理对象，通过该代理对象，应用程序进程就可以请求WMS服务了。

mWindowSession便是WMS创建的Session对象在App进程中的一个代理。创建过程如下：

![](media/906b54a212ad96021614789f90ae9624.png)

主要看下WMS的addWindow干了哪些事儿？

>   WMS.addWindow

>   public intaddWindow(Session session, IWindow client, int seq,
>   WindowManager.LayoutParams attrs, int viewVisibility, int displayId, Rect
>   outContentInsets, Rect outStableInsets, Rect outOutsets, InputChannel
>   outInputChannel) { 

>       ...

>   final WindowState win = new WindowState(this, session, client, token,
>   parentWindow,

>                      appOp[0], seq, attrs, viewVisibility,
>   session.mUid, session.mCanAddInternalSystemWindow);//构造函数中生成mInputWindowHandle
>   = new InputWindowHandle(...)

>       ...

>   final boolean openInputChannels = (outInputChannel != null &&
>   (attrs.inputFeatures & INPUT_FEATURE_NO_INPUT_CHANNEL) == 0); 

>   if (openInputChannels) { 

>   win.openInputChannel(outInputChannel);
>   //在这里IMS和App进程分别和inputchannel建立联系，至此，输入系统底层数据交互通道便打通了

>   } 

>       ...

>   mInputMonitor.updateInputWindowsLw(false
>   /\*force\*/);将WMS中的窗口信息拷贝至IMS中

>       ...

>   }

这里介绍下IMS中如何持有WMS中的窗口信息，这关系到如何确定InputDispatcher派发事件到哪一个目标窗口。

![](media/8204e016698f0f80cbe8b5760c89606b.png)

#### 上述过程的整体流程如下：

![](media/6292bd2ccdd46e1715e22792ceda6c52.png)

#### 六、View事件分发机制

#### 当UI主线程收到底层上报的input事件,便会调用InputEventReceiver.dispachInputEvent方法,事件最终交给DecorView进行处理，即mView.dispatchPointerEvent/mView.dispatchKeyEvent。

![](media/207f22777a2513cee96626724cebacbb.png)

事件到达Activity后，便进入了Android事件分发体系。首先介绍下事件分发体系的参与者：Activity，PhoneWindow,
DecorView。

![](media/ffb72ea6ef2e2793d6dd7b795d65a8b9.png)

每一个Activity内部都包含一个Window用来管理要显示的视图。而Window是一个抽象类，其具体实现是
PhoneWindow类。DecovrView作为PhoneWindow的一个内部类，实际管理着具体视图的显示。他是FrameLayout的子类，盛放着我们的标题栏和根视图。我们自己写的一些列View和ViewGroup都是由他来管理的。

**6.1 触摸事件的分发流程**

![](media/2c9d9da974d98e7f9ebb8e0fac2af7b4.png)

注：onTouchListener.onTouch执行在onTouchEvent之前，onClickListener.onClick执行在onTouchEvent内。

（1）同一个事件序列是指从手指触摸屏幕的那一刻起，到手指离开屏幕的那一刻结束。在这个过程中所产生的一系列事件，这个事件序列以
down 事件开始，中间含有数量不定的 move 事件，最终以 up 结束。

（2）正常情况下，一个事件序列只能被一个View拦截并且消耗、这一条原因可以参考（3），因为一旦一个元素拦截了某次事件，那么同一个事件序列内的所有事件都会直接交给它处理。因此同一个事件序列中的时间不能分别由两个View同时处理，但是通过特殊手段可以做到，比如一个View将该本身处理的事件通过
OnTouchEvent 强行传递给其他 View 处理。

（3）某个 View
一旦决定拦截，那么这一个事件序列都只能由它来处理（如果事件序列能够传递给它的话），并且它的OnInterceptTouchEvent
不会再被调用。这条很好理解，就是说当一个View决定拦截一个事件后，那么系统会把同一个事件序列内的其他方法都直接交给它处理，因此就不用再调用这个
View 的 OnInterceptTouchEvent 去询问它是否要拦截了。

（4）某个 View 一旦开始处理事件，如果它不消耗 ACTION_DOWN 事件（OnTouchEvent
返回
false），那么同一个事件序列中的其他事件都不会再交给它来处理，并且事件将重新交由它的父元素来处理，即父元素的
OnTouchEvent 会被调用。意思就是事件一旦交给一个View处理，那么他就必须要消耗掉
ACTION_DOWN
事件，否则同一个事件序列中剩下的事件就不会再交给它来处理了，这就好比上级交给程序员一件

（5）如果 View 不消耗 ACTION_DOWN
以外（ACTION_DOWN事件已经被消耗）的其他事件，那么这个点击事件会消失，此时父元素的
OnTouchEvent 并不会被调用，并且当前 View
可以继续收到后续的事件，最终这些消失的点击事件会传递给Activity处理。

（6）ViewGroup默认不拦截任何事件。Android源码中的 ViewGroup 的
OnInterceptTouchEvent 方法默认返回false。

（7） View 没有 OnInterceptTouchEvent 方法，一旦有点击事件传递给它，那么它的
OntouchEvent 方法就会被调用。

**6.2 按键事件的分发机制**

  
控件的焦点影响了按键事件的派发，此处的焦点便是我们所熟知的View.requestFocus()。控件树的焦点管理分为两个部分：其一是描述个体级别的焦点体系的PFLAG_FOCUSED标记，用于表示一个控件是否拥有焦点；其二是描述控件树级别的焦点状态的ViewGroup.mFocused成员，用于提供一条链接控件树的根控件到实际拥有焦点的子控件的单向链表。这条链表提供了在控件树中快速查找焦点控件的简便方法。如下图所示

![](media/a4caf8d5940a99a9d2b380e886cd7b2a.png)

接下来分析按键事件的分发流程

![](media/ea7c413e4f9b7d1e43dda5ee238eacf0.png)
