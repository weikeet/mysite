*Handle onto a raw buffer that is being managed by the screen compositor.*

处理由屏幕合成器管理的原始缓冲区。

这句话包括下面两个意思：

1.  通过Surface就可以获得原生缓冲器以及其中的内容。就像在C++语言中，可以通过一个文件的句柄，就可以获得文件的内容一样。

2.  原始缓冲区（a raw buffer）是用于保存当前窗口的像素数据的。

![](media/8cc2818e45010be20ceb1bd6f41cecd1.png)

帮我们创建Surface的是WindowManagerService，它在帮我们创建Surface时，会生成一个Session，然后这个Session会创建一个SurfaceControl来构造Surface。

![](media/d3c159c520328b16eee0a5570db58eef.png)

我们来看看Surface的接口都有什么，除了重要的构造函数外，比较重要的两个接口，lockCanvas,
unlockCanvasAndPost。lockCanvas会返回一个Canvas给我们，我们可以使用这个Canvas来改变Surface的内容，
但是这个改变不会立刻生效，只有在我们调用了unlockCanvasAndPost之后，改变才会生效。

一个surface通常有两个缓冲区以实现双缓冲绘制，当应用正在一个缓冲区中绘制自己下一个UI状态时，SurfaceFlinger可以将另一个缓冲区中的数据合成显示到屏幕上，而不用等待应用绘制完成。

其中java层Surface和NativeSurface对照如下，java层的Surface其实是对native层的一层封装，核心逻辑都在native层的Surface上。

Native Surface

![](media/80dc652e8cb65dfdc1a8b3697824a0c4.png)

Surface是一个ANativeWindow，它的继承关系如下。ANativeWindow是一个EGL可以操作的窗口，其具体定义在system/core/include/system/window.h里，它的主要接口有dequeueBuffer，queueBuffer，lockBuffer等。

![](media/c80a8dd30a4cdb6b620310eee18bf66d.png)

然后着重分析一下native层Surface的lock和unlockAndPost方法。

Surface::lock方法：

![](media/d1ab87eae2b82e279200d0ac65334089.png)

IGraphicBufferProducer服务端实现者是BufferQueueProducer，BufferQueueProducer由BufferQueue实例化。

应用程序不断刷新UI将产生的数据源源不断的写到Buffer中。

Surface::unlockAndPost方法：

![](media/e72559b4a88fc63fbc21c9271bd46a05.png)

应用程序刷新UI填充好Buffer后，通过BufferProducer添加到BufferQueue中，然后BufferQueueConsumer消费Buffer。其中存在ConsumerListener和ProducerListener用来监听状态用来触发生产和消费。

综上，lock和unlockAndPost其实就是一个标准的生产者消费者模型，示意如下

![](media/55ecaf94aa11710c9c8a35f74b2f8b2c.png)

Producer其实对应着App源源不断的UI，准确的说是Surface上对应的帧，Consumer中的对应的处理者就是SurfaceFlinger。

再详细到SurfaceFlinger整个完整的图如下：

![](media/02e4274d8ff6ca1e51cee328e8a4b7ec.png)

1.  生产者dequeue过来一块Buffer，此时该buffer的状态为DEQUEUED，所有者为PRODUCER，生产者可以填充数据了。

2.  生产者填充完数据后,进行queue操作，此时buffer的状态由DEQUEUED-\>QUEUED的转变，buffer所有者也变成了BufferQueue了。这个时候producer代理对象进行queue操作后，producer本地对象会回调BufferQueue的onFrameAvailable函数，通知消费者有可用的buffer已经就绪了，你可以拿去用了。

3.  上面已经通知消费者去拿buffer了，这个时候消费者就进行acquire操作将buffer拿过来，此时buffer的状态由QUEUED-\>ACQUIRED转变，buffer的拥有者由BufferQueue变成Consumer。

4.  当消费者已经消费了这块buffer(已经合成，已经编›r拥有者由Consumer变成BufferQueue。

对应状态的源码如下

>   BufferSlot.h

>   // A buffer can be in one of five states, represented as below:

>   //

>   //         \| mShared \| mDequeueCount \| mQueueCount \| mAcquireCount \|

>   // --------\|---------\|---------------\|-------------\|---------------\|

>   // FREE    \|  false  \|       0       \|      0      \|       0       \|

>   // DEQUEUED\|  false  \|       1       \|      0      \|       0       \|

>   // QUEUED  \|  false  \|       0       \|      1      \|       0       \|

>   // ACQUIRED\|  false  \|       0       \|      0      \|       1       \|

>   // SHARED  \|  true   \|      any      \|     any     \|      any      \|

从上面知道Surface对应App是一个画布，并且Surface中lockCanvas和unlockCanvas产生一个GraphicBuffer，这个buffer最终由SurfaceFlinger处理。

那么Surface的来源又是什么呢？

SurfaceFlinger中的Layer

客户端在请求创建surface时，会在SurfaceFlinger服务中，创建一个Layer对象，而在第一次引用该layer对象时，就会调用onFirstRef函数。其中会创建生产者，消费者，BufferQueue对象。

>   void Layer::onFirstRef() {

>       // Creates a custom BufferQueue for SurfaceFlingerConsumer to use

>       sp\<IGraphicBufferProducer\> producer;

>       sp\<IGraphicBufferConsumer\> consumer;

>       BufferQueue::createBufferQueue(&producer, &consumer, true);

>       mProducer = new MonitoredProducer(producer, mFlinger, this);

>       mSurfaceFlingerConsumer = new SurfaceFlingerConsumer(consumer,
>   mTextureName, this);

>       mSurfaceFlingerConsumer-\>setConsumerUsageBits(getEffectiveUsage(0));

>       mSurfaceFlingerConsumer-\>setContentsChangedListener(this);

>       mSurfaceFlingerConsumer-\>setName(mName);

>       if (mFlinger-\>isLayerTripleBufferingDisabled()) {

>           mProducer-\>setMaxDequeuedBufferCount(2);

>       }

>       const sp\<const DisplayDevice\>
>   hw(mFlinger-\>getDefaultDisplayDevice());

>       updateTransformHint(hw);

>   }

![](media/b1452f9e2a5be9fb2005ac69d269b3be.png)

<https://blog.csdn.net/armwind/article/details/73436532>

<http://www.10tiao.com/html/431/201601/401709603/1.html>

Layer.onFrameAvailable
