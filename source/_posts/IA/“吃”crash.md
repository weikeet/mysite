---
title: "吃"Crash
date: 2018-01-12 14:16:27
updated: 2019-11-25 16:40:00
---

# "吃"Crash

crash，意为“崩溃”。



crash发生时一般有下面两种表现：

1.  如果该crash发生在主进程，用户如果正在界面上，将会看到界面闪退、或者弹出“应用程序已崩溃”对话框的现象，根据不同机型，对话框显示的条件会有所区别，但是界面基本上是会闪退的。

2.  如果该crash发生在work和clean进程，可能会发生扫描结果不完整这类问题，严重的情况下可能会导致界面阻塞甚至ANR。



crash在安卓上又可以分为Java层和Native层crash，Native层的crash比较复杂，现阶段还没有研究到，需要吃的是Java层的crash。



“吃”crash其实是通过Java的异常处理方式不让crash抛出来（抛出来的话进程会退出）。



【我们要吃的是】

> 频繁发生的，但是不是由我们程序的代码引起的，并且无法通过Java的try+catch保护的异常。



这种异常很难通过修改代码解决。

所以，现在最想吃的一个crash是：

>   在Android
>   4.4版本，TCL以及一些机型，由于系统的漏洞，App升级后Notification资源文件的id变化导致发送Notification时导致的crash。

这个问题困扰了我们很久，曾经尝试过通过keep Id的方式，让Notification资源的id不变来解决这个问题，但是维护成本是在太高，于是我们放弃了对这个问题的维护，但是crash又多了起来。



根据fabric上看到的——10几个用户发生1000多次的现象来看，该问题不是对用户可见的，但是，如果是notificationToggle发生crash，这将会导致我们工作进程起来之后，创建notificationToggle时crash，然后工作进程死掉，接着再起来，死掉，一直循环下去，用户可能会有扫描不到垃圾、进App一直卡、手机变烫等问题；



【如果吃掉这个crash】

推测：notificationToggle不会显示，最坏情况，手机上notification里会有一块空白区域，但是不至于让进程起来就死掉。



【吃crash的方式】

采用了launcher组的方法，因为要吃的crash发生在主线程，于是就利用主线程的looper的消息机制，将发生crash的那条消息通过try+catch的方法捕获到，如果判断捕获到的异常是RemoteServiceException并且包含Bad notification posted from package.*字段，那么不讲讲异常抛出来（抛出来的话就是普通的crash），而是通过其他形式记录下来，并且跳过当前消息。（下面是launcher组的与这个crash有关的配置）

```
<dict>
  <key>MessagePattern</key>

  <string>Bad notification posted from package.*</string>

  <key>Type</key>

  <string>RemoteServiceException</string>
</dict>
```

 “跳过当前消息”可能会有逻辑上的问题，所以哪些crash能吃，哪些不能吃需要试验。


根据launcher组的经验，我们决定将我们组最多的crash（上面这一条）给吃掉，这样crash率在MaxCleaner上预计可以减少30%～40%。
