-   String

    -   getString()

        -   Returns a localized string from the application’s package’s default
            string table

    -   getText()

        -   Returns a localized, styled CharSequence from the application’s
            package’s default string table

    -   e.g.

        -   \<*string name="sample_text"\>Plain, \<b\>bold\</b\>,
            \<i\>italic\</i\>, \<b\>\<u\>bold-underline\</u\>\</b\>\</string\>*

            -   *getText(R.string.sample_text);      *

            -   *getResource().getText(R.string.sample_text);       *

            -   *getString(R.string.sample_text);*

            ![](media/c314f2ed821d59d92722cf730207bcc7.jpg)

-   Quantity String (Plurals)

    -   getQuantityString(int id, int quantity)

        -   Returns the string necessary for grammatically correct pluralization
            of the given resource ID for the given quantity.

        -   quantity=["zero" \| "one" \| "two" \| "few" \| "many" \| "other"]

        ![](media/db4e96a47e3894a21e8ab7fbce02ad3f.png)

        -   String songsFound
            = res.[getQuantityString](http://developer.android.com/reference/android/content/res/Resources.html#getQuantityString(int,%20int))(R.plurals.numberOfSongsAvailable,
            quantity,count);

            -   quantity - quantity for selecting item res

            -   count - quantity for format argument

Formatting and Styling

-   Escaping 

    -   %a + %a == 2%a

        -   \<stringformatted="false"\>%a + %a == 2%a\</string\>

        -   \<string\>%%a + %%a == 2%%a\</string\>

![](media/b4fd8b057af5a94f88f30b44f9eef14f.png)

-   Formatting

    -   \<stringname="welcome_messages"\>Hello, %1\$s! You have %2\$d new
        messages.\</string\>

-    Styling with HTML markup

    ![](media/4a1785116cc84c22332de8dd4ccca7dc.png)

-   Formatting + Styling 

    ![](media/3ec30d9157259589052b18dff9bb8b12.png)

    ![](media/7858995dbc32c256d4f7acff82b374c7.png)

    ![](media/ee991b99067d9b0d9561737630edd31a.png)

-   Spannables

    -   Text object, style with typeface properties, eg. color, font weight

String Resource Coding Rules

1.  使用getQuantityString()和plurals来处理单复数资源问题

2.  xml资源中，如果需要传入参数，则在显示%的地方，需要用
    %%代替，如果不需要传入参数，需要传入%的地方，只需要一个%，并且加入formatted=“false”

3.  单引号与双引号需要“\\”进行转义

4.  不需要格式化的字符串，需要添加formatted=“false”, 不要使用头尾添加双引号方式

5.  字符串中需要传入多个参数，需要%1\$d, %2\$s此类方式来定义区分

6.  需要翻译的字符串内容，需要定义在string.xml中

7.  不需要翻译的字符串内容，不要定义在string.xml中

8.  不要用多个getString()拼接字符串

结论：

-   在字符串中使用formatted=“false”属性，代码中如果在getString(id,
    arg)使用了参数传递，则formatted会自动变为”true”，也就是说，

    -   \<**string** name="str_define" formatted="false"\>percent 30%%,
        %1\$d\</**string**\>

    -   getString(R.string.*str_define*, 5)获取字符串时，会正常显示为"percent
        30%, 5”

-   formatted=“false”,
    只在编译时起作用，真正决定是否需要format的是在通过getString()获取string资源时，是否传入参数

-   “\\”，反斜杠（back slash）不受formatted影响

-   xml资源中，如果出现%或者’, 未进行转义的话，会编译错误

-   如果xml中存在%1\$d此类需要传入参数，代码中如果getString()时并没有传入相应的参数，会编译错误

-   string资源中使用html标签，颜色只能用6位色值，不支持8位色值,属性value用单引号双引号均可,
    示例如下:

    -   \<**string** name="check_download_apks_safe"\>\<font color =
        \\"\#177777\\"\>%1\$d files are \</font\>\<font
        color=\\'\#4285f4\\'\>safe\</font\>\</**string**\>

Multi-Languages

-   关于阿拉伯语的各种问题汇总

-   <http://blog.csdn.net/lipengshiwo/article/details/52319298>   Arabic issues

**Android系统已经定义好的部分常用字符串**

-   android.R.string.*cancel - Cancel*

-   android.R.string.*no - Cancel*

-   android.R.string.*ok - OK*

-   android.R.string.*yes - OK*

-   android.R.string.*copy - Copy*

-   android.R.string.*unknownName - Unknown*

-   android.R.string.*selectAll - Select all*

-   android.R.string.*search_go - Search*

 以上String资源可以直接从系统SDK中调用，无需再次定义或翻译
