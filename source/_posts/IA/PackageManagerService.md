PackageManager是我们经常接触的一个类。它提供了获取安装应用程序列表、某个应用程序的appName和应用图标、根据指定的intent查询相应的activity等功能。然而PackageManager只是个抽象类，这些功能的真正实现都是在PackageManagerService做的。

**PackageManagerService所处位置**

借用文杰小哥哥分享AMS时的一页PPT，说明下PMS在android系统中的位置

![](media/6b2811841313c04c3e7c22b075887ecf.png)

**PackageManagerService的作用**

-   进行权限检查。当应用程序调用一个需要特定权限的接口时，系统判断调用者是否具备该权限，从而保证系统的安全。

-   提供一个根据intent匹配到具体的Activity、Provider、Service的服务。即当应用程序调用startActivity(intent)时，能够把参数中指定的intent转换成一个具体的包含了程序包名称及具体Componet名称的信息，以便Java类加载具体的Componet。

-   提供安装删除应用程序的接口

**PackageManagerService类家族**

![](media/6753f43e8f2ca87880da3664570209a8.png)

-   IPackageManager是个aidl，定义了服务端和客户端通信的业务函数，aidl编译生成IPackageManager.java，同时会生成内部类Stub和Proxy

-   PackageManagerService继承自Stub，因为Stub从Binder派生而来，因此作为服务端与Binder通信

-   PackageManager是个抽象类，它定义了客户端能调用的方法。IPackgeManager虽然定义了许多服务端和客户端通信的业务函数，但出于安全的考虑，PackageManager定义的方法实际上只是IPackgeManager的一个子集

-   ApplicationPackageManager继承自PackageManager，其内部有个成员变量mPM指向了Proxy。当客户端通过Context.getPackageManager()获取PackageManager对象时，实际上返回的是ApplicationPackageManager对象

**PackageManagerService实现权限检查功能原理**

应用程序在调用一个需要特定权限的接口时，得满足两个条件，第一个是该手机系统具有此功能，第二个是此应用程序申请到了此功能。这两类信息都储存在手机特定的文件目录下。倘若每次调用需要特定权限的接口时都需要去访问这两类文件，程序的响应速度肯定会比较慢，导致用户体验不好。为此，PMS在初始化时会去解析这两类文件信息，存在内存里，避免了应用程序在访问相关接口时还需去解析文件的情况。但是，把文件解析放在PMS初始化，增加了PMS构造所花的时间，这也是android手机启动慢的一个原因。

1.
Android储存手机支持的功能的文件目录是system/etc/permissions。该目录下两个最重要的文件是platform.xml和handheld-core-hardware.xml

platform.xml文件中主要使用了如下4个标签：

·  permission和group用于建立Linux层gid和Android层pemission之间的映射关系。

· 
assign-permission用于向指定的uid赋予相应的权限。这个权限由Android定义，用字符串表示。

·  library用于指定系统库。当应用程序运行时，系统会自动为这些进程加载这些库

[--\>platform.xml样例]

\<permissions\>

  
\<!--建立权限名与gid的映射关系。如下面声明的BLUTOOTH_ADMIN权限，它对应的用户组是

    net_bt_admin

  --\>

 \<permission name="android.permission.BLUETOOTH_ADMIN" \>

       \<group gid="net_bt_admin" /\>

 \</permission\>

 \<permission name="android.permission.BLUETOOTH" \>

       \<group gid="net_bt" /\>

  \</permission\>

  ......

   \<!--

     赋予对应uid相应的权限。如果下面一行表示uid为shell，那么就赋予

       它SEND_SMS的权限，其实就是把它加到对应的用户组中--\>

   \<assign-permission name="android.permission.SEND_SMS"uid="shell" /\>

   \<assign-permission name="android.permission.CALL_PHONE"uid="shell" /\>

   \<assign-permission name="android.permission.READ_CONTACTS"uid="shell" /\>

   \<assign-permission name="android.permission.WRITE_CONTACTS"uid="shell" /\>

   \<assign-permission name="android.permission.READ_CALENDAR" uid="shell" /\>

......

    \<!--
系统提供的Java库，应用程序运行时候必须要链接这些库，该工作由系统自动完成 --\>

    \<library name="android.test.runner"

           file="/system/frameworks/android.test.runner.jar" /\>

    \<library name="javax.obex"

           file="/system/frameworks/javax.obex.jar"/\>

\</permissions\>

handheld-core-hardware.xml包含了许多feature标签。这些feature用来描述一个手持终端（包括手机、平板电脑等）支持的硬件特性，例如支持camera、支持蓝牙等。

[--\>handheld-core-hardware.xml样例]

\<permissions\>

   \<feature name="android.hardware.camera" /\>

   \<feature name="android.hardware.location" /\>

   \<feature name="android.hardware.location.network" /\>

   \<feature name="android.hardware.sensor.compass" /\>

   \<feature name="android.hardware.sensor.accelerometer" /\>

   \<feature name="android.hardware.bluetooth" /\>

   \<feature name="android.hardware.touchscreen" /\>

   \<feature name="android.hardware.microphone" /\>

   \<feature name="android.hardware.screen.portrait" /\>

   \<feature name="android.hardware.screen.landscape" /\>

   \</permissions\>

2. 应用程序申请的权限等信息储存在data/permission/packages.xml和packages.list。

packages.xml: PMS扫描完目标文件夹后会创建该文件。当系统进行程序安装、卸载和更新等操作时，均会更新该文件。该文件保存了系统中与package相关的一些信息，比如安装时间，上一次更新时间等

packages.list：描述系统中存在的所有非系统自带的APK的信息。当这些程序有变动时，PMS就会更新该文件

**PackageManagerService实现intent查询原理**

![](media/f6499b84a03cd4937d53a21e63e0c658.png)

PMS在初始化时会去扫描system/frameworks（系统库）、system/app（系统应用）、vendor/app（厂商应用）、data/app和data/app-private目录下应用的manifest文件。扫描完后在PMS对应的数据结构中储存，当AMS启动activity,
Service等四大组件时，PMS会通过传入intent的信息返回具体的四大组件，然后交由AMS启动

扫描流程图

PMS扫描完指定目录下的app后，储存的信息有

![](media/d031931f82bca6f1f38a1bfa1fe4d8ce.png)

ActivityIntentResolver继承关系

![](media/a802ea6dee5318669084bc29d2bdd63e.png)

PMS解析app的manifest文件涉及到的相关类

![](media/4cb0bc50c3c55de4d71021d6b0896fc9.png)

以startActivity(Intent)为例，说明一下PMS查找出对应的activity的步骤

1. 假设intent带了ComponentName，则去mActivities
(ActivityIntentResolver实例)中查找制定ComponentName的Activity，查找到即返回，没查找到则会返回null。注意，显示启动的intent肯定会有ComponentName信息

2.
当隐式启动activity时，如果intent带了packageName信息，则去mPackages(Package实例)中查到mActivities能够启动的Activity,
查找到即返回，没查找到则会返回null

3. 如果intent没带packageName信息，则去mActivities
(ActivityIntentResolver实例)中去mFilters查找（mFilters在ActivityIntentResolver的父类IntentResolver中声明，ActivityIntentResolver在添加Activity时会把Activity的IntentFilter信息添加到mFilters中）。

**隐式intent匹配**

当Android系统接收到一个隐式Intent要启动一个Activity(或其他组件)时，Android会根据以下三个信息比较Intent的信息与注册的组件的intent-filter的信息，从而为该Intent选择出最匹配的Activity(或其他组件)：

-   intent中的action

-   intent中的category

-   intent中的data（包含Uri以及data的MIME类型） 

也就是隐式intent对象要满足要启动的目标组件中注册的intent-filter中的\<action
/\>、\<category /\>、\<data
/\>三个标签中的信息，即要分别通过action测试、category测试以及data测试。intent-filter信息是在Android的manife文件中描述的，顾名思义，intent-filter是intent过滤器，就是用来过滤intent的。

如果隐式intent对象同时通过了某个组件的中intent-filter的action测试、category测试以及data测试，那么该组件就可以被intent对象所启动。如果隐式intent对象没有通过系统中任何组件的intent-filter测试，那么就没有Android系统无法找到该intent对象要启动的组件

具体的匹配规则参见文档 <https://blog.csdn.net/iispring/article/details/48481793>
