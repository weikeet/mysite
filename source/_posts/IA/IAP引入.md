-   引入libMax内的iap/IAPManager（gradle &
    libmaxconfig），可以不用引入资源以及PurchaseDialog，资源文件有引用其他模块的资源文件，可能会有依赖问题

-   Manifest中加入权限：

                    \<**uses-permission
android:name="com.android.vending.BILLING"** /\>

-   在gradle中加入

       implementation
**'com.ihandy.iap:iap.gm:**[4.0.0.gm](http://4.0.0.gm)**’**

-   替换config-r.ya以及config-d.ya文件

   *注意文件内缩进不能错，以及不能有中文引号*

           Debug
Tip：文件中最重要的就是密钥和productId，如果想看一下拿到的productId是否正确，可以用HSLog打印，如果是空则是config文件错误

-   使用时需要在Application开始的时候先调用IAPManager.init()初始化

         
 可以在每次session开始的时候查询用户的购买状态，使用queryOwnProducts()方法即可，该方法会自动查询并将结果写入SharedPreference

-   如果需要查询的时候，直接调用hasPurchasedRemoveAdsProduct从SP中读取即可

-   同进程可以注册listener监听购买结果变化，不同进程可以注册observer监听

-   onVerifySucceeded是购买后我们也收到了账单，onPurchaseSucceeded是他购买成功我们不一定收到了账单

测试注意事项：

-   必须打release包才能测试，debug包完全不能测试，如果没有上架，用release包测试会弹出一个对话框，如果没有弹出对话框可能就是config文件错误

-   打包后需要将其放到Google的alpha/beta版进行release，然后从Google商店/后台下载后安装才可以（*注意每次测试versionCode要+1，另外登录的Google账户必须加入了alpha后台注册为测试开发人员，并且需要激活，具体询问运营*）

-   如果想测试购买，Google账户必须绑定了visa卡之类的

-   不稳定性抽风无法购买，可以卸载后重装试一下

-   P.S. 常见错误提示：

  =\>Receiving error while doing in-app purchase in my app in android. Error
while retrieving information from server [DF-AA-20]
可能是账户没有被激活/Config错误/没有用release包 

       =\>The item you purchase is not available. 可能是Config文件错误

          =\>Version not correct. 版本号要加一
