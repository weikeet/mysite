**【IntelliJ IDEA】**

IntelliJ IDEA 在 2015 年 06 月官网主页是这样介绍自己的：

>   Excel at enterprise, mobile and web development with Java, Scala and Groovy,
>   with all the latest modern technologies and frameworks available out of the
>   box.

简明翻译：IntelliJ IDEA 主要用于支持 Java、Scala、Groovy
等语言的开发工具，同时具备支持目前主流的技术和框架，擅长于企业应用、移动应用和
Web 应用的开发

（个人理解：类似于Eclipse的Java开发环境）

**【IntelliJ IDEA 主要功能介绍】**

语言支持上：
------------

| **安装插件后支持** | **SQL类**  | **基本JVM** |
|--------------------|------------|-------------|
| PHP                | PostgreSQL | Java        |
| Python             | MySQL      | Groovy      |
| Ruby               | Oracle     |             |
| Scala              | SQL Server |             |
| Kotlin             |            |             |
| Clojure            |            |             |

其他支持：

| **支持的框架** | **额外支持的语言代码提示** | **支持的容器** |
|----------------|----------------------------|----------------|
| Spring MVC     | HTML5                      | Tomcat         |
| GWT            | CSS3                       | TomEE          |
| Vaadin         | SASS                       | WebLogic       |
| Play           | LESS                       | JBoss          |
| Grails         | JavaScript                 | Jetty          |
| Web Services   | CoffeeScript               | WebSphere      |
| JSF            | Node.js                    |                |
| Struts         | ActionScript               |                |
| Hibernate      |                            |                |
| Flex           |                            |                |

**【IntelliJ IDEA & Android Studio】**

Intellij
IDEA有一个非常强大的用XML描述插件系统，他可以在IDE的各个环节加上扩展，比如欢迎界面，语言和构建系统的支持，菜单,
etc

这些代码以外部资源（jar）的方式加载进IDE，并不需要你去修改Intellij本身的代码。

Android Studio最核心的部分就是一个Intellij插件（Android Support Plugin）

Intellij IDEA分为免费社区版与收费旗舰版，Android Studio是基于IDEA Community
Edition开发的，因为Community 版本不仅是免费的而且是开源的

![](media/33a5d72121896375b38f0f6fe36eda23.png)

可以看出之前刚推出的studio1.0正式版是基于idea13开发的，而不是最新的idea14。

所以基于IntelliJ IDEA Community Edition开发的插件一般也适用于Android Studio。 

**【IntelliJ IDEA Plugin】**

构建在IntelliJ平台上的产品（例如Android Studio）是可组合的应用程序。 

插件可以通过多种方式扩展平台，从添加简单的菜单项到添加对完整语言的支持，构建系统和调试器。 

IntelliJ平台中的许多现有功能都是作为插件来编写的，IntelliJ IDEA
本身就是它自己的插件平台最大的开发者，可以根据最终产品的需要来包含或排除这些插件。

**【如何写IntelliJ IDEA Plugin】**

环境准备、简单例子、Debug、发布等请参考：

<https://www.jetbrains.org/intellij/sdk/docs/basics/getting_started.html>

**【Reference】**

<https://www.jetbrains.org/intellij/sdk/docs/welcome.html>

<https://github.com/judasn/IntelliJ-IDEA-Tutorial>
