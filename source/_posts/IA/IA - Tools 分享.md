    在计算机科学中，工具 (tools) 是指协助我们工作的程序：

| 工具                  | 载体              | 功能                                                                                                                                                                                                                                                       | 作者                        |
|-----------------------|-------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------|
| 自动编译工具          | GitLab runner     | 提供自动编译等一系列服务                                                                                                                                                                                                                                   | 柬文                        |
| SmartCanary工具       | Web服务+App       | Debug环境的自动化性能问题检测工具                                                                                                                                                                                                                          | 文杰                        |
| Flurry查询工具        | Web服务           | 提供轻便的flurry查询服务 <http://192.168.1.14/#/flurry> 开发中：PM用EventName编写表达式，批量计算结果                                                                                                                                                      | 博哥-\>文杰                 |
| Gradle工具            | gradle脚本        | 利用gradle脚本，在编译时： 1.生成git节点、编译时间等信息； 2.加密RemoteConfig文件；（项目asset中的RemoteConfig文件是明文） 3.安装git hook；（提交时必须注明代码“已复查”，如果没有，将不允许提交；提交时自动尝试拉取代码） 4.开发中：检查广告是否忘记Active | 1.振亮 2.振亮 3.晓剑 4.王盟 |
| 一键导入翻译工具      | 脚本              | 一键将翻译的字符串导入到对应的strings.xml下                                                                                                                                                                                                                | 柬文                        |
| 多语言字符串工具      | AS plugin         | 对多语言进行移动、删除操作                                                                                                                                                                                                                                 | 涛哥                        |
| IAPusher              | 服务器定时任务    | 通过trello维护随手、闭环和扫雷，定时发送到dev群（组内级别的“做事准则”）                                                                                                                                                                                    | 文杰                        |
| RemoteConfig工具      | Web服务           | 根据app的版本将config文件加密成正确的加密格式                                                                                                                                                                                                              | 博哥-\>文杰                 |
| AdConfig工具          | 应用程序          | （GoldenEye之前）广告config的批量导入和导出操作                                                                                                                                                                                                            | 博哥-\>小智                 |
| Issue推送服务         | Web服务           | 自动将GitLab的issue的变动发送到slack群，并\@assignee                                                                                                                                                                                                       | 文杰                        |
| AppAnnie 爬虫         | 服务器定时任务    | 定时爬取AppAnnie数据，自动生成报表发送到邮箱                                                                                                                                                                                                               | 柬文                        |
| Git Issue 爬虫        | python脚本        | QA借助这个工具爬取git issue，生成issue状态报表                                                                                                                                                                                                             | 涛哥                        |
| Git分支负责人统计工具 | 脚本              | 统计git的远端分支信息，计算分支的owner，生成todo，让owner检查分支是否可以删除                                                                                                                                                                              | 刘龙                        |
| libMax工具            | AS external tools | 围绕libMax（多App代码共享库，是一个submodule）的便捷工具                                                                                                                                                                                                   | 振亮                        |

举两个例子：

【自动编译工具】

    自动编译工具实质是一个持续集成工具，它包括：

-   代码的自动编译（DEBUG）、打包（RELEASE）过程

-   代码仓库（GitLab）

-   一台服务器

搭建流程参照*GitLab CI*（Continuous Integration：持续集成）官方文档。

    我们组通过集成GitLab
CI到一台闲置的电脑上，当Develop和Master分支有新的提交时，自动运行编译/打包脚本，将结果反馈出来。

自动编译工具能在公司内推广，全靠开新总的一句话！

    集成GitLab CI后，我们可以：

-   在slack上快速查看编译结果  
    

    ![](media/413bd5f084cb6362fab388905aa18ecd.png)

-   通过GitLab下载编译好的Apk文件以及打包的输出文件  
    

    ![](media/bb906c171557c3a440b5b612b4877cb4.png)

-   通过打包时注入的脚本：

    -   lint检查确保代码没有问题

    -   检查view的构造函数有没有缺失

    -   统计四大组件所在的进程

    -   检查RemoteConfig中没有被引用到的配置项 / RemoteConfig中缺失的配置项

-   服务器上包含所有项目最新develop或master分支的代码，从而提供宏观的代码比对服务  
    

    ![](media/7380d000fd390051b4ca0db190e16dc3.png)

【SmartCanary】

SmartCanary是Debug环境下的自动化性能检测工具，负责检查Activity的内存泄漏等性能问题，并通知到Slack群。SmartCanary放在公司maven仓库里，通过gradle的compile方法集成：

>   def CANARY_VERSION = ".gm:+"

>   debugCompile "com.ihandy.ia.canary:smartcanary-android\$CANARY_VERSION"

>   releaseCompile
>   "com.ihandy.ia.canary:smartcanary-android-no-op\$CANARY_VERSION"

设计成这种形式主要因为：

1.  开源库LeakCanary的"no-op"版提供的接口太少，如果订制一些功能，在打包时会出现找不到类和方法的编译错误。

2.  使用".gm:+”，App只需要集成一次。

3.  在打包时，能确保不会出现调试代码。

    SmartCanary目前包含三个模块：

-   **Leak**：通过集成LeakCanary开源库，当Activity发生内存泄漏问题时，上传到服务器，服务器存储后转发到slack群，并\@模块负责人。

    -   在dependencies中添加下面代码即可集成LeakCanary：

>   debugCompile 'com.squareup.leakcanary:leakcanary-android:1.5.4'

>   releaseCompile 'com.squareup.leakcanary:leakcanary-android-no-op:1.5.4'

-   如果在Application的onCreate里通过LeakCanary.install(application)方法激活LeakCanary，出现Activity的内存泄漏时会发送notification，可以通过桌面的icon点进去看详情。但是这样消息会比较分散，不方便整体的管理。

-   我们通过查看LeakCanary源码，修改了install的方式，采用以下方法代替传统的LeakCanary.install(application)方法：

    -   LeakCanary  
            .*refWatcher*(application)  
            .watchDelay(120, TimeUnit.*SECONDS*)  
            .listenerServiceClass(AutoUploadDisplayLeakService.class)  
            .excludedRefs(AndroidExcludedRefs.*createAppDefaults*().build())  
            .buildAndInstall();

-   *refWatcher*(application)：设置引用监听。传入application是因为要通过注册Application.ActivityLifecycleCallbacks来监听Activity的onDestroy；我们普遍认为Activity走完onDestroy之后，对象应该在一定时间内被销毁。

-   watchDelay(120,
    TimeUnit.*SECONDS*)：设置观察时间，默认是5秒。LeakCanary监听到Activity的onDestroy后，会创建一个弱引用，时间到了之后，GC，如果弱引用还在，说明Activity对象还在，此时会去DumpHeap、分析问题。然而往往会有动画或者后台线程在onDestroy之后5秒还在运行，我们接受这种情况，因此将阈值设置为2分钟。

-   listenerServiceClass(AutoUploadDisplayLeakService.class)：设置接收Service，Service需要继承自DisplayLeakService。LeakCanary分析完问题后，会将结果发送到这个service，然后调用afterDefaultHandling方法。于是我们自定义了AutoUploadDisplayLeakService，并重载了此方法。（目前服务器地址是写死在库里的，会发送到IA组的服务器上）  
    \@Override  
    protected void afterDefaultHandling(HeapDump heapDump, AnalysisResult
    result, String leakInfo) {  
        if (result.excludedLeak) {  
            return;  
        }  
      
      
        if (result.leakFound \|\| result.failure != null) {  
            String leakDetailString = LeakCanary.*leakInfo*(this, heapDump,
    result, true); // leakInfo  
            String leakPreviewString = LeakCanary.*leakInfo*(this, heapDump,
    result, false);  
            LeakCanaryUploadManager.*getInstance*().upload(leakDetailString,
    leakPreviewString);  
        }  
    }

-   发送消息到Slack的代码可以参考SlackUtils类（也可以参考Slack的*attachments*文档）。

-   一个没有释放广告造成的内存泄漏：

![](media/0ee1371ca4850a39f09752a1ce29d64c.png)

-   **Block**：最开始的设计是，通过Looper的setMessageLogging方法为mainLooper设置Printer（Printer将作为Looper的mLogging成员变量）

    -   public void setMessageLogging(\@Nullable Printer printer) {  
            mLogging = printer;  
        }

-   Looper在消息处理前会调用：

    -   final Printer logging = me.mLogging;  
        if (logging != null) {  
            logging.println("\>\>\>\>\> Dispatching to " + msg.target + " " +  
                    msg.callback + ": " + msg.what);  
        }

-   Looper在消息处理结束后，会调用：

    -   if (logging != null) {  
            logging.println("\<\<\<\<\< Finished to " + msg.target + " " +
        msg.callback);  
        }

-   通过解析字符串，把ß运行时间较长的消息通过log打印出来。

-   **Assertion**：App端可以借助SmartCanary的上传、转发的机制检查代码问题。如果代码中发现一些问题需要即时得到解决，除了Debug抛Exception外，还可以利用这套机制将问题暴露出来。

SmartCanary帮助我们确保了内存泄漏问题，一旦发生了内存泄漏，并且设备连上了公司的网络，那么会立即上报到slack群，即使上传失败，SmartCanary也会不断尝试，直到服务器告诉它“内存泄漏已经成功发送到slack群”，它才会停下来。同时，SmartCanary在查找性能原因的时候也帮我们提供了方便。

   
最重要的是，SmartCanary为我提供了发挥想象力的空间。release版的App不会包含任何调试代码，因此在不影响程序正常运行的情况下，可以随意添加调试代码。

其他工具一览：

| 工具                  | 载体              | 功能                                                                                                                                                                                                                                                       | 作者                        |
|-----------------------|-------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------|
| Flurry查询工具        | Web服务           | 提供轻便的flurry查询服务 <http://192.168.1.14/#/flurry> 开发中：PM用EventName编写表达式，批量计算结果                                                                                                                                                      | 博哥-\>文杰                 |
| Gradle工具            | gradle脚本        | 利用gradle脚本，在编译时： 1.生成git节点、编译时间等信息； 2.加密RemoteConfig文件；（项目asset中的RemoteConfig文件是明文） 3.安装git hook；（提交时必须注明代码“已复查”，如果没有，将不允许提交；提交时自动尝试拉取代码） 4.开发中：检查广告是否忘记Active | 1.振亮 2.振亮 3.晓剑 4.王盟 |
| 一键导入翻译工具      | 脚本              | 一键将翻译的字符串导入到对应的strings.xml下                                                                                                                                                                                                                | 柬文                        |
| 多语言字符串工具      | AS plugin         | 对多语言进行移动、删除操作                                                                                                                                                                                                                                 | 涛哥                        |
| IAPusher              | 服务器定时任务    | 通过trello维护随手、闭环和扫雷，定时发送到dev群（组内级别的“做事准则”）                                                                                                                                                                                    | 文杰                        |
| RemoteConfig工具      | Web服务           | 根据app的版本将config文件加密成正确的加密格式                                                                                                                                                                                                              | 博哥-\>文杰                 |
| AdConfig工具          | 应用程序          | （GoldenEye之前）广告config的批量导入和导出操作                                                                                                                                                                                                            | 博哥-\>小智                 |
| Issue推送服务         | Web服务           | 自动将GitLab的issue的变动发送到slack群，并\@assignee                                                                                                                                                                                                       | 文杰                        |
| AppAnnie 爬虫         | 服务器定时任务    | 定时爬取AppAnnie数据，自动生成报表发送到邮箱                                                                                                                                                                                                               | 柬文                        |
| Git Issue 爬虫        | python脚本        | QA借助这个工具爬取git issue，生成issue状态报表                                                                                                                                                                                                             | 涛哥                        |
| Git分支负责人统计工具 | 脚本              | 统计git的远端分支信息，计算分支的owner，生成todo，让owner检查分支是否可以删除                                                                                                                                                                              | 刘龙                        |
| libMax工具            | AS external tools | 围绕libMax（多App代码共享库，是一个submodule）的便捷工具                                                                                                                                                                                                   | 振亮                        |

工具开发的注意事项

1.  工具的目标：提升工作效率。任何工具都不能偏离这个目标。

2.  工具越精简越好：目的是实用以及便于维护。工具应该在确保提升工作效率的前提下，尽量降低成本。

    1.  精简工具的UI：大部分工具都是一个脚本，除非UI能给我们很多好处（例如上面的宏观代码比对），一般情况下是不需要UI的；

    2.  近似方法：“Git分支负责人统计工具”中，计算分支owner就是采用的近似算法，会有个别owner计算错误，这时候需要收到todo的同学手动把todo转出去；

    3.  调整需求：有时候可以通过调整需求，甚至让用户多一点学习成本来精简工具，例如一键导入翻译工具，PM使用这个工具后可以不再一个一个添加多语言字符串，但是需要自己运行python代码，并且需要将翻译的字符串严格按照规定的格式导入。

3.  工具的开发也需要有生命周期：<https://trello.com/b/zD4EExNQ/ia-tools>

    1.  任务池：工具的需求提出之后，先进入任务池

    2.  需求分析：分析需求，评估工具的投入产出比，如果可行，则进入开发阶段；

    3.  开发：工具也有Due Date，但是通常情况下优先级都比较低；

    4.  测试：对于工具来说，测试就是“试用”；

    5.  文档：部分工具需要写说明文档；

    6.  开发完成：完成之后的工具进入“已有工具”列表。

【附件】

lint脚本：

在app的gradle里添加代码：

>   lintOptions {

>       checkReleaseBuilds true

>       ignoreWarnings true

>       abortOnError true

>       disable 'MissingTranslation', ‘MissingQuantity’ //
>   忽略了缺翻译和缺Quantity项的检查。

>       lintConfig file("../libs/ihs/libMax/lint.xml”) // 指向lint文件

>   }

\<此处有一个lint.xml文件，请到IA - Tools 分享.resources文件中查看\>

gradle脚本：

\<此处有一个build.gradle.txt文件，请到IA - Tools 分享.resources文件中查看\>
