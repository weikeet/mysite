为什么要重载equal()方法？
=========================

        因为Object的equal方法默认是两个对象的引用的比较，只有指向同一个内存中的地址，才相等，不然不相等。如果需要利用对象里面的属性来判断是否相等，需要重写equals()方法。

**现象：**

先看一下要对比对象Coder：（这种equals()方法是参考了系统String类的写法，有对应的适用场景）

![](media/9a83c0b69b607e61bce792fefcef03cf.png)

然后执行如下代码：

![](media/f9283a91d1a09b67ba5f055c5f470045.png)

我们期待的结果是都为true

结果：是不是有点暴跌眼镜

![](media/0b8965c9f7794f91ab6a68755da8e023.png)

解析：
======

参考链接：*http://www.cnblogs.com/dolphin0520/p/3681042.html*

        先讲一下hashCode的作用。一般hash系列的集合都不允许集合中存在重复元素，那么需要查重判断。使用元素的equals()对比是一种方法，但是如果已经有上万条数据了呢？效率是不得不考虑的。

        hashCode因此应运而生。集合每存一条不相同的元素就把此元素的HashCode值用一个table存储起来，当集合要添加新对象的时候，先调用新添加对象的hashCode方法获取hashCode值和存储在table中的HashCode值进行对比（int数值的对比效率高），如果不存在相同HashCode值，那么可以说明无重复，就直接存进去了，不再做任何比较了；如果有已经存在的hashCode值，那么会再次调用equals()方法进行比较，如果equals()相同，那就不保存了，如果equals()不相同，说明无重复，存下来，存的位置会和这个同HashCode的元素的物理地址在一起。

        这样就大大减少调用equals()方法的概率，节省了效率。HashCode就是根据一定的规则将与对象相关的信息（对象的存储地址，对象的字段等）映射成一个数值，这个值称作散列值。

        在Hash系列的集合中查找元素（执行contains(obj)方法）的时候，HashCode是非常重要的，会先获取obj对象的HashCode值，然后在集合存已有元素HashCode值的Table中查找，如果没有同HashCode值，就直接断定不包含了，返回false了。刚刚的案例，如果不重写HashCode值，那么得到的系统提供的hashCode值（一般是根据物理地址计算的）是：

![](media/cc87b5681b848dd83a7f4a5881a184ea.png)

        完全不一样对不对，所以contain直接返回false了。此时虽然coder1和coder2的equals()方法返回值为true，但不允许重复的HashSet集合也是可以存入这两个元素的，根本原因就是判断是否重复先判断的是HashCode值。

        同样的操作我们换成ArrayList上，结果如下：

![](media/de26a33f2cc288427d5a0436af302b93.png)

         ArrayList做contains()判断得到的结果是true。为什么呢？因为ArrayList做contains()判断时，只做equals()方法判断，用不到HashCode值

        再回到如果在Hash系列的集合中查找元素的那里，如果obj对象HashCode的值在该集合存已有元素的HashCode值的Table中存在，那么会根据这个HashCode值直接定位到对应的物理地址上，也就是指定了此HashCode值的内存块上（会忽略其他内存块，效率很高），和此内存块中的元素进行equals()方法对比，得到结果。HashCode做第一层筛选，可以否决不可以肯定，equals()方法做第二层筛选，可以否决可以肯定。

参考链接：<http://blog.csdn.net/neosmith/article/details/17068365>

        下图可以参考HashTable的一个参考图，节点下面有可能存在新的树形结构，不能用人类思维来理解HashTable。

![](media/bbbc65d4a71738eaf0dda04542ccadb1.png)

        接下来就是HashCode方法的健壮性的问题了，可以随便重写一个HashCode方法，比如我喜欢8这个数字，可以这样写。

![](media/e7f3d2e21405140d7c5143ffe3d78ebd.png)

        这样写是满足HashSet的重复判断的，再运行结果如下：

![](media/9e6acbe8299fda57c4e9229dfc48dccb.png)

        但是这样写HashCode，就失去了哈希的意义，哈希表退化成了链表，成了低效率的集合。每个元素的hashCode()方法都返回相同的值，那么所有的对象都会被放到同一个HashCode指定的bucket（内存块）中，每次执行查找操作都会遍历进行equals()操作，那么完全失去了哈希表的作用了，所以需要一个健壮的hashCode()。

**hashCode()方法健壮要求：**

        1.在程序执行期间，只要equals方法的比较操作用到的信息没有被修改，那么对这同一个对象调用多次，hashCode方法必须始终如一地返回同一个整数。

        2.如果两个对象根据equals方法比较是相等的，那么调用两个对象的hashCode方法必须返回相同的整数结果。

        3.如果两个对象根据equals方法比较是不等的，则hashCode方法不一定得返回不同的整数。

        写成下面的样子就行了

![](media/e6ee93f6b2828e785f3cef04c94ec0f4.png)

        上面的数值可以写成自己喜欢的，个人觉得只要是质数就比较好。这样写就足够严谨，可以很大程度上避免两个不同的元素出现相同的HashCode的情况。

        代码运行结果为：

![](media/7979080346b9e0740a6810730bdbbc87.png)

**最后再提供一种更健壮的equals()方法的写法：**

![](media/40f5dfc52addba30517385578b217c7e.png)

        这种方法通过字节码文件类型对比，必须是同一种类型，不像之前的写法，父类和子类是区分不出来的。
