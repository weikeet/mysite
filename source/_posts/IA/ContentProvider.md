Socket Binder

Zygote 

引用计数

本次分享源码取自Android 26版本（不排除因手抖贴成25版本代码的情况）

前言

先来看看ContentProvider（CP）是什么：

>   *\* Content providers are one of the primary building blocks of Android
>   applications, providing*

>   *\* content to applications. They encapsulate data and provide it to
>   applications through the single*

>   *\* {\@link ContentResolver} interface. A content provider is only required
>   if you need to share*

>   *\* data between multiple applications. For example, the contacts data is
>   used by multiple*

>   *\* applications and must be stored in a content provider. If you don't need
>   to share data amongst*

>   *\* multiple applications you can use a database directly via*

>   *\* {\@link android.database.sqlite.SQLiteDatabase}.*

这是官方文档（或者源代码）里给出的对ContentProvider的定位，简单翻译一下就是：

>   ContentProvider是安卓四大组件之一，主要负责为各应用提供数据。它封装了数据，并将其通过单一的ContentResolver提供给各应用。当你需要在多应用间共享数据时才需要使用ContentProvider。例如：通信录数据被多个应用使用，它必须存到ContentProvider里。如果你不需要多应用共享数据，你可以直接使用数据库。

URI

CP使用URI来表示需要操作的数据，URI包括三部分：

scheme: 安卓规定CP的scheme为 content://

authority: 签名，和CP在xml文件里定义的authorities一致，用于确定该URI对应哪个CP

path: 路径，用于CP内定位具体的数据

例如操作MediaStore的图片的URI为：

>   content://media/external/images/media

其中authority为media，path为external/image/media

我们的URI通常定义为这样：

>   private static final String *AUTHORITY_NAME* = ".cpu_cooler";

>   private static Uri createBaseUri(Context context) {

>       return Uri.*parse*("content://" + context.getPackageName() +
>   *AUTHORITY_NAME* + "/");

>   }

manifest.xml

>   \<provider

>       android:name=".module.cpucooler.CpuContentProvider"

>       android:authorities="\${applicationId}.cpu_cooler"

>       android:enabled="true"

>       android:exported="false"

>       android:process=":work" /\>

ContentProvider提供数据的方法：

总的来说有两大类：

-   通过query、insert、delete、update方法来提供数据；对应的，获取数据需要使用ContentResolver的query、insert、dalete、update方法

-   通过call方法来提供数据；对应的，获取数据需要使用ContentResolver的call方法

1.通过query、insert、delete、update方法来提供数据：

    在新建CP的子类时，你会发现有6个方法是必须实现的：

![](media/e36f08b2cf8982aa962e2f32e4c562f5.png)

   
而ContentResolver里也有对应的query、insert、delete、update方法，如果CP正确实现了这些方法，则可以ContentResolver就可以通过这些方法从CP中获取数据，例如可以通过下面方法从MediaStore（系统进程）中获取所有的照片：

>   private static final String[] *IMAGES_PROJECTION* = {

>           MediaStore.Images.Media.*\_ID*,

>           MediaStore.Images.Media.*DATA*,

>           MediaStore.Images.Media.*SIZE*,

>           MediaStore.Images.Media.*DATE_MODIFIED*

>   };

>   HSApplication.*getContext*().getContentResolver().query(MediaStore.Images.Media.*EXTERNAL_CONTENT_URI*,
>   *IMAGES_PROJECTION*, null, null, MediaStore.Images.Media.*DATE_MODIFIED* + "
>   desc");

这么方法适合跨应用使用，通用性较强，不需要知道对方CP的类结构，但是实现难度比较大，需要双方都遵守SQLite的姿势；

2.第一种方法通用性好，但是对于有一些很难用数据库来描述的数据，使用第一种方法会有些别扭，于是CP提供了第二种提供数据的方法——call

>   *\* Call a provider-defined method.  This can be used to implement*

>   *\* interfaces that are cheaper and/or unnatural for a table-like*

>   *\* model.*

>   *…*

>   *public \@Nullable Bundle call(\@NonNull String method, \@Nullable String
>   arg,*

>   *        \@Nullable Bundle extras) {*

>   *    return null;*

>   *}*

CP定义实现call方法，解析method、arg和extras

>   \@Nullable

>   \@Override

>   public Bundle call(String method, String arg, Bundle extras) {

>       Bundle bundle = new Bundle();

>       switch (method) {

>           case *METHOD_GET_LAST_SCAN_TIME*:

>               bundle.putLong(*EXTRA_KEY_LAST_SCAN_TIME*,
>   HSPreferenceHelper.*create*(HSApplication.*getContext*(),
>   *MODULE_NAME*).getLong(*PREF_KEY_LAST_SCAN_TIME*, 0));

>               break;

>   ...

>           default:

>               break;

>       }

>       return bundle;

>   }

   
客户端通过ContentResolver的call来调用CP定义的方法，只不过咱们通常是在CP里再封装了一层静态方法：

>   public static long getLastScanTime() {

>       Bundle bundle =
>   HSContentProviderUtils.*call*(*createBaseUri*(HSApplication.*getContext*()),
>   *METHOD_GET_LAST_SCAN_TIME*, null, null);

>       return null == bundle ? 0 : bundle.getLong(*EXTRA_KEY_LAST_SCAN_TIME*,
>   0);

>   }

>   其中HSContentProviderUtils转调了：

>   Bundle bundle =
>   context.getApplicationContext().getContentResolver().call(uri, method, arg,
>   extras);

相信大家都很熟悉进程的概念了，这里我们再讨论一下为什么我们需要CP。

【CP的使用场景一】

   
SharedPreference（简称SP）：这是安卓提供的一种便捷的数据存储方式，但是它是非进程安全的。

【题外话

（这里申明一下，所谓SP的线程安全是说SP提供的基本操作是安全的，例如多个线程同时往同一个SP里面写的时候，如果key不冲突，则所有的写操作都能够成功；但是下面代码并不是安全的，需要通过synchronized关键词来确保数据的正确性；因此，我们下面讨论的安全也是指基本操作是安全的）。

![](media/b545d0dd4eb951ffb59b92df8987af26.png)

![](media/a241a68322101ce804fd49110693c541.png)

】

回到正题，SP是非进程安全的，是因为SP有自己的内存缓存，所以，多进程操作同一个SP文件时，由于每个进程都有自己的内存缓存，所以会存在后写入SP的进程会覆盖先写入的进程的结果。虽然曾经有一个标记可以通过将SP退化成文件操作的方式来实现进程安全，但是这个标记已经废弃了——

>   *\* \@deprecated MODE_MULTI_PROCESS does not work reliably in*

>   *\* some versions of Android, and furthermore does not provide any*

>   *\* mechanism for reconciling concurrent modifications across*

>   *\* processes.  Applications should not attempt to use it.  Instead,*

>   *\* they should use an explicit cross-process data management*

>   *\* approach such as {\@link android.content.ContentProvider
>   ContentProvider}.*

    好在有替代方案——也就是上面注释中提到的ContentProvider；

   
既然SP无法做到进程安全，那我们就通过CP封装SP，让它只在一个进程中访问，这样既保留了SP的特性，又能确保进程安全。

    所以framework为我们提供了便捷的跨进程使用SP的方法

>   HSPreferenceHelper.*create*(HSApplication.*getContext*(),
>   *PREF_FILE_EXTERNAL_ADS_UTILS*).getBooleanInterProcess(*PREF_KEY_IS_EXTERNAL_ADS_NEW_USER*,
>   false);

   
它的内部实现是通过一个叫做PreferenceProvider的CP，并指定multiprocess为false（默认就是false，为true将不能满足我们的需求，所以我们不考虑为true的情况）来将SP的各种方法进行了封装转调，PreferenceProvider被我们定义在了work因进程，所以，最终只有一个进程可以访问SP文件。

![](media/d5790e5d01ac353f5e196f3811945097.png)

    当然，我们可以自己实现CP来封装SP，详细参考libmax版本的SettingProvider。

【CP的使用场景二】

   
内存数据：多进程环境下，不同进程之间的内存数据是无法直接访问的，例如我们在work进程中初始化了一个单例Manager，如果我们尝试在主进程访问这个单例是，往往结果就是再次创建了一个新的单例，这并不是我们想要的——

![](media/837b48730ce777c6dde66404f436f020.png)

所以，为了解决这种问题，我们需要使用CP来封装一个在所有进程中唯一的一个对象；

例如AppLock，由于需要常驻后台监控前后台变化情况，所以需要将核心逻辑（AppLockController，是一个单例）放到保活能力较强的work进程，而从AppLock的主界面在主进程，主界面要和AppLockController进行通信，这是CP就起到了这个通信中间人的作用——

![](media/4fb17eb3d4a059ac4105ada0fc4f0c3e.png)

上面都是废话，我们这次分享的主要目的还是：

深入了解CP的具体实现

我们都知道，CP内部是Binder实现的，它的类图：

![](media/7f898eb6154582f9ba0b124df55e5593.png)

其中mTransport就是最终负责CP跨进程通信的IBinder，可以说其他的一切操作都是为了维护这个mTransport（包括权限检查、缓存、链接、adj等），例如mTransport中的call方法：

>   \@Override

>   public Bundle call(String callingPkg, String method, \@Nullable String arg,
>   \@Nullable Bundle extras) {

>       Bundle.setDefusable(extras, true);

>       final String original = setCallingPackage(callingPkg);

>       try {

>           return ContentProvider.this.call(method, arg, extras);

>       } finally {

>           setCallingPackage(original);

>       }

>   }

ApplicationContentResolver继承于ContentResolver,
是ContextImpl的内部类，它的实现往往是通过转调ActivityThread来完成；

其实CP的跨进程通信原理就可以大致地猜出来了——我们之前介绍过：要通过Binder实现跨进程通信，首先需要拿到对方的IBinder，而AMS会帮助我们保存了所有已发布的ContentProvider的IBinder，所以只要找AMS要到我们想要的IBinder，就可以随心所欲地进行跨进程通信了（之前的跨进程插屏广告也就是这个原理）。

然而ContentProvider实际上比我想象的多了很多细节，下面我们来跟踪ContentResolver的call方法的实现：

跟踪前先介绍几个概念：

![](media/440c818c8b0d4b36bd5444920681b3d4.png)

system_server中：

-   **ProcessRecord**:
    AMS用来记录进程的对象，所有与应用进程相关的信息都存到这里了，同时包括四大组件的记录；

-   **ContentProviderRecord**:
    AMS用来记录ContentProvider的对象，里面有AMS、ProviderInfo、uid、ApplicationInfo、ComponentName、IContentProvider(mTransport的句柄)、ArrayList\<**ContentProviderConnection**\>等成员变量；

-   **ProviderInfo**:
    Manifest里定义的那些东西，主要是authority和permission这些；

-   **ContentProviderConnection: **即进程和CP间的连接，里面有**ProcessRecord**,
    **ContentProviderRecord**、stableCount、unstableCount等参数；stableCount、unstableCount与进程的存活有关。

-   **ContentProviderHolder**:
    调用AMS的getContentProvider方法时，拿到的不是IContentProvider(mTransport的句柄)对象、而是**ContentProviderHolder**对象。**ContentProviderHolder**对IContentProvider进行了封装，它封装了连接，并支持了懒加载，也就是说当调用进程和CP进程为同一个进程时，它允许IContentProvider推迟到调用进程去加载，而不是在AMS进程阻塞等待（后面会介绍）。

应用进程中：

-   **ActivityThread**:
    应用进程的入口，主要负责和AMS通信，接收AMS的指令来维护当前进程的各组件的状态，ContentProvider也不例外，下面是它的部分成员变量：

    -   final ArrayMap\<**ProviderKey**, **ProviderClientRecord**\>
        **mProviderMap** = new ArrayMap\<**ProviderKey**,
        **ProviderClientRecord**\>(); //
        当前缓存的**ProviderClientRecord**（其实就是IContentProvider），key为**ProviderKey**

    -   final ArrayMap\<IBinder, **ProviderRefCount**\> **mProviderRefCountMap**
        = new ArrayMap\<IBinder, **ProviderRefCount**\>(); //
        对某个CP的引用计数，IBinder是IContentProvider(mTransport)，也就是对应一个CP对象

    -   final ArrayMap\<IBinder, **ProviderClientRecord**\> **mLocalProviders**
        = new ArrayMap\<IBinder, **ProviderClientRecord**\>(); // 该进程创建的CP

    -   final ArrayMap\<ComponentName, **ProviderClientRecord**\>
        **mLocalProvidersByName** = new ArrayMap\<ComponentName,
        **ProviderClientRecord**\>(); //
        用ComponentName重新索引了一次该进程创建的CP，看样子它是后来加的，为了解决4.0的那个bug。

-   **ProviderKey**: authority和uid作为key

-   **ProviderClientRecord**:
    authorities、IContentProvider、ContentProvider、**ContentProviderHolder**的组合。只有**mLocalProvider**里的ContentProvider成员变量不为空。

-   **ProviderRefCount**:
    该进程对某个CP的引用计数（stableCount和unstableCount），引用计数都为0时，往H（主线程）里抛断开连接（**ContentProviderConnection**）的消息(关于往H里抛断开连接的消息，一个有意思的注释：//
    TODO: it would be nice to post a delayed message, so if we come back and
    need the same provider quickly we will still have it
    available.)，让AMS把这个连接从**ContentProviderRecord**里删掉

1.contentResolver.call

>   public final \@Nullable Bundle call(\@NonNull Uri uri, \@NonNull String
>   method,

>           \@Nullable String arg, \@Nullable Bundle extras) {

>       Preconditions.checkNotNull(uri, "uri");

>       Preconditions.checkNotNull(method, "method");

>       IContentProvider provider = acquireProvider(uri);

>       if (provider == null) {

>           throw new IllegalArgumentException("Unknown URI " + uri);

>       }

>       try {

>           final Bundle res = provider.call(mPackageName, method, arg, extras);

>           Bundle.setDefusable(res, true);

>           return res;

>       } catch (RemoteException e) {

>           // Arbitrary and not worth documenting, as Activity

>           // Manager will kill this process shortly anyway.

>           return null;

>       } finally {

>           releaseProvider(provider);

>       }

>   } — ContentResolver.java

关键的代码已经高亮标出来了，其中 IContentProvider provider =
acquireProvider(uri); 就是去拿目标CP的mTransport的句柄（即IContentProvider）；然后直接调用
provider.call(mPackageName, method, arg,
extras) 方法，相当于同步执行mTransport里的call方法，也就是调用ContentProvider的call方法，从而实现跨进程通信；releaseProvider(provider);表示“释放”对provider的引用计数，当acquireProvider成功时，会“增加”引用计数，这里是对称的；如果此进程对某ContentProvider的引用为0时，会在下一个主线程消息中移除对此CP的连接及缓存（这里先不要理解成我们经常认为的release就行）。

2.acquireProvider(uri)

>   public final IContentProvider acquireProvider(Uri uri) {

>       if (!*SCHEME_CONTENT*.equals(uri.getScheme())) {

>           return null;

>       }

>       final String auth = uri.getAuthority();

>       if (auth != null) {

>           return acquireProvider(mContext, auth);

>       }

>       return null;

>   } — ContentResolver.java

3.acquireProvider(mContext, auth);

这个方法的具体实现在ContextImpl的ApplicationContentResolver里

>   \@Override

>   protected IContentProvider acquireProvider(Context context, String auth) {

>       return
>   mMainThread.acquireProvider(context, ContentProvider.getAuthorityWithoutUserId(auth), resolveUserIdFromAuthority(auth),
>   true);

>   } — ContextImpl.java

其中 mMainThread 就是ActivityThread

4.mMainThread.acquireProvider(context, ContentProvider.getAuthorityWithoutUserId(auth),resolveUserIdFromAuthority(auth),
true);

>   public final IContentProvider acquireProvider(

>           Context c, String auth, int userId, boolean stable) {

>       final IContentProvider provider = acquireExistingProvider(c, auth,
>   userId, stable); // 查询缓存

>       if (provider != null) {

>           return provider;

>       }

>       // There is a possible race here.  Another thread may try to acquire

>       // the same provider at the same time.  When this happens, we want to
>   ensure

>       // that the first one wins.

>       // Note that we cannot hold the lock while acquiring and installing the

>       // provider since it might take a long time to run and it could also
>   potentially

>       // be re-entrant in the case where the provider is in the same process.

>       ContentProviderHolder holder = null;

>       try {

>           holder = ActivityManager.*getService*().getContentProvider(

>                   getApplicationThread(), auth, userId, stable); //
>   找AMS拿ContentProviderHolder

>       } catch (RemoteException ex) {

>           throw ex.rethrowFromSystemServer();

>       }

>       if (holder == null) {

>           Slog.e(*TAG*, "Failed to find provider info for " + auth);

>           return null;

>       }

>       // Install provider will increment the reference count for us, and break

>       // any ties in the race.

>       holder = installProvider(c, holder, holder.info,

>               true /\*noisy\*/, holder.noReleaseNeeded, stable); //
>   安装Provider

>       return holder.provider; // 返回IContentProvicer对象

>   }

1.  查询该进程中(ActivityThread中)缓存的IContentProvider——final IContentProvider
    provider = acquireExistingProvider(c, auth, userId, stable);

    1.  命中：直接返回

    2.  未命中：找AMS拿ContentProviderHolder——holder =
        ActivityManager.*getService*().getContentProvider(getApplicationThread(),
        auth, userId, stable);

        1.  holder为null：返回null

        2.  holder不为null：安装Provider并返回holder中的provider对象(IContentProvider)——holder
            = installProvider(c, holder, holder.info, true /\*noisy\*/,
            holder.noReleaseNeeded, stable);

5.1 final IContentProvider provider = acquireExistingProvider(c, auth, userId,
stable);

>   public final IContentProvider acquireExistingProvider(

>           Context c, String auth, int userId, boolean stable) {

>       synchronized (mProviderMap) {

>           final ProviderKey key = new ProviderKey(auth, userId);

>           final ProviderClientRecord pr = mProviderMap.get(key);

>           if (pr == null) {

>               return null;

>           }

>           IContentProvider provider = pr.mProvider;

>           IBinder jBinder = provider.asBinder();

>           if (!jBinder.isBinderAlive()) {

>               // The hosting process of the provider has died; we can't

>               // use this one.

>               Log.*i*(*TAG*, "Acquiring provider " + auth + " for user " +
>   userId

>                       + ": existing object's process dead");

>               handleUnstableProviderDiedLocked(jBinder, true);

>               return null;

>           }

>           // Only increment the ref count if we have one.  If we don't then
>   the

>           // provider is not reference counted and never needs to be released.

>           ProviderRefCount prc = mProviderRefCountMap.get(jBinder);

>           if (prc != null) {

>               incProviderRefLocked(prc, stable);

>           }

>           return provider;

>       }

>   }

本地的CP、系统的CP以及“最近正在访问”的CP会缓存在mProviderMap中，本地的CP会被标记成noReleaseNeeded，所以只要不出bug（比如4.0的那个bug，后来已修复），它将一直存在mProviderMap里。

5.2 holder =
ActivityManager.*getService*().getContentProvider(getApplicationThread(), auth,
userId, stable); // 找AMS拿ContentProviderHolder

最终会走到AMS的getContentProvider方法：

>   \@Override

>   public final ContentProviderHolder getContentProvider(

>           IApplicationThread caller, String name, int userId, boolean stable)
>   {

>       enforceNotIsolatedCaller("getContentProvider");

>       if (caller == null) {

>           String msg = "null IApplicationThread when getting content provider
>   "

>                   + name;

>           Slog.w(*TAG*, msg);

>           throw new SecurityException(msg);

>       }

>       // The incoming user check is now handled in
>   checkContentProviderPermissionLocked() to deal

>       // with cross-user grant.

>       return getContentProviderImpl(caller, name, null, stable, userId);

>   }

节选getContentProviderImpl方法：

>   private ContentProviderHolder getContentProviderImpl(IApplicationThread
>   caller,

>           String name, IBinder token, boolean stable, int userId) {

>       ContentProviderRecord cpr;

>       ContentProviderConnection conn = null;

>       ProviderInfo cpi = null;

>       synchronized(this) {

>           …

>   // First check if this content provider has been published...

>   cpr = mProviderMap.getProviderByName(name, userId);

>       …

>           boolean providerRunning = cpr != null && cpr.proc != null &&
>   !cpr.proc.killed;

>           if (providerRunning) { // provider在运行中

>               … 

>   if (r != null && cpr.canRunHere(r)) { //
>   如果调用进程和CP进程是同一个进程、或者支持multiprocess（CP在Manifest里定义multiprocess为true且调用App和CP所在App的uid相同），则直接返回，注意返回前把holder.provider置空了，这是为了让ActivityThread再创建一份localProvider的意思。

>         // This provider has been published or is in the process

>         // of being published...  but it is also allowed to run

>         // in the caller's process, so don't make a connection

>         // and just let the caller instantiate its own instance.

>         ContentProviderHolder holder = cpr.newHolder(null);

>         // don't give caller the provider object, it needs

>         // to make its own.

>         holder.provider = null;

>         return holder;

>         }

>               … 

>               conn = incProviderCountLocked(r, cpr, token, stable);

>               if (conn != null && (conn.stableCount + conn.unstableCount) ==
>   1) { 

>                   if (cpr.proc != null && r.setAdj \<=
>   ProcessList.*PERCEPTIBLE_APP_ADJ*) { //
>   传说中CP客户端会拉高CP所在进程的oomAdj的代码

>   // If this is a perceptible app accessing the provider,

>   // make sure to count it as being accessed and thus

>   // back up on the LRU list.  This is good because

>   // content providers are often expensive to start.

>            updateLruProcessLocked(cpr.proc, false, null);

>                   }

>               }

>               … 

>           }

>           if (!providerRunning) { // provider不在运行中

>       …

>               ComponentName comp = new ComponentName(cpi.packageName,
>   cpi.name);

>               cpr = mProviderMap.getProviderByClass(comp, userId);

>               final boolean firstClass = cpr == null;

>               if (firstClass) {

>                   final long ident = Binder.*clearCallingIdentity*();

>                   // If permissions need a review before any of the app
>   components can run,

>                   // we return no provider and launch a review activity if the
>   calling app

>                   // is in the foreground.

>                   if (mPermissionReviewRequired) {

>                       if
>   (!requestTargetProviderPermissionsReviewIfNeededLocked(cpi, r, userId)) {

>                           return null;

>                       }

>                   }

>                   try {

>                       checkTime(startTime, "getContentProviderImpl: before
>   getApplicationInfo");

>                       ApplicationInfo ai =

>                           AppGlobals.getPackageManager().

>                               getApplicationInfo(

>                                       cpi.applicationInfo.packageName,

>                                       *STOCK_PM_FLAGS*, userId);

>                       checkTime(startTime, "getContentProviderImpl: after
>   getApplicationInfo");

>                       if (ai == null) {

>                           Slog.w(*TAG*, "No package info for content provider
>   "

>                                   + cpi.name);

>                           return null;

>                       }

>                       ai = getAppInfoForUser(ai, userId);

>                       cpr = new ContentProviderRecord(this, cpi, ai, comp,
>   singleton); // 创建一个ContentProviderRecord，此时IContentProvider还是null

>                   } catch (RemoteException ex) {

>                       // pm is in same process, this will never happen.

>                   } finally {

>                       Binder.*restoreCallingIdentity*(ident);

>                   }

>               }

>               checkTime(startTime, "getContentProviderImpl: now have
>   ContentProviderRecord");

>               if (r != null && cpr.canRunHere(r)) { //
>   如果支持multiprocess，这里直接返回，让调用进程新创建一个ContentProvider，而不是阻塞等待CP发布

>                   // If this is a multiprocess provider, then just return its

>                   // info and allow the caller to instantiate it.  Only do

>                   // this if the provider is the same user as the caller's

>                   // process, or can run as root (so can be in any process).

>                   return cpr.newHolder(null);

>               }

>               if (DEBUG_PROVIDER) Slog.w(*TAG_PROVIDER*, "LAUNCHING REMOTE
>   PROVIDER (myuid "

>                           + (r != null ? r.uid : null) + " pruid " +
>   cpr.appInfo.uid + "): "

>                           + cpr.info.name + " callers=" +
>   Debug.getCallers(6));

>               // This is single process, and our app is now connecting to it.

>               // See if we are already in the process of launching this

>               // provider.

>               final int N = mLaunchingProviders.size();

>               int i;

>               for (i = 0; i \< N; i++) { // 检查被调用的CP是不是正在启动中

>                   if (mLaunchingProviders.get(i) == cpr) {

>                       break;

>                   }

>               }

>               // If the provider is not already being launched, then get it

>               // started.

>               if (i \>= N) { //
>   如果被调用的CP不在启动中，则尝试启动它所在的进程

>                   final long origId = Binder.*clearCallingIdentity*();

>                   try {

>                       // Content provider is now in use, its package can't be
>   stopped.

>                       try {

>                           checkTime(startTime, "getContentProviderImpl: before
>   set stopped state");

>                          
>   AppGlobals.getPackageManager().setPackageStoppedState(

>                                   cpr.appInfo.packageName, false, userId);

>                           checkTime(startTime, "getContentProviderImpl: after
>   set stopped state");

>                       } catch (RemoteException e) {

>                       } catch (IllegalArgumentException e) {

>                           Slog.w(*TAG*, "Failed trying to unstop package "

>                                   + cpr.appInfo.packageName + ": " + e);

>                       }

>                       // Use existing process if already started

>                       checkTime(startTime, "getContentProviderImpl: looking
>   for process record");

>                       ProcessRecord proc = getProcessRecordLocked(

>                               cpi.processName, cpr.appInfo.uid, false);

>                       if (proc != null && proc.thread != null && !proc.killed)
>   { //
>   进程本来是启动的，则尝试安装单个CP（其实就是告诉ActivityThread让他单独把这个CP再发布一次）

>                           if (DEBUG_PROVIDER) Slog.d(*TAG_PROVIDER*,

>                                   "Installing in existing process " + proc);

>                           if (!proc.pubProviders.containsKey(cpi.name)) {

>                               checkTime(startTime, "getContentProviderImpl:
>   scheduling install");

>                               proc.pubProviders.put(cpi.name, cpr);

>                               try {

>                                   proc.thread.scheduleInstallProvider(cpi);

>                               } catch (RemoteException e) {

>                               }

>                           }

>                       } else { // 否则启动进程，等待进程发布它的CP

>                           checkTime(startTime, "getContentProviderImpl: before
>   start process");

>                           proc = startProcessLocked(cpi.processName,

>                                   cpr.appInfo, false, 0, "content provider",

>                                   new
>   ComponentName(cpi.applicationInfo.packageName,

>                                           cpi.name), false, false, false);

>                           checkTime(startTime, "getContentProviderImpl: after
>   start process");

>                           if (proc == null) {

>                               Slog.w(*TAG*, "Unable to launch app "

>                                       + cpi.applicationInfo.packageName + "/"

>                                       + cpi.applicationInfo.uid + " for
>   provider "

>                                       + name + ": process is bad");

>                               return null;

>                           }

>                       }

>                       cpr.launchingApp = proc;

>                       mLaunchingProviders.add(cpr);

>                   } finally {

>                       Binder.*restoreCallingIdentity*(origId);

>                   }

>               }

>               checkTime(startTime, "getContentProviderImpl: updating data
>   structures");

>               // Make sure the provider is published (the same provider class

>               // may be published under multiple names).

>               if (firstClass) { // 把ContentProviderRecord加入mProviderMap

>                   mProviderMap.putProviderByClass(comp, cpr);

>               }

>               mProviderMap.putProviderByName(name, cpr);

>               conn = incProviderCountLocked(r, cpr, token, stable);

>               if (conn != null) {

>                   conn.waiting = true;

>               }

>           }

>           checkTime(startTime, "getContentProviderImpl: done!");

>           grantEphemeralAccessLocked(userId, null /\*intent\*/,

>                   cpi.applicationInfo.uid,
>   UserHandle.getAppId(Binder.*getCallingUid*()));

>       }

>       // Wait for the provider to be published...

>       synchronized (cpr) { //
>   阻塞等待cp发布，直到IContentProvider（即mTransport句柄）不为空

>           while (cpr.provider == null) {

>               if (cpr.launchingApp == null) {

>                   Slog.w(*TAG*, "Unable to launch app "

>                           + cpi.applicationInfo.packageName + "/"

>                           + cpi.applicationInfo.uid + " for provider "

>                           + name + ": launching app became null");

>                   EventLog.*writeEvent*(EventLogTags.AM_PROVIDER_LOST_PROCESS,

>                           UserHandle.getUserId(cpi.applicationInfo.uid),

>                           cpi.applicationInfo.packageName,

>                           cpi.applicationInfo.uid, name);

>                   return null;

>               }

>               try {

>                   if (DEBUG_MU) Slog.v(*TAG_MU*,

>                           "Waiting to start provider " + cpr

>                           + " launchingApp=" + cpr.launchingApp);

>                   if (conn != null) {

>                       conn.waiting = true;

>                   }

>                   cpr.wait();

>               } catch (InterruptedException ex) {

>               } finally {

>                   if (conn != null) {

>                       conn.waiting = false;

>                   }

>               }

>           }

>       }

>       return cpr != null ? cpr.newHolder(conn) : null;

>   } — AMS.java

5.3 holder = installProvider(c, holder, holder.info, true /\*noisy\*/,
holder.noReleaseNeeded, stable); // 安装Provider

参数输入基本来自ContentProviderHolder，其中c是context，代码在ActivityThread.java里：

>   */\*\**

>   *\* Installs the provider.*

>   *\**

>   *\* Providers that are local to the process or that come from the system
>   server*

>   *\* may be installed permanently which is indicated by setting
>   noReleaseNeeded to true.*

>   *\* Other remote providers are reference counted.  The initial reference
>   count*

>   *\* for all reference counted providers is one.  Providers that are not
>   reference*

>   *\* counted do not have a reference count (at all).*

>   *\**

>   *\* This method detects when a provider has already been installed.  When
>   this happens,*

>   *\* it increments the reference count of the existing provider (if
>   appropriate)*

>   *\* and returns the existing provider.  This can happen due to concurrent*

>   *\* attempts to acquire the same provider.*

>   *\*/*

>   private ContentProviderHolder installProvider(Context context,

>           ContentProviderHolder holder, ProviderInfo info,

>           boolean noisy, boolean noReleaseNeeded, boolean stable) {

>       ContentProvider localProvider = null;

>       IContentProvider provider;

>       if (holder == null \|\| holder.provider == null) { //
>   holder.provider为空表示需要在该进程创建此CP，if里面会创建一个localProvider

>           if (*DEBUG_PROVIDER* \|\| noisy) {

>               Slog.d(*TAG*, "Loading provider " + info.authority + ": "

>                       + info.name);

>           }

>           Context c = null;

>           ApplicationInfo ai = info.applicationInfo;

>           if (context.getPackageName().equals(ai.packageName)) {

>               c = context;

>           } else if (mInitialApplication != null &&

>                   mInitialApplication.getPackageName().equals(ai.packageName))
>   {

>               c = mInitialApplication;

>           } else {

>               try {

>                   c = context.createPackageContext(ai.packageName,

>                           Context.*CONTEXT_INCLUDE_CODE*);

>               } catch (PackageManager.NameNotFoundException e) {

>                   // Ignore

>               }

>           }

>           if (c == null) {

>               Slog.w(*TAG*, "Unable to get context for package " +

>                     ai.packageName +

>                     " while loading content provider " +

>                     info.name);

>               return null;

>           }

>           if (info.splitName != null) {

>               try {

>                   c = c.createContextForSplit(info.splitName);

>               } catch (NameNotFoundException e) {

>                   throw new RuntimeException(e);

>               }

>           }

>           try {

>               final java.lang.ClassLoader cl = c.getClassLoader();

>               localProvider = (ContentProvider)cl.

>                   loadClass(info.name).newInstance(); // CP对象通过反射创建

>               provider = localProvider.getIContentProvider();

>               if (provider == null) {

>                   Slog.e(*TAG*, "Failed to instantiate class " +

>                         info.name + " from sourceDir " +

>                         info.applicationInfo.sourceDir);

>                   return null;

>               }

>               if (*DEBUG_PROVIDER*) Slog.v(

>                   *TAG*, "Instantiating local provider " + info.name);

>               // XXX Need to create the correct context for this provider.

>               localProvider.attachInfo(c, info); //
>   绑定ProviderInfo到CP，同时调用CP的onCreate

>           } catch (java.lang.Exception e) {

>               if (!mInstrumentation.onException(null, e)) {

>                   throw new RuntimeException(

>                           "Unable to get provider " + info.name

>                           + ": " + e.toString(), e);

>               }

>               return null;

>           }

>       } else {

>           provider = holder.provider;

>           if (*DEBUG_PROVIDER*) Slog.v(*TAG*, "Installing external provider "
>   + info.authority + ": "

>                   + info.name);

>       }

>       ContentProviderHolder retHolder;

>       synchronized (mProviderMap) { // 暴力synchronized解决多线程竞争问题

>           if (*DEBUG_PROVIDER*) Slog.v(*TAG*, "Checking to add " + provider

>                   + " / " + info.name);

>           IBinder jBinder = provider.asBinder();

>           if (localProvider != null) { // 如果上面创建了localProvider

>               ComponentName cname = new ComponentName(info.packageName,
>   info.name);

>               ProviderClientRecord pr = mLocalProvidersByName.get(cname);

>               if (pr != null) { //
>   如果被别人抢先创建了本地CP，哦豁，上面的localProvider白创建了

>                   if (*DEBUG_PROVIDER*) {

>                       Slog.v(*TAG*, "installProvider: lost the race, "

>                               + "using existing local provider");

>                   }

>                   provider = pr.mProvider;

>               } else { //
>   没有被别人抢先，则上面创建的localProvider加入肯德基套餐

>                   holder = new ContentProviderHolder(info);

>                   holder.provider = provider;

>                   holder.noReleaseNeeded = true;

>                   pr = installProviderAuthoritiesLocked(provider,
>   localProvider, holder); //
>   这个里面新建了一个ContentProviderClient，并生成ProviderKey，将ContentProviderClient加入mProviderMap，而且没有引用计数（也就是说不会被释放）；

>                   mLocalProviders.put(jBinder, pr);

>                   mLocalProvidersByName.put(cname, pr);

>               }

>               retHolder = pr.mHolder;

>           } else { //
>   上面没有创建localProvider，也就是用的是别人的IContentProvider

>               ProviderRefCount prc = mProviderRefCountMap.get(jBinder);

>               if (prc != null) { //
>   明明让我去安装CP，却告诉我说已经有了别人的引用了，只好默默把别人的引用计数+1，然后断开我的连接

>                   if (*DEBUG_PROVIDER*) {

>                       Slog.v(*TAG*, "installProvider: lost the race, updating
>   ref count");

>                   }

>                   // We need to transfer our new reference to the existing

>                   // ref count, releasing the old one...  but only if

>                   // release is needed (that is, it is not running in the

>                   // system process).

>                   if (!noReleaseNeeded) {

>                       incProviderRefLocked(prc, stable);

>                       try {

>                          
>   ActivityManager.*getService*().removeContentProvider(

>                                   holder.connection, stable);

>                       } catch (RemoteException e) {

>                           //do nothing content provider object is dead any way

>                       }

>                   }

>               } else { //
>   创建ProviderClientRecord，放入mProviderMap，初始化引用计数（连接在AMS中已经建立好了，不用在这里创建；上面是因为竞争失败主动断开了连接）

>                   ProviderClientRecord client =
>   installProviderAuthoritiesLocked(

>                           provider, localProvider, holder);

>                   if (noReleaseNeeded) {

>                       prc = new ProviderRefCount(holder, client, 1000, 1000);

>                   } else {

>                       prc = stable

>                               ? new ProviderRefCount(holder, client, 1, 0)

>                               : new ProviderRefCount(holder, client, 0, 1);

>                   }

>                   mProviderRefCountMap.put(jBinder, prc);

>               }

>               retHolder = prc.holder;

>           }

>       }

>       return retHolder;

>   }

7.provider.call(mPackageName, method, arg, extras); // IContentProvider
provider;

acquireProvider拿到了IContentProvider，之后调用call也就是通过Binder同步调用了，provider的服务端为mTransport(Transport类型)，Transport是ContentProvider的内部类，Transport的call方法如下：

>   \@Override

>   public Bundle call(String callingPkg, String method, \@Nullable String arg,
>   \@Nullable Bundle extras) {

>       Bundle.setDefusable(extras, true);

>       final String original = setCallingPackage(callingPkg);

>       try {

>           return ContentProvider.this.call(method, arg, extras);

>       } finally {

>           setCallingPackage(original);

>       }

>   }

高亮的ContentProvider.this.call(method, arg,
extras);就是我们override的call方法了。

回到1：

>   public final \@Nullable Bundle call(\@NonNull Uri uri, \@NonNull String
>   method,

>           \@Nullable String arg, \@Nullable Bundle extras) {

>       Preconditions.checkNotNull(uri, "uri");

>       Preconditions.checkNotNull(method, "method");

>       IContentProvider provider = acquireProvider(uri);

>       if (provider == null) {

>           throw new IllegalArgumentException("Unknown URI " + uri);

>       }

>       try {

>           final Bundle res = provider.call(mPackageName, method, arg, extras);

>           Bundle.setDefusable(res, true);

>           return res;

>       } catch (RemoteException e) {

>           // Arbitrary and not worth documenting, as Activity

>           // Manager will kill this process shortly anyway.

>           return null;

>       } finally {

>           releaseProvider(provider);

>       }

>   } — ContentResolver.java

其中releaseProvider(provider);方法，与其对应的是incProviderRefLocked方法，他们的作用是维护对连接的引用计数，如果对某个CP引用计数为0时，将往主线程抛一条断开连接、清除该CP的缓存(在mProviderMap中)的消息（这里为了让连接能够保留一小段时间（保留到抛到主线程的消息执行之前），Android做出了十分骚气的操作，感兴趣的话可以去看看这两个方法的源码和注释、以及ProviderRefCount里removePending的注释）。

下面列出了调用ContentResolver.call方法，且CP进程还没启动的情况下的时序图：

![](media/9dec9df27b933141f84f1e31cbe5ee7a.png)

其他情况比较类似，有愿意画的小伙伴吗👏👏👏👏👏

**附录**

关于ContentProvider的发布

   
上面提到了CP的发布，其实它是和进程启动相关的一步操作，进程启动后，交给App端的入口方法为ActivityThread的main方法；它的简要时序图：

![](media/3b273d596fcf38d78a7d2520e00e5cc7.png)

其中 installContentProviders 代码如下（需要和区分installProvider方法区分开）：

>   private void installContentProviders(

>           Context context, List\<ProviderInfo\> providers) {

>       final ArrayList\<ContentProviderHolder\> results = new ArrayList\<\>();

>       for (ProviderInfo cpi : providers) {

>           if (*DEBUG_PROVIDER*) {

>               StringBuilder buf = new StringBuilder(128);

>               buf.append("Pub ");

>               buf.append(cpi.authority);

>               buf.append(": ");

>               buf.append(cpi.name);

>               Log.*i*(*TAG*, buf.toString());

>           }

>           ContentProviderHolder cph = installProvider(context, null, cpi,

>                   false /\*noisy\*/, true /\*noReleaseNeeded\*/, true
>   /\*stable\*/);

>           if (cph != null) {

>               cph.noReleaseNeeded = true;

>               results.add(cph);

>           }

>       }

>       try {

>           ActivityManager.*getService*().publishContentProviders(

>               getApplicationThread(), results);

>       } catch (RemoteException ex) {

>           throw ex.rethrowFromSystemServer();

>       }

>   }

其中：List\<ProviderInfo\>
providers是从AMS传过来的，由AMS找PMS解析Menifest得出；

其实 installContentProviders 就是依次调用待创建CP的installProvider方法进行创建，再调用publishContentProviders发布到AMS中做记录。

AMS中，publishContentProviders主要是将provider存起来，以便后续使用：

>   public final void publishContentProviders(IApplicationThread caller,

>           List\<ContentProviderHolder\> providers) {

>       if (providers == null) {

>           return;

>       }

>       enforceNotIsolatedCaller("publishContentProviders");

>       synchronized (this) {

>           final ProcessRecord r = getRecordForAppLocked(caller);

>           if (DEBUG_MU) Slog.v(*TAG_MU*, "ProcessRecord uid = " + r.uid);

>           if (r == null) {

>               throw new SecurityException(

>                       "Unable to find app for caller " + caller

>                     + " (pid=" + Binder.*getCallingPid*()

>                     + ") when publishing content providers");

>           }

>           final long origId = Binder.*clearCallingIdentity*();

>           final int N = providers.size();

>           for (int i = 0; i \< N; i++) { // 枚举所有待发布的CP

>               ContentProviderHolder src = providers.get(i);

>               if (src == null \|\| [src.info](http://src.info) == null \|\|
>   src.provider == null) {

>                   continue;

>               }

>               ContentProviderRecord dst =
>   r.pubProviders.get(src.[info.name](http://info.name)); //
>   attachApplication时，会把待发布的CP先存入pubProviders

>               if (DEBUG_MU) Slog.v(*TAG_MU*, "ContentProviderRecord uid = " +
>   dst.uid);

>               if (dst != null) { 

>                   ComponentName comp = new ComponentName(dst.info.packageName,
>   dst.info.name);

>                   mProviderMap.putProviderByClass(comp, dst);

>                   String names[] = dst.info.authority.split(";");

>                   for (int j = 0; j \< names.length; j++) {

>                       mProviderMap.putProviderByName(names[j], dst);

>                   }

>                   int launchingCount = mLaunchingProviders.size();

>                   int j;

>                   boolean wasInLaunchingProviders = false;

>                   for (j = 0; j \< launchingCount; j++) {

>                       if (mLaunchingProviders.get(j) == dst) {

>                           mLaunchingProviders.remove(j);

>                           wasInLaunchingProviders = true;

>                           j--;

>                           launchingCount--;

>                       }

>                   }

>                   if (wasInLaunchingProviders) {

>                      
>   mHandler.removeMessages(*CONTENT_PROVIDER_PUBLISH_TIMEOUT_MSG*, r); //
>   拆除ContentProvider发布时间超过10秒会ANR的炸弹

>                   }

>                   synchronized (dst) {

>                       dst.provider = src.provider;

>                       dst.proc = r;

>                       dst.notifyAll(); //
>   对应getContentProviderImpl中的cpr.wait()

>                   }

>                   updateOomAdjLocked(r, true);

>                   maybeUpdateProviderUsageStatsLocked(r, src.info.packageName,

>                           src.info.authority);

>               }

>           }

>           Binder.*restoreCallingIdentity*(origId);

>       }

>   }

关于ContentProvider的query等其他方法

这些方法代码同call类似，例如query：

>   public final \@Nullable Cursor query(final \@RequiresPermission.Read
>   \@NonNull Uri uri,

>           \@Nullable String[] projection, \@Nullable String selection,

>           \@Nullable String[] selectionArgs, \@Nullable String sortOrder,

>           \@Nullable CancellationSignal cancellationSignal) {

>       Preconditions.checkNotNull(uri, "uri");

>       IContentProvider unstableProvider = acquireUnstableProvider(uri);

>       if (unstableProvider == null) {

>           return null;

>       }

>       IContentProvider stableProvider = null;

>       Cursor qCursor = null;

>       try {

>           long startTime = SystemClock.*uptimeMillis*();

>           ICancellationSignal remoteCancellationSignal = null;

>           if (cancellationSignal != null) {

>               cancellationSignal.throwIfCanceled();

>               remoteCancellationSignal =
>   unstableProvider.createCancellationSignal();

>               cancellationSignal.setRemote(remoteCancellationSignal);

>           }

>           try {

>               qCursor = unstableProvider.query(mPackageName, uri, projection,

>                       selection, selectionArgs, sortOrder,
>   remoteCancellationSignal);

>           } catch (DeadObjectException e) {

>               // The remote process has died...  but we only hold an unstable

>               // reference though, so we might recover!!!  Let's try!!!!

>               // This is exciting!!1!!1!!!!1

>               unstableProviderDied(unstableProvider);

>               stableProvider = acquireProvider(uri);

>               if (stableProvider == null) {

>                   return null;

>               }

>               qCursor = stableProvider.query(mPackageName, uri, projection,

>                       selection, selectionArgs, sortOrder,
>   remoteCancellationSignal);

>           }

>           if (qCursor == null) {

>               return null;

>           }

>           // Force query execution.  Might fail and throw a runtime exception
>   here.

>           qCursor.getCount();

>           long durationMillis = SystemClock.*uptimeMillis*() - startTime;

>           maybeLogQueryToEventLog(durationMillis, uri, projection, selection,
>   sortOrder);

>           // Wrap the cursor object into CursorWrapperInner object.

>           final IContentProvider provider = (stableProvider != null) ?
>   stableProvider

>                   : acquireProvider(uri);

>           final CursorWrapperInner wrapper = new CursorWrapperInner(qCursor,
>   provider);

>           stableProvider = null;

>           qCursor = null;

>           return wrapper;

>       } catch (RemoteException e) {

>           // Arbitrary and not worth documenting, as Activity

>           // Manager will kill this process shortly anyway.

>           return null;

>       } finally {

>           if (qCursor != null) {

>               qCursor.close();

>           }

>           if (cancellationSignal != null) {

>               cancellationSignal.setRemote(null);

>           }

>           if (unstableProvider != null) {

>               releaseUnstableProvider(unstableProvider);

>           }

>           if (stableProvider != null) {

>               releaseProvider(stableProvider);

>           }

>       }

>   }

其实就比call多了一次acquireUnstableProvider的尝试，如果unstable尝试失败了，回滚尝试操作（unstableProviderDied(unstableProvider);），然后再次尝试acquireProvider（stable），stable和unstable在功能上没有太大区别，唯一的区别就是，stable的连接会导致客户端进程随CP进程的死亡而死亡，而unstable是catch住DeadObjectException之后再次做了stable的尝试。
