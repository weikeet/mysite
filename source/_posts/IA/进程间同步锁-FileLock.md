本次分享将围绕以下内容进行：

1、什么是FileChannel？

2、什么是FileLock？

3、如何使用FileLock实现进程间文件同步？

1、什么是FileChannel？

FileChannel用于读取、写入、映射和操作文件的通道，相比于传统的文件读写操作方式效率更高，更灵活，原理参见下图。

![](media/5510842d5af2ae68051c7034cd2dd0b0.png)

传统文件读写

![](media/51cf3f0a2ebb9c8e387de7358b9162d2.png)

nio文件读写

FileChannel对象不能直接创建。一个 FileChannel 实例只能通过在一个打开的 File
对象（RandomAccessFile、FileInputStream 或 FileOutputStream）上调用 getChannel()
方法获取。调用 getChannel() 方法会返回一个连接到相同文件的 FileChannel 对象且该
FileChannel 对象具有与 File
对象相同的访问权限，然后你就可以使用通道对象来利用强大的 FileChannel API了：

>   1. package java.nio.channels;  

>   2.   

>   3. public abstract class FileChannel extends AbstractChannel implements
>   ByteChannel, GatheringByteChannel, ScatteringByteChannel {  

>   4.     // This is a partial API listing  

>   5.     // All methods listed here can throw java.io.IOException  

>   6.   

>   7.     public abstract int read(ByteBuffer dst, long position);  

>   8.     public abstract int write(ByteBuffer src, long position);  

>   9.     public abstract long size();  

>   10.     // 返回当前文件的 position
>   值。返回值是一个长整型（long），表示文件中的当前字节位置。  

>   11.     public abstract long position();  

>   12.     // 将通道的 position
>   设置为指定值。负值，将异常伺候；值可以超过文件尾，这会导致文件空洞。  

>   13.     public abstract void position(long newPosition);  

>   14.     public abstract void truncate(long size);  

>   15.     public abstract void force(boolean metaData);  

>   16.     public final FileLock lock();  

>   17.     public abstract FileLock lock(long position, long size, boolean
>   shared);  

>   18.     public final FileLock tryLock();  

>   19.     public abstract FileLock tryLock(long position, long size, boolean
>   shared);  

>   20.     public abstract MappedByteBuffer map(MapMode mode, long position,
>   long size);  

>   21.       

>   22.     public abstract long transferTo(long position, long count,
>   WritableByteChannel target);  

>   23.     public abstract long transferFrom(ReadableByteChannel src, long
>   position, long count);  

>   24.   

>   25.     public static class MapMode {  

>   26.         public static final MapMode READ_ONLY;  

>   27.         public static final MapMode READ_WRITE;  

>   28.         public static final MapMode PRIVATE;  

>   29.     }  

>   30. }

FileChannel
对象是线程安全（thread-safe）的。多个进程可以在同一个实例上并发调用方法而不会引起任何问题，不过并非所有的操作都是多线程的（multithreaded）。影响通道位置或者影响文件大小的操作都是单线程的（single-threaded）。如果有一个线程已经在执行会影响通道位置或文件大小的操作，那么其他尝试进行此类操作之一的线程必须等待（被阻塞）。

2、什么是FileLock？

FileLock是java 1.4
版本后出现的一个类，它可以通过对一个可写文件(w)加锁，保证同时只有一个进程可以拿到文件的锁，这个进程从而可以对文件做访问；而其它拿不到锁的进程要么选择被挂起等待，要么选择去做一些其它的事情，
这样的机制保证了众进程可以顺序访问该文件。也可以看出，能够利用文件锁的这种性质，在一些场景下，虽然我们不需要操作某个文件，
但也可以通过 FileLock 来进行并发控制，保证进程的顺序执行，避免数据错误。

3、如何使用FileLock实现进程间文件同步？

    首先通过 NIO 的 API 首先获取文件的 FileChannel ，然后可以通过 FileChannel
以下4中方式获取FileLock。

(1)、通过lock() 获取 FileLock，获取文件的独占锁

>   public final FileLock lock() throws IOException {

>       return lock(0L, Long.MAX_VALUE, false);

>   }

默认锁定整个文件，并设置为独占锁。

(2)、通过改方法可以锁定文件的部分数据，并支持设置共享锁。

>   public FileLock lock(long position, long size, boolean shared) throws
>   IOException{

>     ......

>   }

position：锁定文件中的开始位置

size: 锁定文件中的内容长度

shared:
是否使用共享锁。true为共享锁定；false为独占锁定。一些不支持共享锁的操作系统,将自动将共享锁改成排它锁。可以通过调用isShared()方法来检测获得的是什么类型的锁。

(3)、试图获取文件独占锁

>   public final FileLock tryLock() throws IOException {

>       return tryLock(0L, Long.MAX_VALUE, false);

>   }

如果获取不到锁，则返回null。而不阻塞当前线程，等待获取锁。

(4)、通过改方法可以尝试获得文件的部分数据的锁，并支持设置共享锁。

>   public abstract FileLock tryLock(long position, long size, boolean shared)
>   throws IOException{

>       ......

>   }

参数同上(2)。

如果获取不到锁，则返回null。

    示例如下：

>   import java.io.FileNotFoundException;

>   import java.io.IOException;

>   import java.io.RandomAccessFile;

>   import java.nio.ByteBuffer;

>   import java.nio.channels.FileChannel;

>   import java.nio.channels.FileLock;

>   import java.util.Date;

>   public class FileLockTest {

>       public static void main(String[] args){

>           FileChannel channel = null;

>           FileLock lock = null;

>           try {

>               //1.
>   对于一个只读文件通过任意方式加锁时会报NonWritableChannelException异常

>               //2.
>   无参lock()默认为独占锁，不会报NonReadableChannelException异常，因为独占就是为了写

>               //3.
>   有参lock()为共享锁，所谓的共享也只能读共享，写是独占的，共享锁控制的代码只能是读操作，当有写冲突时会报NonWritableChannelException异常

>               RandomAccessFile raf = new RandomAccessFile("logfile.txt","rw");

>               //在文件末尾追加内容的处理

>               raf.seek(raf.length());

>               channel = raf.getChannel();

>               //获得锁方法一：lock()，阻塞的方法，当文件锁不可用时，当前进程会被挂起

>               lock = channel.lock();//无参lock()为独占锁

>               //lock = channel.lock(0L, Long.MAX_VALUE,
>   true);//有参lock()为共享锁，有写操作会报异常

>               //获得锁方法二：trylock()，非阻塞的方法，当文件锁不可用时，tryLock()会得到null值

>               //do {

>               //  lock = channel.tryLock();

>               //} while (null == lock);

>               //互斥操作

>               ByteBuffer sendBuffer=ByteBuffer.wrap((new Date()+"
>   写入\\n").getBytes());

>               channel.write(sendBuffer);

>               Thread.sleep(5000);

>           } catch (FileNotFoundException e) {

>               e.printStackTrace();

>           } catch (IOException e) {

>               e.printStackTrace();

>           } catch (InterruptedException e) {

>               e.printStackTrace();

>           } finally {

>               if (lock != null) {

>                   try {

>                       lock.release();

>                       lock = null;

>                   } catch (IOException e) {

>                       e.printStackTrace();

>                   }

>               }

>               if (channel != null) {

>                   try {

>                       channel.close();

>                       channel = null;

>                   } catch (IOException e) {

>                       e.printStackTrace();

>                   }

>               }

>           }

>       }

>   }

对于一个只读文件通过任意方式加锁时会报NonWritableChannelException异常。

无参lock()默认为独占锁，不会报NonReadableChannelException异常，因为独占就是为了写。

有参lock()为共享锁，所谓的共享也只能读共享，写是独占的，共享锁控制的代码只能是读操作，当有写冲突时会报NonWritableChannelException异常。

    注意在使用FileLock后调用release方法释放锁。

    在Nexus5x上写入单条数据时间在5ms左右，读出单条数据在5ms左右
