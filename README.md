
## dependencies
### Material
```
"hexo": "^4.2.1",
"hexo-deployer-git": "^2.1.0",
"hexo-filter-github-emojis": "^2.1.0",
"hexo-generator-archive": "^1.0.0",
"hexo-generator-category": "^1.0.0",
"hexo-generator-feed": "^1.2.2",
"hexo-generator-index": "^1.0.0",
"hexo-generator-search": "^2.3.0",
"hexo-generator-sitemap": "^2.0.0",
"hexo-generator-tag": "^1.0.0",
"hexo-helper-post-top": "0.0.1",
"hexo-helper-qrcode": "^1.0.2",
"hexo-pdf": "^1.1.1",
"hexo-permalink-pinyin": "^1.0.0",
"hexo-prism-plugin": "^2.3.0",
"hexo-renderer-ejs": "^1.0.0",
"hexo-renderer-marked": "^2.0.0",
"hexo-renderer-stylus": "^1.1.0",
"hexo-server": "^1.0.0",
"hexo-tag-aplayer": "^3.0.4",
"hexo-tag-dplayer": "^0.3.3",
"hexo-wordcount": "^6.0.1"
```

### Fluid
```
"hexo": "^4.2.1",
"hexo-deployer-git": "^2.1.0",
"hexo-generator-archive": "^1.0.0",
"hexo-generator-category": "^1.0.0",
"hexo-generator-feed": "^1.2.2",
"hexo-generator-index": "^1.0.0",
"hexo-generator-search": "^2.3.0",
"hexo-generator-sitemap": "^2.0.0",
"hexo-generator-tag": "^1.0.0",
"hexo-helper-post-top": "0.0.1",
"hexo-helper-qrcode": "^1.0.2",
"hexo-pdf": "^1.1.1",
"hexo-permalink-pinyin": "^1.0.0",
"hexo-renderer-ejs": "^1.0.0",
"hexo-renderer-marked": "^2.0.0",
"hexo-renderer-stylus": "^1.1.0",
"hexo-server": "^1.0.0",
"hexo-tag-aplayer": "^3.0.4",
"hexo-tag-dplayer": "^0.3.3",
"hexo-wordcount": "^6.0.1"
```